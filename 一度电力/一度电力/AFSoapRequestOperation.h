//
//  AFSoapRequestOperation.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>


@interface AFSoapRequestOperation : NSObject


/**
*  登陆请求接口
*
*  @param params 接口要求的参数 数字组存放字典格式的参数
*  @param view   hud显示的加载视图 传过来的是当前window的视图
*  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
*/
+ (void)loginSoapRequestWithParams:(NSArray*)params
                     hudShowInView:(UIView*)view
                             block:(void(^)(NSString *jsonStr))block;

/**
 *  忘记密码接口
 *
    @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)doForgetPwdSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block;

/**
 *  注册接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)registerSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block;

/**
 *  修改密码接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)modifyPwdSoapRequestWithParams:(NSArray*)params
                       hudShowInView:(UIView*)view
                               block:(void(^)(NSString *jsonStr))block;

/**
 *  修改基本信息接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)modifyInfoSoapRequestWithParams:(NSArray*)params
                        hudShowInView:(UIView*)view
                                block:(void(^)(NSString *jsonStr))block;

/**
 *  获取地址信息接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getMyAddressSoapRequestWithParams:(NSArray*)params
                         hudShowInView:(UIView*)view
                                 block:(void(^)(NSString *jsonStr))block;

/**
 *  获取有效地址信息接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getMyValidAddressSoapRequestWithParams:(NSArray*)params
                           hudShowInView:(UIView*)view
                                   block:(void(^)(NSString *jsonStr))block;

/**
 *  修改地址信息接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ReviseAddressSoapRequestWithParams:(NSArray*)params
                           hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block;

/**
 *  添加地址信息接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)addAddressSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block;


/**
 *  一键报障接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)sendWarningSoapRequestWithParams:(NSArray*)params
                         hudShowInView:(UIView*)view
                                 block:(void(^)(NSString *jsonStr))block;
/**
 *  检查之前任务是否全部完成接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getCountUnfinishMissionSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block;

/**
 *  任务进度查询接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)sgetMissionsSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block;

/**
 *  获取任务详情接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)missionDetailSoapRequestWithParams:(NSArray*)params
                           hudShowInView:(UIView*)view
                                   block:(void(^)(NSString *jsonStr))block;

/**
 *  电工获取任务列表接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianGetMissionSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                            block:(void(^)(NSString *jsonStr))block;

/**
 *  电工获取任务详情接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianGetMissionDeatailSoapRequestWithParams:(NSArray*)params
                                    hudShowInView:(UIView*)view
                                            block:(void(^)(NSString *jsonStr))block;
/**
 *  电工接受任务接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianReceiveMissionSoapRequestWithParams:(NSArray*)params
                                        hudShowInView:(UIView*)view
                                                block:(void(^)(NSString *jsonStr))block;
/**
 *  电工完成任务接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianFinishMissionSoapRequestWithParams:(NSArray*)params
                                        hudShowInView:(UIView*)view
                                                block:(void(^)(NSString *jsonStr))block;

/**
 *  评分接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)toScoreSoapRequestWithParams:(NSArray*)params
                                    hudShowInView:(UIView*)view
                                            block:(void(^)(NSString *jsonStr))block;

/**
 *  获取账户信息
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getBillDetailSoapRequestWithParams:(NSArray*)params
                      hudShowInView:(UIView*)view
                              block:(void(^)(NSString *jsonStr))block;
/**
 *  获取app版本号
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getAppVersionSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block;

/**
 *  获取历史任务
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getHistoryMissionRecordSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block;

//
/**
 *  获取任务详情
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getMissionDetailSoapRequestWithParams:(NSArray*)params
                                      hudShowInView:(UIView*)view
                                              block:(void(^)(NSString *jsonStr))block;

@end
