//
//  AFSoapRequestOperation.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "AFSoapRequestOperation.h"

@interface AFSoapRequestOperation ()


@end

@implementation AFSoapRequestOperation

#pragma mark - 登录
+ (void)loginSoapRequestWithParams:(NSArray*)params
                     hudShowInView:(UIView*)view
                             block:(void(^)(NSString *jsonStr))block{


    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"登录中" autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
     [weakSelf soapRequestWithURL:urlStr
                   methodName:login
                       params:params
                hudShowInView:(UIView*)view
                        block:^(NSString *jsonStr) {
         STRONGSELF
         if ([strongSelf nullResult:jsonStr]) {
             
             block(jsonStr);
             
         }else{
         
             [Utils showHUD:@"账号或密码错误" autoHide:YES inView:view];
         }
     }];

}

#pragma mark - 忘记密码
+(void)doForgetPwdSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"正在提交..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:doForgetPwd
                          params:params
                   hudShowInView:(UIView*)view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);

                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

#pragma mark -注册
+(void)registerSoapRequestWithParams:(NSArray*)params
                       hudShowInView:(UIView*)view
                               block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"注册中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:REGISTER
                          params:params
                   hudShowInView:(UIView*)view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}


#pragma mark - 修改密码
+(void)modifyPwdSoapRequestWithParams:(NSArray*)params
                        hudShowInView:(UIView*)view
                                block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"正在提交..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toModifyPwd
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

#pragma mark - 修改基本信息
+(void)modifyInfoSoapRequestWithParams:(NSArray*)params
                         hudShowInView:(UIView*)view
                                 block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"正在修改..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toModifyInfo
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}

+(void)getMyAddressSoapRequestWithParams:(NSArray *)params
                           hudShowInView:(UIView *)view
                                   block:(void (^)(NSString *))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(address_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toGetMyAddress
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}

#pragma mark - 获取有效地址
+(void)getMyValidAddressSoapRequestWithParams:(NSArray*)params
                                hudShowInView:(UIView*)view
                                        block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(address_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toGetMyValidAddress
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

#pragma mark - 修改地址
+(void)ReviseAddressSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"修改中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(address_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toModifyAddress
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];


}

#pragma mark -添加地址信息接口
+(void)addAddressSoapRequestWithParams:(NSArray*)params
                         hudShowInView:(UIView*)view
                                 block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"处理中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(address_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toAddAddress
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    
    

}

#pragma mark - 一键报障
+(void)sendWarningSoapRequestWithParams:(NSArray*)params
                          hudShowInView:(UIView*)view
                                  block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"正在提交..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toSendWarning
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}

#pragma mark - 检查之前任务是否全部完成接口
+(void)getCountUnfinishMissionSoapRequestWithParams:(NSArray *)params
                                      hudShowInView:(UIView *)view
                                              block:(void (^)(NSString *))block{
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];

    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getCountUnfinishMission
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}
#pragma mark - 任务进度查询
+(void)sgetMissionsSoapRequestWithParams:(NSArray*)params
                           hudShowInView:(UIView*)view
                                   block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
//    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getMissions
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}

#pragma mark - 获取任务详情
+(void)missionDetailSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
        [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getMissionDetail
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}
/**
 *  电工获取任务列表接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianGetMissionSoapRequestWithParams:(NSArray*)params
                                    hudShowInView:(UIView*)view
                                            block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
//    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:electricianGetMissionList
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

/**
 *  电工获取任务详情接口
 *
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)ElectricianGetMissionDeatailSoapRequestWithParams:(NSArray*)params
                                           hudShowInView:(UIView*)view
                                                   block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:electricianGetMissionDetail
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

#pragma mark ---- 电工接受任务接口
+(void)ElectricianReceiveMissionSoapRequestWithParams:(NSArray*)params
                                        hudShowInView:(UIView*)view
                                                block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:electricianReceiveMission
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    
    
}

#pragma mark - 评分
+(void)toScoreSoapRequestWithParams:(NSArray*)params
                      hudShowInView:(UIView*)view
                              block:(void(^)(NSString *jsonStr))block{

    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"正在提交..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:toScore
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}


#pragma mark ----- 电工完成任务接口

+(void)ElectricianFinishMissionSoapRequestWithParams:(NSArray*)params
                                       hudShowInView:(UIView*)view
                                               block:(void(^)(NSString *jsonStr))block{
    //加载hud视图 导航栏开始的全屏视图 让其不可点击
    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:electricianFinishMission
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];


}

#pragma mark - 获取账户信息
+(void)getBillDetailSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block{

    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(bill_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getBillList
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}


/**
 *  获取app版本号
 *  @param params 接口要求的参数 数字组存放字典格式的参数
 *  @param view   hud显示的加载视图 传过来的是当前window的视图
 *  @param block  返回json字符串 注 ：block 可自定义任何返回类型 只需改变 返回类型
 */
+(void)getAppVersionSoapRequestWithParams:(NSArray*)params
                            hudShowInView:(UIView*)view
                                    block:(void(^)(NSString *jsonStr))block{
//    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(user_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getAppVersion
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];


}

#pragma mark - 获取历史任务
+(void)getHistoryMissionRecordSoapRequestWithParams:(NSArray*)params
                                      hudShowInView:(UIView*)view
                                              block:(void(^)(NSString *jsonStr))block{

    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getHistoryMissionRecord
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];

}

#pragma mark -  获取任务详情
+(void)getMissionDetailSoapRequestWithParams:(NSArray*)params
                               hudShowInView:(UIView*)view
                                       block:(void(^)(NSString *jsonStr))block{

    [Utils showHUD:@"加载中..." autoHide:NO inView:view];
    
    NSString *urlStr = BASEURL(mission_Class);
    
    WEAKSELF
    [weakSelf soapRequestWithURL:urlStr
                      methodName:getMissionDetail
                          params:params
                   hudShowInView:view
                           block:^(NSString *jsonStr) {
                               
                               STRONGSELF
                               if ([strongSelf nullResult:jsonStr]) {
                                   
                                   block(jsonStr);
                                   
                               }else{
                                   
                                   [Utils showHUD:LoadEmpty autoHide:YES inView:view];
                               }
                           }];
    

}

/**
 *  通用的请求方法
 *
 *  @param url        请求的url
 *  @param methodName 接口名称 登陆对应 login
 *  @param params     接口需要的字段 数组中存放字典格式的数据
 */

#pragma mark -通用的请求方法
+(void)soapRequestWithURL:(NSString*)urlStr
               methodName:(NSString*)methodName
                   params:(NSArray*)params
            hudShowInView:(UIView*)hudShowInView
                    block:(void(^)(NSString *jsonStr))block{

    WEAKSELF
    [weakSelf  checkNetWorkStatusWithHudShowInView:hudShowInView block:^(BOOL networkState) {
        
        if (networkState) {
            
            STRONGSELF
            NSURL *url = [NSURL URLWithString:urlStr];
            //获取soap报头字符串
            NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:params methodName:methodName];
            
            //创建请求
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
            NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMsg length]];
            [request addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];

            [request addValue: msgLength forHTTPHeaderField:@"Content-Length"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody: [soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
            
            //afn请求
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString * returnSoapXML = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSString *jsonStr = [strongSelf paserXML:returnSoapXML];
                block(jsonStr);
                [Utils hideHUD];
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [Utils showHUD:@"网络繁忙，请稍后重试" autoHide:YES inView:hudShowInView];
                [self endRefrensh:hudShowInView];
                
            }];
            
            [operation start];

        }
   
        
    }];

}

#pragma mark - tableView 刷新 或加载更多时 出现错误把菊花停掉
+(void)endRefrensh:(UIView*)view{

    if ([view  isKindOfClass:[UITableView class]]) {
        
        UITableView *tableView = (UITableView*)view;
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    }

}

#pragma mark -解析服务器返回的数据
+(NSString*)paserXML:(NSString*)xmlString{
    
    
    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithXMLString:xmlString options:0 error:nil];
    GDataXMLElement *xmlEle = [xmlDoc rootElement];
    NSArray *array = [xmlEle children];
    
    NSString *jsonStr;
    for (int i = 0; i < [array count]; i++) {
        GDataXMLElement *ele = [array objectAtIndex:i];
        // 根据标签名判断
        if ([[ele name] isEqualToString:@"return"]) {
            // 读标签里面的属性
            NSLog(@"name --> %@", [[ele attributeForName:@"value"] stringValue]);
            jsonStr = @"";
            
        } else {
            // 直接读标签间的String
            jsonStr = [ele stringValue];
        }
        
    }
    
    return jsonStr;
}

#pragma mark -判断返回结果是否为空 或失败
+(BOOL)nullResult:(NSString*)jsonStr{

    if ([jsonStr isEqualToString:@""] || [jsonStr isEqualToString:@"null"]) {
        
        return NO;
        
    }else if (jsonStr == NULL){
        
        return NO;
    }
    
    return YES;
}

#pragma mark -检查网络
+ (void)checkNetWorkStatusWithHudShowInView:(UIView*)hudView
                                      block:(void(^)(BOOL networkState))block{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if(status == AFNetworkReachabilityStatusNotReachable){
            NSLog(@"网络连接已断开，请检查您的网络！");
            [Utils showHUD:@"网络连接已断开，请检查您的网络！" autoHide:YES inView:hudView];
            [self endRefrensh:hudView];
            
            block(NO);
            return ;
        }else if (status == AFNetworkReachabilityStatusUnknown){
            [self endRefrensh:hudView];
            NSLog(@"未知网络");
            block(YES);
        }else{
                if (status == AFNetworkReachabilityStatusReachableViaWWAN){
                NSLog(@"移动蜂窝网络");
                block(YES);
                
            }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
                NSLog(@"Wifi 网络");
                block(YES);
                
            }
            
        }
        
    }];
    
}


@end
