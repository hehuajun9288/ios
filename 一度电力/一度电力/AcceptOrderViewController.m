//
//  AcceptOrderViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/25.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AcceptOrderViewController.h"
#import "OrderDetailTableViewController.h"

@interface AcceptOrderViewController ()

@end

@implementation AcceptOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"任务状态";
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Mission *mission = self.missionArr[indexPath.row];
    OrderDetailTableViewController *orderDetailTVC = [[OrderDetailTableViewController alloc] init];
    orderDetailTVC.dno = mission.dno;
    orderDetailTVC.FinishReqeust = ^(BOOL finishRequest){
    if (finishRequest) {
    [self.tableView.mj_header beginRefreshing];
    }
    };
    [self.navigationController pushViewController:orderDetailTVC animated:YES];
}
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    return [self creatTableHeaderView:@[@"交易类型",@"日期"]];
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    UIView *v = [self creatTableHeaderView:@[@"交易类型",@"日期"]];
//    
//    return v.HEIGTH;
//}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
