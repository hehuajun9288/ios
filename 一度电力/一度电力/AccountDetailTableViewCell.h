//
//  AccountDetailTableViewCell.h
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

//带输入的cell
@interface MissionDetailInputCell : UITableViewCell

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *whiteContentView ;

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                   labelText:(NSString*)labelText;

@end

@interface AccountDetailTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *detailLB;
@property (nonatomic, strong) UILabel *titleLB;
@property (nonatomic, strong) UIView *whiteContentView ;

/**
 *  初始化cell
 *
 *  @param style           style
 *  @param reuseIdentifier reuseIdentifier
 *  @param labelText       前面label显示的文本
 *  @param info            交易信息
 *
 *  @return cell
 */
-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                   labelText:(NSString*)labelText
                        info:(NSString*)info;



@end

@interface ButtonCell : UITableViewCell

@property (nonatomic, strong) UIButton *button ;
@property (nonatomic, copy) void (^buttonTap)();

@end