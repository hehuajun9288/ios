//
//  AccountDetailTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AccountDetailTableViewCell.h"

@implementation AccountDetailTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                   labelText:(NSString*)labelText
                        info:(NSString*)info{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _whiteContentView = [[UIView alloc] initWithFrame:CGRectMake(5, VIEW_START_Y, SCREEN_WIDTH - 10, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
        [self addSubview:_whiteContentView];
        _whiteContentView.backgroundColor = WHITE_COLOR;
        
        CGSize textSize = [Utils getTextSize:labelText
                                  textRegion:CGSizeMake(200, 20) textFontSize:App_Delegate.largeFont];
        CGFloat detailLB_width = SCREEN_WIDTH - VIEW_START_X * 3 -textSize.width;
        CGFloat detailLB_HEIGHT = (TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 4);

        _titleLB = [UILabel labelWithFrame:CGRectMake(VIEW_START_X, VIEW_START_Y, textSize.width , detailLB_HEIGHT)
                                 LableText:labelText
                                      size:App_Delegate.largeFont
                                 textColor:[UIColor lightGrayColor]
                             textAlignment:NSTextAlignmentLeft];
        [_whiteContentView addSubview:_titleLB];

          CGSize infoTextSize = [Utils getTextSize:info
                                      textRegion:CGSizeMake(detailLB_width, 1000)
                                    textFontSize:App_Delegate.largeFont];
        
        _detailLB = [UILabel labelWithFrame:CGRectMake(textSize.width + VIEW_START_X * 2, VIEW_START_Y, detailLB_width ,infoTextSize.height >  detailLB_HEIGHT ? infoTextSize.height : detailLB_HEIGHT)
                                  LableText:info
                                       size:App_Delegate.largeFont
                                  textColor:[UIColor lightGrayColor]
                              textAlignment:NSTextAlignmentCenter];
        [_whiteContentView addSubview:_detailLB];

        
        CGPoint center = _titleLB.center;
        center.y = _detailLB.center.y;
        _titleLB.center = center;
        
        
        CGRect frame = _whiteContentView.frame;
        frame.size.height = _detailLB.HEIGTH + _detailLB.Y + VIEW_START_Y ;
        _whiteContentView.frame = frame;
        
        frame = self.frame;
        frame.size.height = _whiteContentView.HEIGTH+ _whiteContentView.Y  ;
        self.frame = frame;

    }
    
    return self;
}

@end

@implementation MissionDetailInputCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                   labelText:(NSString *)labelText{

    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _whiteContentView = [[UIView alloc] initWithFrame:CGRectMake(5, VIEW_START_Y, SCREEN_WIDTH - 10, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
        [self addSubview:_whiteContentView];
        _whiteContentView.backgroundColor = WHITE_COLOR;
        
        CGSize textSize = [Utils getTextSize:labelText
                                  textRegion:CGSizeMake(200, _whiteContentView.HEIGTH)
                                textFontSize:App_Delegate.largeFont];
        
        UILabel *label = [UILabel labelWithFrame:CGRectMake(VIEW_START_X, 0, textSize.width, _whiteContentView.HEIGTH)
                                       LableText:labelText
                                            size:App_Delegate.largeFont
                                       textColor:[UIColor lightGrayColor]
                                   textAlignment:NSTextAlignmentLeft];
        
        [_whiteContentView addSubview:label];
        
        CGFloat x = label.X + label.WIDTH + VIEW_START_X;
        _textField = [UITextField textFieldWithFrame:CGRectMake(x, 0, _whiteContentView.WIDTH - x, _whiteContentView.HEIGTH)
                                           placehold:@""
                                              target:self
                                                 tag:0];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        [_whiteContentView addSubview:_textField];
         self.frame = CGRectMake(0, 0, SCREEN_WIDTH, _whiteContentView.HEIGTH + VIEW_START_Y * 2);
        
    }
    
    return self;
}


@end

@implementation ButtonCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
        _button = [UIButton buttonWithFrame:frame
                                               title:@"提交"
                                            fontSize:App_Delegate.largeFont
                                          titleColor:WHITE_COLOR
                                              action:@selector(click)
                                              target:self
                                                 tag:0
                                     backgroundColor:BUTTON_BACKGROUND_COLOR];
        [self addSubview:_button];

    }
    
    return self;
}

-(void)click{

    if (_buttonTap) {
        _buttonTap();
    }
}

@end
