//
//  AccountDetailTableViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillDetail.h"

@interface AccountDetailTableViewController : UITableViewController

@property (nonatomic, strong) BillDetail *billDetail;

@end
