//
//  AccountDetailTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AccountDetailTableViewController.h"
#import "AccountDetailTableViewCell.h"

@interface AccountDetailTableViewController ()

@end

@implementation AccountDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"交易详情";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *labelText = @"";
    NSString *detailText = @"";
    
 
    switch (indexPath.row) {
        case 0:
            labelText = @"编号";
            detailText = _billDetail.id;
            break;
            
        case 1:
            labelText = @"类型";
            detailText = _billDetail.logtype;

            break;
        case 2:
            labelText = @"交易时间";
            detailText = _billDetail.rtime;

            break;
        case 3:
            labelText = @"任务描述";
            detailText = [NSString stringWithFormat:@"%@",_billDetail.info];

            break;
        case 4:
            labelText = @"实际费用";
            detailText = [NSString stringWithFormat:@"¥%.2f",_billDetail.overage.floatValue];

            break;
    
        default:
            break;
    }

    AccountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AccountDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:@"cell"
                                                       labelText:labelText
                                                            info:detailText ];
    }

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    AccountDetailTableViewCell *cell = (AccountDetailTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];

    return cell.HEIGTH;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
