//
//  AccountInfoViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AccountInfoViewController.h"
#import "BillDetail.h"
#import "AccountTableViewCell.h"
#import "AccountDetailTableViewController.h"

@interface AccountInfoViewController ()

@property (nonatomic, strong) NSMutableArray *billDetails;
@end

//请求的页数 从第一页开始
static int page = 1;

@implementation AccountInfoViewController


-(NSMutableArray *)billDetails{
    
    if (_billDetails == nil) {
        
        _billDetails = [NSMutableArray array];
    }
    
    return _billDetails;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账户交易信息";
    
    [self.tableView.mj_header beginRefreshing];

}
#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    int size = 10;
    int offset = (page - 1) * size;
    NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
    NSDictionary *offsetDic = @{@"offset":[NSString stringWithFormat:@"%d",offset]};
    NSDictionary *sizeDic = @{@"size":[NSString stringWithFormat:@"%d",size]};
    
    return @[uidDic,offsetDic,sizeDic];
}

#pragma mark - 下拉刷新获取任务列表
-(void)getHeaderRefreshBillDetail{
    
    
    WEAKSELF
    [AFSoapRequestOperation getBillDetailSoapRequestWithParams:[weakSelf getParams]
                                                 hudShowInView:weakSelf.tableView
                                                         block:^(NSString *jsonStr) {
                                                             
                                                             STRONGSELF
                                                             NSArray *infoDics = [jsonStr JSONValue];
                                                             
                                                             [weakSelf.billDetails removeAllObjects];
                                                             
                                                             //获取mission对象
                                                             for (NSDictionary *infoDic in infoDics) {
                                                                 
                                                                 BillDetail *info = [BillDetail mj_objectWithKeyValues:infoDic];
                                                                 [strongSelf.billDetails addObject:info];
                                                             }
                                                             
                                                             [strongSelf.tableView reloadData];
                                                             
                                                             //结束刷新
                                                             [strongSelf.tableView.mj_header endRefreshing];
                                                         }];
}

#pragma mark - 重写父类下拉刷新和上啦加载更多的方法
-(void)headerWithRefresh{
    
    page = 1;
    [self getHeaderRefreshBillDetail];
    
    //恢复加载更多
    [self.tableView.mj_footer resetNoMoreData];
}
-(void)footerWithRefresh{
    
    page ++ ;
    [self getFooterRefreshBillDetail];
}

#pragma mark - 加载更多
-(void)getFooterRefreshBillDetail{
    
    WEAKSELF
    [AFSoapRequestOperation getBillDetailSoapRequestWithParams:[weakSelf getParams]
                                                 hudShowInView:weakSelf.tableView
                                                         block:^(NSString *jsonStr) {
                                                             
                                                             STRONGSELF
                                                             NSArray *infoDics = [jsonStr JSONValue];
                                                             
                                                             //全部加载完毕
                                                             if (infoDics.count == 0) {
                                                                 
                                                                 [strongSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                                 return ;
                                                             }
                                                             
                                                             //获取mission对象
                                                             for (NSDictionary *infoDic in infoDics) {
                                                                 
                                                                 BillDetail *info = [BillDetail mj_objectWithKeyValues:infoDic];
                                                                 [strongSelf.billDetails addObject:info];
                                                             }
                                                             
                                                             [strongSelf.tableView reloadData];
                                                             
                                                             //结束刷新
                                                             [strongSelf.tableView.mj_footer endRefreshing];
                                                         }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _billDetails.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    AccountTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AccountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    BillDetail *detail = _billDetails[indexPath.row];
    cell.typeLabel.text = detail.logtype;
    cell.dateLable.text = [detail.rtime componentsSeparatedByString:@" "][0];
    
    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    return [self creatTableHeaderView:@[@"交易类型",@"日期"]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    UIView *v = [self creatTableHeaderView:@[@"交易类型",@"日期"]];

    return v.HEIGTH;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    AccountDetailTableViewController *adTVC = [[AccountDetailTableViewController alloc] init];
    adTVC.billDetail = _billDetails[indexPath.row];
    [self.navigationController pushViewController:adTVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 44;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
