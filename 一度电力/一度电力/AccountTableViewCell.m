//
//  AccountTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AccountTableViewCell.h"

@implementation AccountTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        CGFloat labelWidht = SCREEN_WIDTH / 2;
        
        CGRect frame = CGRectMake(0, 0, labelWidht, self.HEIGTH);
        _typeLabel = [self creatLabelWithFrame:frame
                                     textColor:BLACK_COLOR];
        [self addSubview:_typeLabel];
        
        frame.origin.x = labelWidht;
        _dateLable = [self creatLabelWithFrame:frame
                                     textColor:[UIColor lightGrayColor]];
        
        [self addSubview:_dateLable];
    }
    
    return self;
}

-(UILabel*)creatLabelWithFrame:(CGRect)frame textColor:(UIColor*)textColor{

    UILabel *label = [UILabel labelWithFrame:frame
                                   LableText:@""
                                        size:App_Delegate.largeFont
                                   textColor:textColor
                               textAlignment:NSTextAlignmentCenter];
    
    return label;
    
}
@end
