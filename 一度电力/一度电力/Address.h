//
//  Address.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject


/**
 "id": 7,
 "uid": 102,
 "sid": 2,
 "areacode": null,
 "province": null,
 "city": null,
 "district": null,
 "street": null,
 "address": "长沙市雨花区韶山中路100号",
 "zipcode": null
 
 */

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *sid;
@property (nonatomic, strong) NSString *areacode;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *zipcode;

-(void)nullString;

@end
