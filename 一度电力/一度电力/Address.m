//
//  Address.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "Address.h"

@implementation Address

/**
 "id": 7,
 "uid": 102,
 "sid": 2,
 "areacode": null,
 "province": null,
 "city": null,
 "district": null,
 "street": null,
 "address": "长沙市雨花区韶山中路100号",
 "zipcode": null
 
 */

-(void)nullString{

    if (self.id == NULL) {
        
        self.id = @"";
    }
    if (self.uid == NULL) {
        
        self.uid = @"";
    }
    if (self.sid == NULL) {
        
        self.sid = @"";
    }
    if (self.areacode == NULL) {
        
        self.areacode = @"";
    }
    if (self.province == NULL) {
        
        self.province = @"";
    }
    if (self.city == NULL) {
        
        self.city = @"";
    }
    if (self.district == NULL) {
        
        self.district = @"";
    }
    if (self.street == NULL) {
        
        self.street = @"";
    }
    if (self.address == NULL) {
        
        self.address = @"";
    }
    if (self.zipcode == NULL) {
        
        self.zipcode = @"";
    }



}

@end
