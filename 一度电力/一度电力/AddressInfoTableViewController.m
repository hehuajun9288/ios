//
//  AddressInfoTableViewController.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/28.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "AddressInfoTableViewController.h"
#import "AddressInfoView.h"

@interface AddressInfoTableViewController ()<AddressViewDelegate>

@property (nonatomic, strong) AddressInfoView *addressInfoView;
@end

@implementation AddressInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址信息";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    

    _addressInfoView = [[AddressInfoView alloc] initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT )];
    _addressInfoView.delegate = self;
    [self.view addSubview:_addressInfoView];
    
}

#pragma mark -addressView delegate
-(void)popViewController{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 移除通知
-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_addressInfoView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
