//
//  AddressInfoView.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/1.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressViewDelegate <NSObject>

-(void)popViewController;

@end
@interface AddressInfoView : UIView

@property (nonatomic, weak) id <AddressViewDelegate> delegate ;

@end
