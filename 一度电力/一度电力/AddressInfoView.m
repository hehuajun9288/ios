//
//  AddressInfoView.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/1.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AddressInfoView.h"
#import "Address.h"
#import "ModifyAddressResult.h"
#import "AddAddressResult.h"

@interface AddressInfoView ()<UITextViewDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *addressArr;
@property (nonatomic, strong) NSMutableArray *textFieldArr;
@property (nonatomic, strong) UIButton *changeBtn;
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, strong) UITextField *modifyTF; //正在编辑的textField
@end
@implementation AddressInfoView


-(NSMutableArray *)addressArr{

    if (_addressArr == nil) {
        
        _addressArr = [NSMutableArray array];
    }
    
    return _addressArr;
}
-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self createView];
    }
    
    return self;
}

#define Start_X VIEW_START_X           // 第一个textField的X坐标
#define Start_Y 20.0f           // 第一个textField的Y坐标
#define Width_Space 5.0f        // 2个按钮之间的横间距
#define Height_Space 20.0f      // 竖间距
#define Button_Height (TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)   // 高
#define Button_Width (SCREEN_WIDTH - VIEW_START_X * 2)      // 宽

#pragma mark -创建视图
-(void)createView
{
    
    _textFieldArr = [NSMutableArray array];
    
    //添加通知 监听textfield的变化
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
    
    //请求参数
    NSDictionary *idDic = @{@"id":App_Delegate.userModel.id};
    
    //获取地址信息
    WEAKSELF
    [AFSoapRequestOperation getMyAddressSoapRequestWithParams:@[idDic] hudShowInView:self block:^(NSString *jsonStr) {
        
        STRONGSELF
        NSArray *addressDics = [jsonStr JSONValue];
        for (NSDictionary *addressDic in addressDics) {
            
            Address *address = [Address mj_objectWithKeyValues:addressDic];
            [address nullString];
            [strongSelf.addressArr addObject:address];
        }
        
        
        [strongSelf creatTextFieldAndButtons];
        

    }];

}

#pragma mark -创建textField和button
-(void)creatTextFieldAndButtons{

//    UITextView *textView ;
    
    UITextField *textField;
    for (int i = 0 ; i < _addressArr.count; i++) {
        
        Address *address = _addressArr[i];
        CGSize size = [Utils getTextSize:address.address
                              textRegion:CGSizeMake(SCREEN_WIDTH - 20, 1000)
                            textFontSize:App_Delegate.largeFont];

        CGRect frame = CGRectMake(i % 1 * (Button_Width + Width_Space) + Start_X, i / 1  * (size.height + Height_Space)+Start_Y, Button_Width, size.height > Button_Height ? size.height + 10 : BUTTON_HEIGHT);
        
        textField = [UITextField textFieldWithFrame:frame
                                          placehold:address.address
                                             target:self
                                                tag:i];
        _modifyTF = textField;
        textField.backgroundColor = [UIColor whiteColor];
        textField.text  = address.address;
        textField.textAlignment = NSTextAlignmentCenter;
//        textView = [UITextView new];
//        textView.frame = frame;
//        textView.tag = i;
//        textView.delegate = self;
//        textView.backgroundColor = WHITE_COLOR;
//        textView.textAlignment = NSTextAlignmentCenter;
//        textView.text = address.address;
//        [textView setFont:[UIFont systemFontOfSize:19]];
        [self addSubview:textField];
        

//        
//        [textView mas_makeConstraints:^(MASConstraintMaker *make) {
//           
//            make.left.mas_equalTo(VIEW_START_X);
//            make.right.mas_equalTo(-VIEW_START_X);
//            make.top.mas_equalTo(20);
//            make.height.mas_equalTo(size.height + 20);
//        }];
        
    }
    
    [self creatButtonWithFrame:textField.frame];

}
#pragma mark - 创建修改和添加密码按钮
-(void)creatButtonWithFrame:(CGRect)frame{

    //圆角按钮
//    CGRect frame = CGRectMake(VIEW_START_X, textView.frame.origin.y + textView.frame.size.height + Height_Space, SCREEN_WIDTH - VIEW_START_X * 2, BUTTON_HEIGHT);

    //修改
    _changeBtn = [self creatButtonWhthFrame:frame.origin.y + frame.size.height + Height_Space Title:@"修改"];
//    [UIButton buttonWithFrame:frame
//                                            title:@"修改"
//                                         fontSize:App_Delegate.smallFont
//                                       titleColor:WHITE_COLOR
//                                           action:@selector(buttonClick:)
//                                           target:self tag:1
//                                  backgroundColor:BUTTON_BACKGROUND_COLOR];
//    [_changeBtn setBorder];
    [_changeBtn addTarget:self action:@selector(modifyAddress) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_changeBtn];
    
//    WEAKSELF
//    weakSelf.changeBtn.buttonTap = ^(){
//    
//        STRONGSELF
//        [strongSelf modifyAddress];
//        
//    };
//    frame.origin.x = SCREEN_WIDTH - CONTENT_VIEW_START_X - _changeBtn.WIDTH;
//
//    //添加按钮
//    _addBtn = [UIButton buttonWithFrame:frame
//                                            title:@"添加"
//                                         fontSize:App_Delegate.smallFont
//                                       titleColor:WHITE_COLOR
//                                           action:@selector(buttonClick:)
//                                           target:self tag:2
//                                  backgroundColor:BUTTON_BACKGROUND_COLOR];
//    
//    [_addBtn setBorder];
//
//    [self addSubview:_addBtn];

}
-(void)dealloc{
    
    NSLog(@"dealloc");
}
#pragma mark - button的处理方法
-(void)buttonClick:(UIButton*)button{

    [_modifyTF resignFirstResponder];
    if (button.tag == 1) {//修改
        
        [self modifyAddress];
        
        }else{//添加
            
            if (App_Delegate.userModel.utypeid.intValue == 8) {//普通用户
                
                [self addAddress];
                
            }else{//物业和电工用户
            
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您暂不能添加地址.请与客服联系" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
            }
            
     }
}

#pragma mark - 添加地址 只有普通用户才能添加地址
-(void)addAddress{

    if (_addressArr.count >= 5) {
        
        [Utils showHUD:@"最多只能添加5个地址" autoHide:YES inView:self];
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入地址" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alertView show];
        
    }

}
#pragma mark - 修改地址
-(void)modifyAddress{

    [_modifyTF resignFirstResponder];
    NSMutableString *jsonStr = [NSMutableString stringWithString:@"["];
    
    //判断地址是否为空
    BOOL isNullAddress = NO;
    
    //遍历数组 把数组中的对象转json
    for (int i = 0; i < _addressArr.count; i ++) {
        
        Address *ad  = _addressArr[i];
        
        if ([ad.address isEqualToString:@""]) {
            
            isNullAddress = YES;
            break;
        }else{
        
            NSDictionary *dic = [ad mj_keyValues];
            [jsonStr appendString: [NSString stringWithFormat:@"%@",[Utils dictionaryToJsonStriing:dic]]];
            
            if (i != (_addressArr.count - 1)) {
                
                [jsonStr appendString:@","];
            }

        }
    }
    [jsonStr appendString:@"]"];
    
    if (isNullAddress) {
        
        [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,输入的地址不能为空..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
        
    }else{//提交修改
        
        NSDictionary* infoDic = @{@"liststr":jsonStr};
        WEAKSELF
        [AFSoapRequestOperation ReviseAddressSoapRequestWithParams:@[infoDic] hudShowInView:weakSelf block:^(NSString *jsonStr) {
            
            STRONGSELF
            ModifyAddressResult *ar = [ModifyAddressResult mj_objectWithKeyValues:[jsonStr JSONValue]];
            
            if (ar.result.intValue == 1) {
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您修改地址成功!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [strongSelf.delegate popViewController];
                }];

            }else{
            
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,修改地址失败.请与客服联系..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];

            }
        }];

    }
}
#pragma mark - alertView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 1) {
        
        
        UITextField *tf = [alertView textFieldAtIndex:0];
        
        if (tf.text.length > 0) {
            
            //获取数组中的对象 ，创建新的对象时方便创建属性
            Address *ad = [_addressArr objectAtIndex:0];
            NSDictionary *adDic = [ad mj_keyValues];
            
            //新添加的地址对象
            Address *addAddress = [Address mj_objectWithKeyValues:adDic];
            addAddress.address = tf.text;
            
            //新添加的对象转字符串
            NSString *jsonStr = [Utils dictionaryToJsonStriing:[addAddress mj_keyValues]];

            NSDictionary *addDic = @{@"addrstr":jsonStr};
            
            WEAKSELF
            [AFSoapRequestOperation addAddressSoapRequestWithParams:@[addDic]
                                                      hudShowInView:weakSelf
                                                              block:^(NSString *jsonStr) {
                                                                  
                                                                  STRONGSELF
                                                                  AddAddressResult *aar = [AddAddressResult mj_objectWithKeyValues:[jsonStr JSONValue]];
                                                                  
                                                                  if (aar.result.intValue == 1) {
                                                                      
                                                                      [strongSelf.addressArr addObject:addAddress];
                                                                      
                                                                      //移除所有子视图 重新创建
                                                                      for (UIView *view in self.subviews) {
                                                                          
                                                                          [view removeFromSuperview];
                                                                      }
                                                                      
                                                                      [self creatTextFieldAndButtons];


                                                                      [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您添加地址成功!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                         
                                                                          [strongSelf.delegate popViewController];
                                                                      }];
                                                                  }else{
                                                                  
                                                                      [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,添加地址失败.请与客服联系!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                                  }
                                                                  
                                                          
                                                      }];

        }else{
        
            [Utils showHUD:@"请输入正确地址" autoHide:YES inView:self];
        }
        
    }
}

#pragma mark -textField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    _modifyTF = textField;
}

#pragma mark -监听textfield内容变化
-(void)textFieldTextDidChangeNotification:(NSNotification*)noty{
    
    UITextField *tf = noty.object;
    
    Address *address = [_addressArr objectAtIndex:tf.tag];
    address.address = tf.text;

    if ([tf.text isEqualToString:@""]) {
        
        [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,输入的地址不能为空..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];

    }
    
//    CGSize size = [Utils getTextSize:tf.text
//                          textRegion:CGSizeMake(tf.WIDTH, 1000) textFontSize:App_Delegate.largeFont];
    
    
//    CGRect frame = tf.frame;
//    frame.size.height = size.height > BUTTON_HEIGHT ? size.height : BUTTON_HEIGHT;
//    tf.frame = frame;
//    
//    frame = _changeBtn.frame;
//    frame.origin.y = tf.Y + tf.HEIGTH + Height_Space;
//    _changeBtn.frame = frame;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
