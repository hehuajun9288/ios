//
//  App.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

/**
 *  宏定义文件
 */
#ifndef App_h
#define App_h

//是否记住密码
#define rememberPwdKey @"rememberPwd"

//登陆名的key
#define loginNameKey  @"loginName"

//密码key
#define passworkKey  @"passwork"

//账号密码的key
#define accountKey  @"account"

//输入密码时长度错误的提示
#define PWD_ALERT_TEXT @"密码长度必须在6 - 20个字符之间"

#define user_defaults           [NSUserDefaults standardUserDefaults]

//手机号的长度
#define PHONE_NUMBER_LENGTH   11

#define VIEW_START_X  15                             //控件的x起点
#define VIEW_START_Y  10                             //控件的Y起点
#define CONTENT_VIEW_START_X  40                    //登陆或注册内容输入的视图开始 X
#define BUTTON_HEIGHT  (TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)
#define RGBA(R/*红*/, G/*绿*/, B/*蓝*/, A/*透明*/) \
[UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]

#define CLEAR_COLOR  [UIColor clearColor]
#define WHITE_COLOR  [UIColor whiteColor]
#define BLACK_COLOR  [UIColor blackColor]

#define BUTTON_BACKGROUND_COLOR  RGBA(26, 36, 109, 1)

//view背景色蓝色
#define VIEW_BACKGROUND_COLOR_BLUE RGBA(41, 56, 139, 1)

//#define SCALE           9.5

//tableView cell的高度
#define TABLE_VIEW_HEIGHT_FOR_ROW  SCREEN_HEIGHT * 0.105

//屏幕宽高
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

//导航栏+状态栏的高度
#define NAVIGATIONBAR_HEIGHT    64

//加载框隐藏时间
#define HUDHIDEDELAY            2.5

// block self 防止弱引用 不走dealloc 方法 参照代码
#define WEAKSELF typeof(self) __weak weakSelf = self;
#define STRONGSELF typeof(weakSelf) __strong strongSelf = weakSelf;

//HUD显示文字
#define NoConnectionNetwork @"没有连接网络!"
#define LOGINING            @"登录中..."
#define Loading             @"加载中"
#define LoadFailed          @"请求超时，请稍后再试"
#define LoadData            @"暂无更多数据!"
#define LoadNull            @"暂无搜索结果!"
#define LoadTimeOut         @"请求超时，请稍后再试"
#define LoadSuccess         @"加载成功"
#define LoadEmpty           @"网络数据错误，请稍后再试"

//判断设备是否IPHONE4
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

//判断设备是否IPHONE4
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define IPHONE6PLUS ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

//判断iphone6
#define IPHONE6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

// 引用delegate的属性 直接 CT_app.属性
#define App_Delegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
//#   define DLog(...)
#endif

#endif /* App_h */
