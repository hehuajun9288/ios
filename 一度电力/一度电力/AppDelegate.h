//
//  AppDelegate.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/19.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (assign,nonatomic) CGFloat smallFont; //小号字体
@property (assign,nonatomic) CGFloat largeFont; //大号字体

@property (nonatomic, strong) UserModel *userModel ;
@property (nonatomic, strong) NSString *password ;

@end

