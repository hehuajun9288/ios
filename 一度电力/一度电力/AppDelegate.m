//
//  AppDelegate.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/19.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    LoginViewController *viewCtr   = [[LoginViewController alloc] init];
    MyNavigationController *navi   = [[MyNavigationController alloc] initWithRootViewController:viewCtr];
    self.window.rootViewController = navi;
    
    //设置状态栏和导航栏
    [self setNavigationBarColor];
    
    //初始化字体大小
    [self initFontsize];
    //获取版本号
//    [self getVersion];

    [self.window makeKeyAndVisible];
    
    return YES;
}


#pragma mark 设置导航条
- (void)setNavigationBarColor{
    
    if([UINavigationBar conformsToProtocol:@protocol(UIAppearanceContainer)]) {
    
        //ios 7之后view的y坐标上移的处理方法
        if ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0) {
            
            self.window.rootViewController.edgesForExtendedLayout=UIRectEdgeNone;
            
        }
        
        //状态栏颜色
        UIView *statusBarView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
        statusBarView.backgroundColor = RGBA(45, 68, 147, 1);//[UIColor blackColor];
        [self.window.rootViewController.view addSubview:statusBarView];

        //导航栏背景图片 68 95 196
//        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"rectangle-1.png"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBarTintColor:RGBA(52, 71, 183, 1)];
        //NSFontAttributeName : [UIFont boldSystemFontOfSize:self.largeFont],
        [[UINavigationBar appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
        
    }
}

#pragma mark - 设置字体大小
-(void)initFontsize{
    
    
    if (iPhone4 || iPhone5 ) {
        self.smallFont = 12;
        self.largeFont = 14;
    }else if (IPHONE6){
        self.smallFont = 12;
        self.largeFont = 16;
    }else if(IPHONE6PLUS){
        self.smallFont = 14;
        self.largeFont = 18;
    }else{
        self.smallFont = 12;
        self.largeFont = 14;
    }
}
#pragma mark 获取版本号
-(void)getVersion{

    
    //或者本地app版本号  version
    NSString* versionnum = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    //bulid
    NSString* versionbuild = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString* appVersionStr = [NSString stringWithFormat:@"%@.%@",versionnum,versionbuild];
    NSMutableArray *arr=[NSMutableArray array];
    NSDictionary *parms = @{@"apptype":@"2"};

    [arr addObject:parms];
    
    [AFSoapRequestOperation getAppVersionSoapRequestWithParams:arr hudShowInView:self.window.rootViewController.view block:^(NSString *jsonStr) {

        if ([[jsonStr JSONValue] valueForKey:@"result"]) {

            if (![appVersionStr isEqualToString:[[jsonStr JSONValue] valueForKey:@"msg"]]) {
                UIAlertView* tips = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您有新版本可以更新，点击确定可以下载！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [tips show];
                
            }
            
        }
      
        
    }];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
       if (buttonIndex == 0) {
        
        NSLog(@"取消");
    }else if (buttonIndex == 1){
        
        NSLog(@"跳转");
        NSString *iTunesLink = @"itms-apps://itunes.apple.com/us/app/guang-huo-bao/id1073520900?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
