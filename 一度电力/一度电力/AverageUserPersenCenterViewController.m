//
//  AverageUserViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/2.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AverageUserPersenCenterViewController.h"
#import "AverageUserTSViewController.h"
#import "YiJianBaoZhangViewController.h"
#import "AccountInfoViewController.h"

@interface AverageUserPersenCenterViewController ()

@end

@implementation AverageUserPersenCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
}

#pragma mark - 初始化视图
-(void)initPersonCenterView{
    
    CGRect frame = CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT);
    self.personCenterView = [[PersonCenterView alloc] initWithFrame:frame
                                                         labelTexts:@[@"一键服务",@"任务状态",@"账户信息"] imageNames:@[@"故障申报",@"工单处理",@"工单处理"]];
    
    [self.view addSubview:self.personCenterView];
    
    WEAKSELF
    weakSelf.personCenterView.HangleImageViewTag = ^(NSInteger imageViewTag){
        
        STRONGSELF
        if (imageViewTag == 0) {//一键报障
            
            NSDictionary *dic = @{@"uid":App_Delegate.userModel.id};
            YiJianBaoZhangViewController *yjbzVC = [[YiJianBaoZhangViewController alloc] init];
            yjbzVC.title = @"一键服务";
            
            [strongSelf.navigationController pushViewController:yjbzVC animated:YES];
//            [AFSoapRequestOperation
//             getCountUnfinishMissionSoapRequestWithParams:@[dic]
//             hudShowInView:weakSelf.view
//             block:^(NSString *jsonStr)  {
//                
//                 NSDictionary *dic = [jsonStr JSONValue];
//                 NSString  *resultString =  dic[@"result"];
//                 if (resultString.intValue == 1) {//可以保障
//         
//                     YiJianBaoZhangViewController *yjbzVC = [[YiJianBaoZhangViewController alloc] init];
//                     yjbzVC.title = @"一键报障";
//                     [strongSelf.navigationController pushViewController:yjbzVC animated:YES];
//
//                 }else{
//                 
//                     [Utils showMessage:@"尊敬的客户,您上次的任务还未完成,请等任务完成并评分后再进行保障!" Delegate:nil];
//                 }
//                
//            }];

            
        }else if(imageViewTag == 1){//任务状态
            
            AverageUserTSViewController *tastVC = [[AverageUserTSViewController alloc] init];
            tastVC.labelTexts = @[@"工单号",@"日期",@"状态"];
            tastVC.mtype = @"2";
            [strongSelf.navigationController pushViewController:tastVC animated:YES];
            
            
        }else if(imageViewTag == 2){//账户信息
        
            NSLog(@"账户信息");
            AccountInfoViewController *accountInfoVC = [[AccountInfoViewController alloc] init];
            [self.navigationController pushViewController:accountInfoVC animated:YES];
        }
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
