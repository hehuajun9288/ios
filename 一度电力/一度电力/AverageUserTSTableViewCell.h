//
//  AverageUserTSTableViewCell.h
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/6.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mission.h"


@interface AverageUserTSTableViewCell : UITableViewCell

-(void)cellData:(Mission*)mission;

@end
