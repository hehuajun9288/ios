//
//  AverageUserTSTableViewCell.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/6.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AverageUserTSTableViewCell.h"

@interface AverageUserTSTableViewCell ()

@property (nonatomic, strong) UILabel *workOrderNunLB;
@property (nonatomic, strong) UILabel *dateLB;
@property (nonatomic, strong) UILabel *statusLB;
@property (nonatomic, strong) UILabel *missionDetailLB;

@end
@implementation AverageUserTSTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self creatCell];
    }
    
    return self;
}


#pragma mark - 创建cell的内容
-(void)creatCell{
    
    
    CGFloat labelWidth = (SCREEN_WIDTH - VIEW_START_X * 2) / 3;
    
    
    CGRect frame = CGRectMake(VIEW_START_X, VIEW_START_Y, labelWidth , 20);
    
     _workOrderNunLB = [UILabel labelWithFrame:frame
                                    LableText:@"CS10001601073515"
                                         size:App_Delegate.largeFont
                                    textColor:RGBA(1, 1, 255, 1)
                                textAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:_workOrderNunLB];
    _workOrderNunLB.lineBreakMode = NSLineBreakByWordWrapping;
    //日期
    frame.origin.x = _workOrderNunLB.X + _workOrderNunLB.WIDTH;
    _dateLB = [UILabel labelWithFrame:frame
                            LableText:@"2015-1-6"
                                 size:App_Delegate.largeFont
                            textColor:[UIColor grayColor]
                        textAlignment:NSTextAlignmentCenter];
    [self.contentView addSubview:_dateLB];
    
    //状态

    frame.origin.x = _dateLB.X + _dateLB.WIDTH;
    _statusLB = [UILabel labelWithFrame:frame
                              LableText:@"完成"
                                   size:App_Delegate.largeFont
                              textColor:BLACK_COLOR
                          textAlignment:NSTextAlignmentCenter];
    [self.contentView addSubview:_statusLB];


    //任务详情
     frame = CGRectMake(VIEW_START_X, _workOrderNunLB.Y + _workOrderNunLB.HEIGTH + 15, SCREEN_WIDTH - VIEW_START_X * 2, 20);
    _missionDetailLB = [UILabel labelWithFrame:frame
                                     LableText:@"时间到了非技术的浪费就死定了"
                                          size:App_Delegate.largeFont
                                     textColor:BLACK_COLOR
                                 textAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:_missionDetailLB];
}

#pragma mark - 设置cell的显示内容
-(void)cellData:(Mission*)mission{

    /**
     private Long id;			//序号
     private String dno;			//任务流水号
     private Long uid;			//用户id
     private int sid;			//站点id
     private Long aid;			//地址id
     private int mtype;			//任务类型
     private int mstatus;		//任务状态
     private Long dutymanid;		//责任人id
     private String description;	//任务描述
     private String ctime;		//任务创建时间
     private String dtime;		//任务完成时间

     */
    
//    CGSize size = [Utils getTextSize:mission.dno
//                          textRegion:CGSizeMake(200, 200)
//                        textFontSize:App_Delegate.largeFont];
//
//    CGRect frame = _workOrderNunLB.frame;
//    frame.size.width = size.width ;
//    _workOrderNunLB.frame = frame;
    _workOrderNunLB.text = mission.dno;
    
    _dateLB.text = [mission.ctime componentsSeparatedByString:@" "][0];
//    frame = _dateLB.frame;
//    frame.origin.x = _workOrderNunLB.X + _workOrderNunLB.WIDTH + 10;
//    _dateLB.frame = frame;
    
    //mstatus	任务状态：0:已审核;1:取消;2:已分派;3:完成;4:已评分;5:申请 6:代办再审核;
    switch (mission.mstatus.intValue) {
        case 0://已审核
            _statusLB.text = @"已审核";
            _statusLB.textColor = [UIColor blackColor];
            break;
        case 1://取消
            _statusLB.text = @"已取消";
            _statusLB.textColor = [UIColor blackColor];
            break;
        case 2://已分派
            _statusLB.text = @"已分派";
            _statusLB.textColor = [UIColor blackColor];
            break;
        case 3://3:完成
            _statusLB.text = @"完成";
            _statusLB.textColor = [UIColor blueColor];

            break;
        case 4://已评分
            _statusLB.text = @"已评分";
            _statusLB.textColor = [UIColor blackColor];
            break;
        case 5://申请
            _statusLB.text = @"已申请";
            _statusLB.textColor = [UIColor blackColor];
            break;
        case 6://申请
            _statusLB.text = @"再审核";
            _statusLB.textColor = [UIColor blackColor];
            break;

        default:
            break;
    }
    
    //根据文本内容设置label的宽高
    NSString *detail = [NSString stringWithFormat:@"故障详情: %@",mission.descri];
    
    //获取文本宽高
    CGSize textSize = [Utils getTextSize:detail
                              textRegion:CGSizeMake(SCREEN_WIDTH - VIEW_START_X * 2, 1000)
                            textFontSize:App_Delegate.largeFont];
    
    CGRect frame = _missionDetailLB.frame;
    frame.size = textSize;
    _missionDetailLB.frame = frame;
    _missionDetailLB.text = detail;
    
    //设置cell的高度
    frame = self.frame;
    frame.size.height = _missionDetailLB.Y + _missionDetailLB.HEIGTH + VIEW_START_Y;
    self.frame = frame;
}

@end
