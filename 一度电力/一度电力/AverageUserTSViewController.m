//
//  AverageUserTSViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/3.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "AverageUserTSViewController.h"
#import "AverageUserTSTableViewCell.h"
#import "Mission.h"
#import "MissionDetailTableViewController.h"

@interface AverageUserTSViewController ()

@property (nonatomic, strong) NSMutableArray *missionArr;
//@property (nonatomic, assign) int offset;  //分页请求数
@end

//请求的页数 从第一页开始
static int page = 1;

@implementation AverageUserTSViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    _missionArr = [NSMutableArray array];
    //自动进入刷新状态
    [self.tableView.mj_header beginRefreshing];
    //[self getMissionsData];
    
}

#pragma mark - 下拉刷新获取任务列表
-(void)getHeaderRefreshMissionsData{

    
    WEAKSELF
    [AFSoapRequestOperation sgetMissionsSoapRequestWithParams:[weakSelf getParams]
                                                hudShowInView:weakSelf.tableView
                                                        block:^(NSString *jsonStr) {
                                                            
                                                            STRONGSELF
                                                            NSArray *missionDics = [jsonStr JSONValue];
                                                            [weakSelf.missionArr removeAllObjects];
                                                            
                                                            //获取mission对象
                                                            for (NSDictionary *missionDic in missionDics) {
                                                                
                                                                Mission *mission = [[Mission alloc] initWithDic:missionDic];
                                                                [strongSelf.missionArr addObject:mission];
                                                            }
                                                            
                                                            [strongSelf.tableView reloadData];
                                                            
                                                            //结束刷新
                                                            [strongSelf.tableView.mj_header endRefreshing];

                                                        }];

}
/**
 *  获取接口要的参数
 *
 *  @param offset 分页开始序号
 *
 *  @return 接口参数
 */
#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    int size = 10;
    int offset = (page - 1) * size;
    NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
    NSDictionary *mtypeDic = @{@"mtype":self.mtype};
    NSDictionary *offsetDic = @{@"offset":[NSString stringWithFormat:@"%d",offset]};
    NSDictionary *sizeDic = @{@"size":[NSString stringWithFormat:@"%d",size]};
    
    return @[uidDic,mtypeDic,offsetDic,sizeDic];
}
#pragma mark - 重写父类下拉刷新和上啦加载更多的方法
-(void)headerWithRefresh{

    page = 1;
    [self getHeaderRefreshMissionsData];
    
    //恢复加载更多
    [self.tableView.mj_footer resetNoMoreData];
}
-(void)footerWithRefresh{

    page ++ ;
    [self getFooterRefreshMissionsData];
}

#pragma mark - 加载更多
-(void)getFooterRefreshMissionsData{

    WEAKSELF
    [AFSoapRequestOperation sgetMissionsSoapRequestWithParams:[weakSelf getParams]
                                                hudShowInView:weakSelf.view
                                                        block:^(NSString *jsonStr) {
                                                            
                                                            STRONGSELF
                                                            NSArray *missionDics = [jsonStr JSONValue];
                                                            
                                                            //全部加载完毕
                                                            if (missionDics.count == 0) {
                                                                
                                                                [strongSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                                return ;
                                                            }
                                                            for (NSDictionary *missionDic in missionDics) {
                                                                
                                                                Mission *mission = [[Mission alloc] initWithDic:missionDic];
                                                                [strongSelf.missionArr addObject:mission];
                                                            }
                                                            
                                                            [strongSelf.tableView reloadData];
                                                            [strongSelf.tableView.mj_footer endRefreshing];
                                                            
                                                        }];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.missionArr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    AverageUserTSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AverageUserTSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor cyanColor];
    }
    
    Mission *mission = _missionArr[indexPath.row];
    [cell cellData:mission];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    AverageUserTSTableViewCell *cell = (AverageUserTSTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    Mission *mission = _missionArr[indexPath.row];
    [cell cellData:mission];
    
    return cell.HEIGTH;
}

#pragma mark - tableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{


    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Mission *mission = _missionArr[indexPath.row];

    //mstatus	任务状态：0:已审核;1:取消;2:已分派;3:完成;4:已评分;5:申请;
    //只有完成的情况下才可以进入评分界面
    
    if (mission.mstatus.intValue == 3) {
        
        MissionDetailTableViewController *mdTVC = [[MissionDetailTableViewController alloc] init];
        mdTVC.missionDno = mission.dno;
        mdTVC.FinishReqeust = ^(BOOL finishRequest){
        
            if (finishRequest) {
                
                [self.tableView.mj_header beginRefreshing];
            }
        };
        [self.navigationController pushViewController:mdTVC animated:YES];

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
