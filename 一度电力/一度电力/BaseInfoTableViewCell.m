//
//  ChangePasswordTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/27.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "BaseInfoTableViewCell.h"

@implementation BaseInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
     
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        switch (row) {
          case 0:
                break;
            case 1://登录名
                _loginNameTF = [self cellView:row imageName:@"account" tfPlacehold:@"登录名" isHasLable:YES];
                _loginNameTF.text = App_Delegate.userModel.loginname;
                _loginNameTF.keyboardType = UIKeyboardTypeNamePhonePad;

                break;
            case 2://姓名
                _userNameTF = [self cellView:row imageName:@"account" tfPlacehold:@"姓名" isHasLable:YES];
                _userNameTF.text = App_Delegate.userModel.realname;
                
                break;
            case 3://手机号

                _phoneNumTF = [self cellView:row imageName:@"手机号" tfPlacehold:@"手机号码" isHasLable:YES];
                _phoneNumTF.text = App_Delegate.userModel.mobile;
                _phoneNumTF.keyboardType = UIKeyboardTypeNumberPad;
                break;
              case 4:
                [self creatChangeButton];
                break;
            default:
                break;
        }

    }
    
    return self;
}

#pragma mark - 创建修改按钮
-(void)creatChangeButton{
    
    
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    _changInfoBtn = [UIButton buttonWithFrame:frame
                                                title:@"修改"
                                             fontSize:App_Delegate.largeFont
                                           titleColor:WHITE_COLOR
                                               action:nil
                                               target:nil tag:0
                                      backgroundColor:BUTTON_BACKGROUND_COLOR];
    [self.contentView addSubview:_changInfoBtn];
    
}


@end
