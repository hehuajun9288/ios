//
//  ChangePasswordViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/27.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "BasiInfoViewController.h"
#import "BaseInfoTableViewCell.h"

@interface BasiInfoViewController ()

@property (nonatomic, strong) BaseInfoTableViewCell *loginCell;
@property (nonatomic, strong) BaseInfoTableViewCell *userNameCell;
@property (nonatomic, strong) BaseInfoTableViewCell *phoneNumCell;

@end

@implementation BasiInfoViewController


-(void)resignFirstResponderForSelf{

    [_loginCell.loginNameTF resignFirstResponder];
    [_userNameCell.userNameTF resignFirstResponder];
    [_phoneNumCell.phoneNumTF resignFirstResponder];

}
-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    [self resignFirstResponderForSelf];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"基本信息";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    
    [self initTableView];
 }

-(void)initTableView{

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT) style:UITableViewStylePlain];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    
    [self.view addSubview:_tableView];

}
#pragma mark - tableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 5;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    BaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[BaseInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.changInfoBtn addTarget:self action:@selector(changeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return TABLE_VIEW_HEIGHT_FOR_ROW;
}


#pragma mark - 修改按钮方法
-(void)changeBtnAction{

    [self initBaseInfoCell];
    [self resignFirstResponderForSelf];
    
    if ([self checkInfo]) {
        
        /**
         *   <id>?</id>
         <!--Optional:-->
         <loginname>?</loginname>
         <!--Optional:-->
         <realname>?</realname>
         */
        NSDictionary *idDic = @{@"id":App_Delegate.userModel.id};
        NSDictionary *loginNameDic = @{@"loginname":_loginCell.loginNameTF.text};
        NSDictionary *realnameDic = @{@"realname":_userNameCell.userNameTF.text};
        NSDictionary *mobileDic = @{@"mobile":_phoneNumCell.phoneNumTF.text};

        WEAKSELF
        [AFSoapRequestOperation modifyInfoSoapRequestWithParams:@[idDic,loginNameDic,realnameDic,mobileDic] hudShowInView:weakSelf.view block:^(NSString *jsonStr) {
            
            STRONGSELF
            NSDictionary *resultDic = [jsonStr JSONValue];
            NSString *result = resultDic[@"result"];
            if (result.intValue == 1) {
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您的信息修改成功..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [strongSelf popViewController];
                }];
                
            }else{
            
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您的信息修改失败,清稍后再试..." message:nil cancelButtonTitle:@"确定" otherButtonTitles: nil handler:nil];
            }
            
        }];
    }
}

-(void)popViewController{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 判断输入信息
-(BOOL)checkInfo{

    if (_loginCell.loginNameTF.text.length < 5) {
        
        [self showHudWithText:@"登录名不能少于5个字符"];
        return NO;
        
    }else if (_userNameCell.userNameTF.text.length <= 0){
        
        [self showHudWithText:@"姓名不能为空"];
        return NO;
        
    }else if (![Utils isValidateMobile:_phoneNumCell.phoneNumTF.text]){
    
        [self showHudWithText:@"手机号输入错误"];
        return NO;
        
    }else if ([_loginCell.loginNameTF.text isEqualToString:App_Delegate.userModel.loginname] && [_userNameCell.userNameTF.text isEqualToString:App_Delegate.userModel.realname] &&[_phoneNumCell.phoneNumTF.text isEqualToString:App_Delegate.userModel.mobile]){
    
        [self showHudWithText:@"您还没做任何更改"];
        return NO;
    }
    
    return YES;
}

-(void)showHudWithText:(NSString*)text{

    [Utils showHUD:text autoHide:YES inView:self.view];
}
-(void)initBaseInfoCell{

    _loginCell = [self cellForRow:1];
    _userNameCell = [self cellForRow:2];
    _phoneNumCell = [self cellForRow:3];
}

#pragma mark - 根据row获取tableView的cell
-(BaseInfoTableViewCell*)cellForRow:(NSInteger)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    BaseInfoTableViewCell *cell = (BaseInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
