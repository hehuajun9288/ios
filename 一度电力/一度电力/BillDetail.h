//
//  BillDetail.h
//  一度电力
//
//  Created by 廖幸杰 on 16/4/24.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BillDetail : NSObject

/**
 *  private Long id;
	private Long uid;		//用户id
	private String uno;		//用户编号
	private String realname;	//用户姓名
	private String mobile;	//手机
	private String address;	//地址
	private String logtype;	//交易类型 1：充值，2：消费
	private String info;	//记录具体信息
	private BigDecimal overage;	//交易金额
	private String rtime;	//交易时间
 */

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *uno;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *logtype;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSString *rtime;
@property (nonatomic, strong) NSString *overage;

@end
