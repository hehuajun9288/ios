//
//  CTMBHUDManager.h
//  畅停
//
//  Created by wff on 15/10/4.
//  Copyright © 2015年 HCT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface CTMBHUDManager : NSObject

/**
 *  HUD 显示
 *
 *  @param showStr    加载信息
 *  @param mode       加载模式
 *  @param autoHide   是否隐藏
 *  @param afterDelay 隐藏延迟时间
 *  @param yesOrNo    界面是否交互
 */
+ (void)showHUDWithToShowStr:(NSString *)showStr
                     HUDMode:(MBProgressHUDMode)mode
                    autoHide:(BOOL)autoHide
                  afterDelay:(NSTimeInterval)afterDelay
      userInteractionEnabled:(BOOL)yesOrNo
                      inView:(UIView*)view;


/*;
 *  HUD 隐藏
 */
+ (void)hideHUD;

@end
