//
//  CTMBHUDManager.m
//  畅停
//
//  Created by wff on 15/10/4.
//  Copyright © 2015年 HCT. All rights reserved.
//

#import "CTMBHUDManager.h"

@interface CTMBHUDManager (){

}

@end

static MBProgressHUD *HUD;
@implementation CTMBHUDManager

+ (void)showHUDWithToShowStr:(NSString *)showStr
                     HUDMode:(MBProgressHUDMode)mode
                    autoHide:(BOOL)autoHide
                  afterDelay:(NSTimeInterval)afterDelay
      userInteractionEnabled:(BOOL)yesOrNo
            inView:(UIView*)view{
    if (!HUD){
    
        HUD                        = [[MBProgressHUD alloc] initWithFrame:view.bounds];
        HUD.backgroundColor        = CLEAR_COLOR;
        [view addSubview:HUD];
    }
        HUD.userInteractionEnabled = !yesOrNo;
        HUD.mode                   = mode;
        HUD.labelText              = showStr;
        HUD.margin                 = 10.f;
    //   HUD.removeFromSuperViewOnHide = YES;
        [HUD show:YES];
        [view addSubview:HUD];
    
        if (autoHide){
            HUD.userInteractionEnabled = NO;
            HUD.mode                   = MBProgressHUDModeText;
            [HUD hide:YES afterDelay:afterDelay];
    }
}

+ (void)hideHUD{
    [HUD hide:YES afterDelay:0.5];
}

@end
