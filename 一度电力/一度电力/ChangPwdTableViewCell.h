//
//  ChangPwdTableViewCell.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/27.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "MyTableViewCell.h"

@interface ChangPwdTableViewCell : MyTableViewCell



@property (nonatomic, strong) UITextField *oldPwdTF;
@property (nonatomic, strong) UITextField *aNewPwdTF;
@property (nonatomic, strong) UITextField *repeatPwdTF;

@property (nonatomic, strong) UIButton *commiteBtn ;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row;

@end
