//
//  ChangPwdTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/27.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "ChangPwdTableViewCell.h"

@implementation ChangPwdTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        switch (row) {
            case 0:
                break;
            case 1://旧密码
                _oldPwdTF = [self cellView:row imageName:@"密码" tfPlacehold:@"旧密码" isHasLable:YES];
                _oldPwdTF.secureTextEntry = YES;

                break;
            case 2://新密码
                _aNewPwdTF = [self cellView:row imageName:@"密码" tfPlacehold:@"新密码" isHasLable:YES];
                _aNewPwdTF.secureTextEntry = YES;

                break;
            case 3://重复密码
                
                _repeatPwdTF = [self cellView:row imageName:@"密码" tfPlacehold:@"重复密码" isHasLable:YES];
                _repeatPwdTF.secureTextEntry = YES;

                break;
            case 4:
               _commiteBtn =  [self creatButtonWhthTitle:@"提交"];
            
                break;
            default:
                break;
        }
        
    }
    
    return self;
}

@end
