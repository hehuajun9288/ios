//
//  ChangePwdTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/27.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "ChangePwdTableViewController.h"
#import "ChangPwdTableViewCell.h"
#import "ModifyPwd.h"
#import "LoginViewController.h"

@interface ChangePwdTableViewController ()

@property (nonatomic, strong) ChangPwdTableViewCell *oldPwdCell;
@property (nonatomic, strong) ChangPwdTableViewCell *anewPwdCell;
@property (nonatomic, strong) ChangPwdTableViewCell *repeastPwdCell;

@end

@implementation ChangePwdTableViewController


-(void)resignFirstResponderForSelf{
    
    [_oldPwdCell.oldPwdTF resignFirstResponder];
    [_anewPwdCell.aNewPwdTF resignFirstResponder];
    [_repeastPwdCell.repeatPwdTF resignFirstResponder];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    [self resignFirstResponderForSelf];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"修改密码";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    NSLog(@"password = %@",App_Delegate.password);
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

#pragma mark - tableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 5;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    ChangPwdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[ChangPwdTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.commiteBtn addTarget:self action:@selector(commiteBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return TABLE_VIEW_HEIGHT_FOR_ROW;
}


#pragma mark -commiteBtnAction
-(void)commiteBtnAction{

    [self initChangePwdCell];
    [self resignFirstResponderForSelf];
    
    if ([self checkInfo]) {
        
        NSDictionary *idDic = @{@"id":App_Delegate.userModel.id};
        NSDictionary *pwdDic = @{@"password":_anewPwdCell.aNewPwdTF.text};
        
        WEAKSELF
        [AFSoapRequestOperation modifyPwdSoapRequestWithParams:@[idDic,pwdDic] hudShowInView:self.view block:^(NSString *jsonStr) {
            
            STRONGSELF
            ModifyPwd *mp = [ModifyPwd mj_objectWithKeyValues:[jsonStr JSONValue]];
            
            if (mp.result.intValue == 1) {
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您修改密码成功!请重新登录..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [strongSelf popToViewController];
                }];
                
                
            }else{
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您修改密码失败!请与客服联系!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];

            
            }
        }];
    }
}

#pragma mark - pop LoginViewController界面
-(void)popToViewController{

    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[LoginViewController class]]) {
            
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}
#pragma mark - 检查输入信息是否有误
-(BOOL)checkInfo{

    if (_oldPwdCell.oldPwdTF.text.length <= 0) {
        
        [self showHUDWithHintText:@"旧密码不能为空"];
        return NO;
        
    }else if (![_oldPwdCell.oldPwdTF.text isEqualToString:App_Delegate.password]){
    
        [self showHUDWithHintText:@"旧密码输入错误"];
        return NO;
    }else if (_anewPwdCell.aNewPwdTF.text.length < 6){
    
        [UIAlertView bk_showAlertViewWithTitle:PWD_ALERT_TEXT message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
        
        return NO;
    }else if (_repeastPwdCell.repeatPwdTF.text.length <= 0){
    
        [self showHUDWithHintText:@"请重复输入密码"];
        return NO;
    }else if (![_anewPwdCell.aNewPwdTF.text isEqualToString:_repeastPwdCell.repeatPwdTF.text]){
    
        [self showHUDWithHintText:@"输入密码不一致"];
        return NO;
    }
    
    return YES;
}

-(void)showHUDWithHintText:(NSString*)hintText{
    
    [Utils showHUD:hintText autoHide:YES inView:self.view];
    
}

-(void)initChangePwdCell{
    
    _oldPwdCell = [self cellForRow:1];
    _anewPwdCell = [self cellForRow:2];
    _repeastPwdCell = [self cellForRow:3];
}

#pragma mark - 根据row获取tableView的cell
-(ChangPwdTableViewCell*)cellForRow:(NSInteger)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    ChangPwdTableViewCell *cell = (ChangPwdTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
