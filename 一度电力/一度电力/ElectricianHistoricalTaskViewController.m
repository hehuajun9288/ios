//
//  ElectricianHistoricalTaskViewController.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/4/26.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "ElectricianHistoricalTaskViewController.h"

@interface ElectricianHistoricalTaskViewController ()

@end

@implementation ElectricianHistoricalTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Mission *mission = self.missions[indexPath.row];
    HistoryTastDetailTableViewController *htdTVC = [[HistoryTastDetailTableViewController alloc] init];
    htdTVC.dno = mission.dno;
    [self.navigationController pushViewController:htdTVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
