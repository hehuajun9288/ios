//
//  ElectricianViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/2.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "ElectricianPersenCenterViewController.h"
#import "ElectricianTSViewController.h"
#import "ElectricianWorkOrderProcessingViewController.h"
#import "ElectricianHistoricalTaskViewController.h"


@interface ElectricianPersenCenterViewController ()

@end

@implementation ElectricianPersenCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}
#pragma mark - 初始化视图
-(void)initPersonCenterView{
    
    CGRect frame = CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT);
    
    self.personCenterView = [[PersonCenterView alloc] initWithFrame:frame
                                                         labelTexts:@[@"接工单",@"工单处理",@"历史任务"]
                                                         imageNames:@[@"接工单",@"工单处理",@"工单处理"]];
    [self.view addSubview:self.personCenterView];
    
    WEAKSELF
    weakSelf.personCenterView.HangleImageViewTag = ^(NSInteger imageViewTag){
        
        STRONGSELF //有self的地方要把注释打开 用strongself 代替self 防止弱引用
        if (imageViewTag == 0) {//接工单
            
            ElectricianTSViewController *elecVC = [[ElectricianTSViewController alloc] init];
            [strongSelf.navigationController pushViewController:elecVC animated:YES];
            
        }else if(imageViewTag == 1){//工单处理
            
            ElectricianWorkOrderProcessingViewController *elecVC = [[ElectricianWorkOrderProcessingViewController alloc] init];
            [strongSelf.navigationController pushViewController:elecVC animated:YES];
            
        }else if (imageViewTag == 2){//历史任务
            

            [strongSelf.navigationController pushViewController:[[ElectricianHistoricalTaskViewController alloc] init] animated:YES];
        }
    };
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
