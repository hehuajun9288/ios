//
//  ElectricianTSViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/3.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "TastStateTableViewController.h"
#import "Mission.h"
#import "AccountTableViewCell.h"

@interface ElectricianTSViewController : TastStateTableViewController

@property (nonatomic, strong) NSString *mtype;
@property (nonatomic, strong) NSMutableArray *missionArr;

//-(NSArray*)getParams:(NSString*)id;

-(void)getFooterRefreshMissionsData:(NSString*)id;
-(void)getHeaderRefreshMissionsData:(NSString*)id;

@end
