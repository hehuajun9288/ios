//
//  ElectricianTaskDeatailViewController.h
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "BasiInfoViewController.h"
#import "TsakDeatailCell.h"
#import "Mission.h"
@interface ElectricianTaskDeatailViewController : UITableViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
>
@property(nonatomic,strong)Mission* receiveMissionModel;
@property(nonatomic,strong)NSString* jsonAddress;
@property(nonatomic,strong)NSString* jsonMobile;
@property(nonatomic,strong)NSString* ExpectedDuration;//预计工期
@property(nonatomic,strong)NSString* ExpectedFee;//预计费用
@property (nonatomic, copy) void (^FinishReqeust)(BOOL finishRequest);
@end
