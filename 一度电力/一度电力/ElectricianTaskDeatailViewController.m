//
//  ElectricianTaskDeatailViewController.m
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//
#define BUUTTON_WEIGHT  100
#import "ElectricianTaskDeatailViewController.h"

@interface ElectricianTaskDeatailViewController ()

@property (nonatomic, strong) NSArray *leftLBTextArr;
@property (nonatomic, strong) NSMutableArray *rightLBTextArr;

@end

@implementation ElectricianTaskDeatailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"工单任务评估";
    self.tableView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _rightLBTextArr = [NSMutableArray array];
    UIView* footView = [self tableFootView];
    self.tableView.tableFooterView = footView;
    [self RequestData];
    
}

/**
 *  获取接口要的参数
 *
 *  @param offset 分页开始序号
 *
 *  @return 接口参数
 */
#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    
    
    NSDictionary *dnoDic = @{@"dno":self.receiveMissionModel.dno};
    return @[dnoDic];
}


-(void)RequestData{
    WEAKSELF
    [AFSoapRequestOperation ElectricianGetMissionDeatailSoapRequestWithParams:[weakSelf getParams]
                                                             hudShowInView:weakSelf.view
                                                                     block:^(NSString *jsonStr) {
                                                                         
                                                                         STRONGSELF
                                                                         
                                                                         strongSelf.leftLBTextArr = @[@"编号",@"用户名",@"地址",@"申请时间",@"电话",@"任务描述",@"预估工期(天)",@"预估费用(元)"];
                                                                         NSDictionary *dic = [jsonStr JSONValue];
                                                                         
                                                                         //编号
                                                                         [strongSelf.rightLBTextArr addObject:strongSelf.receiveMissionModel.dno];
                                       
                                         //用户名
                                                                         [strongSelf.rightLBTextArr addObject:[dic valueForKey:@"realname"]];
                                                                         //地址
                                                                         [strongSelf.rightLBTextArr addObject:[dic valueForKey:@"address"]];

                                                                         //时间
                                                                         [strongSelf.rightLBTextArr addObject:strongSelf.receiveMissionModel.ctime];

                                                                         //手机号
                                                                         [strongSelf.rightLBTextArr addObject:[dic valueForKey:@"mobile"]];

                                                                         //故障描述
                                                                         [strongSelf.rightLBTextArr addObject:strongSelf.receiveMissionModel.descri];


                                                                         [strongSelf.rightLBTextArr addObject:@"33"];
                                                                         [strongSelf.rightLBTextArr addObject:@""];
                                                                         [strongSelf.tableView reloadData];
                                                                     }];
   
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSString *cellID = @"Cell";
    TsakDeatailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[TsakDeatailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (_rightLBTextArr.count > 0 && _leftLBTextArr.count > 0) {
        [cell cellData:_leftLBTextArr[indexPath.row] rightLBText:_rightLBTextArr[indexPath.row]];
        
    }

    if (indexPath.row == 6) {
        cell.rightLB.hidden = YES;
 
        UITextField* ExpectedTimeTXT = [[UITextField alloc]initWithFrame:CGRectMake(cell.rightLB.frame.origin.x, cell.leftLB.frame.origin.y + 9, cell.frame.size.width - BUUTTON_WEIGHT, cell.leftLB.frame.size.height)];
        ExpectedTimeTXT.keyboardType = UIKeyboardTypeNumberPad;
        ExpectedTimeTXT.placeholder = @"请输入预计工期";
        if (self.ExpectedDuration) {
        ExpectedTimeTXT.text = self.ExpectedDuration;
        }
        ExpectedTimeTXT.tag = 8000;
        ExpectedTimeTXT.delegate = self;
        if ([ExpectedTimeTXT isFirstResponder]) {
            [ExpectedTimeTXT resignFirstResponder];
        }
        [cell addSubview:ExpectedTimeTXT];
    }else if (indexPath.row == 7){
        cell.rightLB.hidden = YES;
        UITextField* ExpectedMoneyTXT = [[UITextField alloc]initWithFrame:CGRectMake(cell.rightLB.frame.origin.x, cell.leftLB.frame.origin.y + 9, cell.frame.size.width - BUUTTON_WEIGHT, cell.leftLB.frame.size.height)];
        ExpectedMoneyTXT.placeholder = @"请输入预估费用";
        ExpectedMoneyTXT.keyboardType = UIKeyboardTypeDecimalPad;

        if (self.ExpectedFee) {
        ExpectedMoneyTXT.text = self.ExpectedFee;
        }
        ExpectedMoneyTXT.tag = 8001;
        ExpectedMoneyTXT.delegate = self;
        if ([ExpectedMoneyTXT isFirstResponder]) {
            [ExpectedMoneyTXT resignFirstResponder];
        }
        [cell addSubview:ExpectedMoneyTXT];
    }


    return cell;

}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//
//    return [self tableFootView].HEIGTH;
//
//}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    
//    return  [self tableFootView];
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (_rightLBTextArr.count > 0 && _leftLBTextArr.count > 0) {
        
        TsakDeatailCell *cell = (TsakDeatailCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        [cell cellData:_leftLBTextArr[indexPath.row] rightLBText:_rightLBTextArr[indexPath.row]];
        
        return cell.HEIGTH;
    }
    
    return TABLE_VIEW_HEIGHT_FOR_ROW;
}

-(UIView*)tableFootView{

    UIView* headerView = [[UIView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, TABLE_VIEW_HEIGHT_FOR_ROW)];
    headerView.backgroundColor = [UIColor clearColor];
    //self.myTableView.tableHeaderView = headerView;
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    
//    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y , BUUTTON_WEIGHT, headerView.HEIGTH - VIEW_START_Y * 2);
    UIButton* acceptBtn = [UIButton buttonWithFrame:frame
                                              title:@"提交"
                                           fontSize:App_Delegate.largeFont
                                         titleColor:WHITE_COLOR
                                             action:@selector(clickBtnAction:)
                                             target:self
                                                tag:2000
                                    backgroundColor:BUTTON_BACKGROUND_COLOR];
//    acceptBtn.layer.masksToBounds = YES;
//    acceptBtn.layer.cornerRadius = 25;
//    [acceptBtn.layer setBorderWidth:0.5]; //边框宽度
//    acceptBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    CGRect Secframe = CGRectMake(self.view.frame.size.width - CONTENT_VIEW_START_X - BUUTTON_WEIGHT , VIEW_START_Y , BUUTTON_WEIGHT, headerView.HEIGTH - VIEW_START_Y * 2);
    UIButton* cancleBtn = [UIButton buttonWithFrame:Secframe
                                              title:@"取消"
                                           fontSize:App_Delegate.largeFont
                                         titleColor:WHITE_COLOR
                                             action:@selector(clickBtnAction:)
                                             target:self
                                                tag:2001
                                    backgroundColor:[UIColor clearColor]];
    cancleBtn.layer.masksToBounds = YES;
    cancleBtn.layer.cornerRadius = 25;
    [cancleBtn.layer setBorderWidth:0.5]; //边框宽度
    cancleBtn.layer.borderColor=[UIColor whiteColor].CGColor;

    
    [headerView addSubview:acceptBtn];
 //   [headerView addSubview:cancleBtn];
    return headerView;

}
#pragma mark - UItextfiledDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{

    if (textField.tag == 8000) {
        self.ExpectedDuration = textField.text;
    }else{
    
        self.ExpectedFee = textField.text;
    }


}
/**
 *  获取接口要的参数
 *
 *  @param offset 分页开始序号
 *
 *  @return 接口参数
 */
#pragma mark - 设置接口参数
-(NSArray*)getParamsWithOffset:(int)offset{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    
    NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
    NSDictionary *dnoDic = @{@"dno":self.receiveMissionModel.dno};
    NSDictionary* fee = @{@"vfee":self.ExpectedFee};
    NSDictionary* Vtime = @{@"vtime":self.ExpectedDuration};
    return @[uidDic,dnoDic,fee,Vtime];
}


-(void)clickBtnAction:(UIButton* )sender{

    UIButton* btn = sender;
    switch (btn.tag) {
        case 2000:
        {
            NSLog(@"接受");
            [self.tableView reloadData];
            if (self.ExpectedDuration.length == 0) {
                [Utils showHUD:@"请输入预估工期" autoHide:YES inView:self.view];
                return;
            }else if (self.ExpectedFee.length == 0){
                [Utils showHUD:@"请输入预估费用" autoHide:YES inView:self.view];
                return;
            }else{
            
                
                WEAKSELF
                [AFSoapRequestOperation ElectricianReceiveMissionSoapRequestWithParams:[weakSelf getParamsWithOffset:0]
                                                                         hudShowInView:weakSelf.view
                                                                                 block:^(NSString *jsonStr) {
                                                                                     
                                                                                     STRONGSELF
                                                                                     
                                                                                     NSDictionary *dic = [jsonStr JSONValue];
                                                                                     
                                                                                     NSString *result = dic[@"result"];
                                                                                     
                                                                                     if (result.intValue == 1) {
                                                                                         
                                                                                         [UIAlertView bk_showAlertViewWithTitle:@"您已经接受该任务" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                                             
                                                                                             [strongSelf.navigationController popViewControllerAnimated:YES];
                                                                                             if (strongSelf.FinishReqeust) {
                                                                                                 
                                                                                                 strongSelf.FinishReqeust(YES);
                                                                                                 
                                                                                             }
                                                                                         }];
                                                                                     }else{
                                                                                         
                                                                                         [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                                                     }
                                                                                     
                                                                                 }];
            
            }
            
            
        }
            break;
        case 2001:
        {
            NSLog(@"取消");
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
