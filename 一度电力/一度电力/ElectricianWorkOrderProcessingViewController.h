//
//  ElectricianWorkOrderProcessingViewController.h
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "TastStateTableViewController.h"
#import "WorkOrderCell.h"
#import "WordOrderDeatailViewController.h"
#import "Mission.h"
#import "AccountTableViewCell.h"

@interface ElectricianWorkOrderProcessingViewController : TastStateTableViewController
@property (nonatomic, strong) NSMutableArray *missionArr;
@end
