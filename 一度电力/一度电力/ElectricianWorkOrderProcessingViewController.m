//
//  ElectricianWorkOrderProcessingViewController.m
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "ElectricianWorkOrderProcessingViewController.h"

@interface ElectricianWorkOrderProcessingViewController ()

@end
static int offset = 1;

@implementation ElectricianWorkOrderProcessingViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"工单处理单";
    _missionArr = [NSMutableArray array];
    //自动进入刷新状态
    [self.tableView.mj_header beginRefreshing];
   
}

#pragma mark - 下拉刷新获取任务列表
-(void)getHeaderRefreshMissionsData{
    
    
    WEAKSELF
    [AFSoapRequestOperation ElectricianGetMissionSoapRequestWithParams:[weakSelf getParamsWithOffset:offset]
                                                         hudShowInView:weakSelf.view
                                                                 block:^(NSString *jsonStr) {
                                                                     
                                                                     STRONGSELF
                                                                     
                                                                     
                                                                     NSArray *missionDics = [jsonStr JSONValue];
                                                                     [weakSelf.missionArr removeAllObjects];
                                                                     
                                                                     //获取mission对象
                                                                     for (NSDictionary *missionDic in missionDics) {
                                                                         
                                                                         Mission *mission = [[Mission alloc] initWithDic:missionDic];
                                                                         NSArray* timeArr = [mission.ctime componentsSeparatedByString:@" "];
                                                                         mission.ctime = timeArr[0];
                                                                         [strongSelf.missionArr addObject:mission];
                                                                     }
                                                                     
                                                                     [strongSelf.tableView reloadData];
                                                                     
                                                                     //结束刷新
                                                                     [strongSelf.tableView.mj_header endRefreshing];
                                                                     
                                                                 }];
    
}
/**
 *  获取接口要的参数
 *
 *  @param offset 分页开始序号
 *
 *  @return 接口参数
 */
#pragma mark - 设置接口参数
-(NSArray*)getParamsWithOffset:(int)offset{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    int size = 10;
    int page = (offset - 1) * size;
    NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
    NSDictionary *mtypeDic = @{@"mstatus":@"2"};
    NSDictionary *offsetDic = @{@"offset":[NSString stringWithFormat:@"%d",page]};
    NSDictionary *sizeDic = @{@"size":[NSString stringWithFormat:@"%d",size]};
    return @[uidDic,mtypeDic,offsetDic,sizeDic];
}
#pragma mark - 重写父类下拉刷新和上啦加载更多的方法
-(void)headerWithRefresh{
    
    offset = 1;
    [self getHeaderRefreshMissionsData];
    //恢复加载更多
    [self.tableView.mj_footer resetNoMoreData];
}
-(void)footerWithRefresh{
    
    offset ++ ;
    [self getFooterRefreshMissionsData];
}

#pragma mark - 加载更多
-(void)getFooterRefreshMissionsData{
    
    WEAKSELF
    [AFSoapRequestOperation ElectricianGetMissionSoapRequestWithParams:[weakSelf getParamsWithOffset:offset]
                                                         hudShowInView:weakSelf.view
                                                                 block:^(NSString *jsonStr) {
                                                                     
                                                                     STRONGSELF
                                                                     
                                                                     NSArray *missionDics = [jsonStr JSONValue];
                                                                     if (missionDics.count == 0) {//没有数据返回时提示全部加载完毕
                                                                         
                                                                         [strongSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                                         return ;
                                                                     }
                                                                     for (NSDictionary *missionDic in missionDics) {
                                                                         
                                                                         Mission *mission = [[Mission alloc] initWithDic:missionDic];
                                                                         NSArray* timeArr = [mission.ctime componentsSeparatedByString:@" "];
                                                                         mission.ctime = timeArr[0];
                                                                         
                                                                         [strongSelf.missionArr addObject:mission];
                                                                     }
                                                                     
                                                                     [strongSelf.tableView reloadData];
                                                                     [strongSelf.tableView.mj_footer endRefreshing];
                                                                     
                                                                 }];
    
    
}

#pragma mark ========  tableviewdelegate ============
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.missionArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    AccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AccountTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    Mission *mission = _missionArr[indexPath.row];
    cell.typeLabel.text = mission.dno;
    cell.dateLable.text = mission.ctime;
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    return [self creatTableHeaderView:@[@"工单号",@"日期"]];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return [self creatTableHeaderView:@[@"工单号",@"日期"]].HEIGTH;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Mission *mission = _missionArr[indexPath.row];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WordOrderDeatailViewController *deatailVC = [[WordOrderDeatailViewController alloc] init];
    deatailVC.receiveMissionModel = mission;
    deatailVC.FinishReqeust = ^(BOOL finishRequest){
        
        if (finishRequest) {
            
            [self.tableView.mj_header beginRefreshing];
        }
    };
    [self.navigationController pushViewController:deatailVC animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
