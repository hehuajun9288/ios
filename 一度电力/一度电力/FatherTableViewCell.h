//
//  FatherTableViewCell.h
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FatherTableViewCell : UITableViewCell

/**
 *  创建cell的内容
 *
 *  @param row         tableView row
 *  @param tfPlacehold TF占位符
 *  @param isLable     是否要显示lable
 *
 *  @return return
 */
-(UITextField*)cellView:(NSInteger)row
            tfPlacehold:(NSString*)tfPlacehold
             isHasLable:(BOOL)isHasLable;
/**
 *  创建cell的内容
 *
 *  @param row         tableView row
 *  @param tfPlacehold TF占位符
 *  @param isLable     是否要显示lable
 *
 *  @return return
 */
-(UILabel*)cellView:(NSInteger)row
            tfLabel:(NSString*)tflabel
         isHasLable:(BOOL)isHasLable;

@end
