//
//  FatherTableViewCell.m
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "FatherTableViewCell.h"

@implementation FatherTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 *  创建cell的内容
 *
 *  @param row         tableView row
 *  @param tfPlacehold TF占位符
 *  @param isLable     是否要显示lable
 *
 *  @return return
 */
-(UITextField*)cellView:(NSInteger)row
            tfPlacehold:(NSString*)tfPlacehold
             isHasLable:(BOOL)isHasLable{

    UIView *cellView = [[UIView alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:cellView];
    
    //白色背景图片
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
    UIImage *image = [UIImage imageNamed:@"content"];
    UIImage *resizeImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
    iv.image = resizeImage;
    iv.userInteractionEnabled = YES;
    [cellView addSubview:iv];
    
    
    return [self creatContentView:iv
                 tfPlacehold:tfPlacehold
                              row:row
                       isHasLable:(BOOL)isHasLable];

}
/**
 *  创建内容视图
 *
 *  @param imageView   内容控件的父视图
 *  @param imageName   图片名称
 *  @param tfPlacehold 占位符
 *  @param row         行
 */
-(UITextField*)creatContentView:(UIImageView*)imageView
                    tfPlacehold:(NSString*)tfPlacehold
                            row:(NSInteger)row
                     isHasLable:(BOOL)isHasLable{
    CGRect tfFrame;
    
    if (isHasLable) {//图片后添加一个label显示文本
        
        CGSize size = [Utils getTextSize:tfPlacehold textRegion:CGSizeMake(200, 100) textFontSize:App_Delegate.largeFont];
        
        CGRect lbFrame = CGRectMake(VIEW_START_X , 0, size.width + 10, imageView.HEIGTH);
        
        UILabel *label = [UILabel labelWithFrame:lbFrame
                                       LableText:tfPlacehold
                                            size:App_Delegate.largeFont
                                       textColor:[UIColor lightGrayColor]
                                   textAlignment:NSTextAlignmentLeft];
        [imageView addSubview:label];
        
        tfFrame.origin.x = label.X + label.WIDTH ;
        tfFrame.size.width = imageView.WIDTH  - label.WIDTH - VIEW_START_X*2 ;
        tfFrame.origin.y = 0;
        tfFrame.size.height = imageView.HEIGTH;
    }
    UITextField *tf = [UITextField textFieldWithFrame:tfFrame
                                            placehold:tfPlacehold
                                               target:self
                                                  tag:row];
    [imageView addSubview:tf];
    
    return tf;
    
}

/**
 *  创建cell的内容
 *
 *  @param row         tableView row
 *  @param tfPlacehold TF占位符
 *  @param isLable     是否要显示lable
 *
 *  @return return
 */
-(UILabel*)cellView:(NSInteger)row
            tfLabel:(NSString*)tflabel
         isHasLable:(BOOL)isHasLable{
    
    
    UIView *cellView = [[UIView alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:cellView];
    
    //白色背景图片
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
    UIImage *image = [UIImage imageNamed:@"content"];
    UIImage *resizeImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
    iv.image = resizeImage;
    iv.userInteractionEnabled = YES;
    [cellView addSubview:iv];
    
    
    return [self creatContentView:iv
                          tfLabel:tflabel row:row isHasLable:isHasLable];
    
    
    
}
/**
 *  创建内容视图
 *
 *  @param imageView   内容控件的父视图
 *  @param imageName   图片名称
 *  @param tfPlacehold 占位符
 *  @param row         行
 */
-(UILabel*)creatContentView:(UIImageView*)imageView
                    tfLabel:(NSString*)tflabel
                        row:(NSInteger)row
                 isHasLable:(BOOL)isHasLable{
    
    CGRect tfFrame;
    
    if (isHasLable) {//图片后添加一个label显示文本
        
        CGSize size = [Utils getTextSize:tflabel textRegion:CGSizeMake(200, 100) textFontSize:App_Delegate.largeFont];
        
        CGRect lbFrame = CGRectMake(VIEW_START_X , 0, size.width + 10, imageView.HEIGTH);
        
        UILabel *label = [UILabel labelWithFrame:lbFrame
                                       LableText:tflabel
                                            size:App_Delegate.largeFont
                                       textColor:[UIColor lightGrayColor]
                                   textAlignment:NSTextAlignmentLeft];
        [imageView addSubview:label];
        
        tfFrame.origin.x = label.X + label.WIDTH ;
        tfFrame.size.width = imageView.WIDTH  - label.WIDTH - VIEW_START_X*2 ;
        tfFrame.origin.y = 0;
        tfFrame.size.height = imageView.HEIGTH;
    }
    UILabel *Contentlabel = [UILabel labelWithFrame:tfFrame
                                          LableText:tflabel
                                               size:App_Delegate.largeFont
                                          textColor:[UIColor lightGrayColor]
                                      textAlignment:NSTextAlignmentLeft];
    
    //    UITextField *tf = [UITextField textFieldWithFrame:tfFrame
    //                                            placehold:tfPlacehold
    //                                               target:self
    //                                                  tag:row];
    
    [imageView addSubview:Contentlabel];
    
    return Contentlabel;
    
    
}
@end
