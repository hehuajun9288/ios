//
//  ForgetPasswordTableViewCell.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/21.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTableViewCell.h"

typedef NS_ENUM(NSInteger,LoginTabelVIewEnumType) {
    
    accountType = 1,
    phoneNumType,
    
};

@interface ForgetPasswordTableViewCell : MyTableViewCell

@property (nonatomic, strong) UITextField *accountTF;
@property (nonatomic, strong) UITextField *phoneNumTF;
@property (nonatomic, strong) UIButton    *commitBtn;
@property (nonatomic, strong) UITextField *textField;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row;

@end
