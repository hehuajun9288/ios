//
//  ForgetPasswordTableViewCell.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/21.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "ForgetPasswordTableViewCell.h"

@implementation ForgetPasswordTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        switch (row) {
            case 0:
                
                break;
            case 1://账号
                
                //只要内容是登陆这种格式（图片 文本 输入框）在cell里面都可以直接这样调用
                _accountTF = [self cellView:row imageName:@"account" tfPlacehold:@"账号" isHasLable:NO];
                _accountTF.keyboardType = UIKeyboardTypeNamePhonePad;
                break;
            case 2://手机号
               _phoneNumTF = [self cellView:row imageName:@"手机号" tfPlacehold:@"手机号码" isHasLable:NO];
                _phoneNumTF.keyboardType = UIKeyboardTypeNumberPad;
                
                break;
            case 3://提交
                [self creatCommitButton];
                break;
                
            default:
                break;
        }
        
    }
    
    return self;
}

#pragma mark - 创建登陆按钮
-(void)creatCommitButton{
    
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    _commitBtn = [UIButton buttonWithFrame:frame
                                             title:@"提交"
                                          fontSize:App_Delegate.largeFont
                                        titleColor:WHITE_COLOR
                                            action:nil
                                            target:nil tag:0
                                   backgroundColor:BUTTON_BACKGROUND_COLOR];
    
    [self.contentView addSubview:_commitBtn];
}

#pragma mark - textField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    //输入字符长度
    NSInteger strLength = textField.text.length - range.length + string.length;
    
    if (textField.tag == 2) {//限制手机号码长度
        
        if (strLength > PHONE_NUMBER_LENGTH) {
            
            return NO;
        }
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _textField = textField;
}



@end
