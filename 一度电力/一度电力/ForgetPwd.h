//
//  ForgetPwd.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForgetPwd : NSObject

@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *msg;

@end
