//
//  ForgetPwdTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "ForgetPwdTableViewController.h"
#import "ForgetPasswordTableViewCell.h"
#import "ForgetPwd.h"
#import "LoginViewController.h"

@interface ForgetPwdTableViewController ()

@property (nonatomic, strong) ForgetPasswordTableViewCell *phoneNumCell;
@property (nonatomic, strong) ForgetPasswordTableViewCell *accountCell;

@end

@implementation ForgetPwdTableViewController

#pragma mark -view lifeCycle
-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [self resignFirstResponderForSelf];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"忘记密码";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;//RGBA(41, 56, 139, 1);
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)resignFirstResponderForSelf{
    
    [_accountCell.accountTF resignFirstResponder];
    [_phoneNumCell.phoneNumTF resignFirstResponder];
}
#pragma mark - tableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 4;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    ForgetPasswordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        
        cell = [[ForgetPasswordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.commitBtn addTarget:self action:@selector(commitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return TABLE_VIEW_HEIGHT_FOR_ROW;
}

#pragma mark - 提交忘记密码
-(void)commitBtnAction{
    
    _accountCell = [self cellForRow:accountType];
    _phoneNumCell = [self cellForRow:phoneNumType];
    
    [self resignFirstResponderForSelf];
    
    if ([self checkInfo]) {
        
        NSDictionary *accountDic = @{@"loginname":_accountCell.accountTF.text};
        NSDictionary *phoneNumDic = @{@"mobile":_phoneNumCell.phoneNumTF.text};
        
        WEAKSELF
        [AFSoapRequestOperation doForgetPwdSoapRequestWithParams:@[accountDic,phoneNumDic] hudShowInView:self.view block:^(NSString *jsonStr) {
            
            STRONGSELF
            ForgetPwd *forgetPwd = [ForgetPwd mj_objectWithKeyValues:[jsonStr JSONValue]];
            
            if (forgetPwd.result.intValue == 1) {
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您的密码重置成功,请查看短信内容,重新登录后及时修改密码!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [self popToLoginViewController];
                }];

            }else{
                
                [Utils showHUD:@"提交失败，请稍后再试!" autoHide:YES inView:strongSelf.view];
                
            }
            
        }];
    }
}
-(void)popToLoginViewController{
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[LoginViewController class]]) {
            
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

#pragma mark - 检查出入框内容
-(BOOL)checkInfo{
    
    
    if (_accountCell.accountTF.text.length <=0) {
        
        [self showHUDWithHintText:@"账号不能为空"];
        return NO;
        
    }else  if (![Utils isValidateMobile:_phoneNumCell.phoneNumTF.text]) {
        
        [self showHUDWithHintText:@"请输入有效的手机号"];
        return NO;
        
    }
    
    return YES;
}

#pragma mark -显示提示内容
-(void)showHUDWithHintText:(NSString*)hintText{
    
    [Utils showHUD:hintText autoHide:YES inView:self.view];
    
}

#pragma mark - 根据row获取tableView的cell
-(ForgetPasswordTableViewCell*)cellForRow:(NSInteger)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    ForgetPasswordTableViewCell *cell = (ForgetPasswordTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
