//
//  HistoricalTaskViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/25.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "HistoricalTaskViewController.h"
#import "AccountTableViewCell.h"

@interface HistoricalTaskViewController ()

@end
//请求的页数 从第一页开始
static int page = 1;

@implementation HistoricalTaskViewController

-(NSMutableArray *)missions{
    
    if (_missions == nil) {
        
        _missions = [NSMutableArray array];
    }
    
    return _missions;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"历史任务";
    
    [self.tableView.mj_header beginRefreshing];
}

-(void)headerWithRefresh{

    page = 1;
    [self getHeaderRefreshHistoryRecord];
    
    //恢复加载更多
    [self.tableView.mj_footer resetNoMoreData];}

-(void)footerWithRefresh{

    page ++ ;
    [self getFooterRefreshhHistoryRecord];
}
#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    int size = 10;
    int offset = (page - 1) * size;
    NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
    NSDictionary *offsetDic = @{@"offset":[NSString stringWithFormat:@"%d",offset]};
    NSDictionary *sizeDic = @{@"size":[NSString stringWithFormat:@"%d",size]};
    
    return @[uidDic,offsetDic,sizeDic];
}
#pragma mark - 加载更多
-(void)getFooterRefreshhHistoryRecord{
    
    WEAKSELF
    [AFSoapRequestOperation getHistoryMissionRecordSoapRequestWithParams:[weakSelf getParams]
                                                           hudShowInView:self.tableView
                                                                   block:^(NSString *jsonStr) {
                                                                       
                                                                       STRONGSELF
                                                                       
                                                                       NSArray *missionDics = [jsonStr JSONValue];
                                                                       if (missionDics.count == 0) {//没有数据返回时提示全部加载完毕
                                                                           
                                                                           [strongSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                                           return ;
                                                                       }
                                                                       for (NSDictionary *missionDic in missionDics) {
                                                                           
                                                                           Mission *mission = [[Mission alloc] initWithDic:missionDic];
                                                                           NSArray* timeArr = [mission.ctime componentsSeparatedByString:@" "];
                                                                           mission.ctime = timeArr[0];
                                                                           
                                                                           [strongSelf.missions addObject:mission];
                                                                       }
                                                                       
                                                                       [strongSelf.tableView reloadData];
                                                                       [strongSelf.tableView.mj_footer endRefreshing];
                                                                   }];
    
    
    
}

-(void)getHeaderRefreshHistoryRecord{
    
    WEAKSELF
    [AFSoapRequestOperation getHistoryMissionRecordSoapRequestWithParams:[weakSelf getParams] hudShowInView:self.tableView block:^(NSString *jsonStr) {
        
        STRONGSELF
        NSArray *infoDics = [jsonStr JSONValue];

        [weakSelf.missions removeAllObjects];
        
        //获取mission对象
        for (NSDictionary *infoDic in infoDics) {
            
            Mission *info = [[Mission alloc] initWithDic:infoDic];
            [strongSelf.missions addObject:info];
        }
        
        [strongSelf.tableView reloadData];
        
        //结束刷新
        [strongSelf.tableView.mj_header endRefreshing];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _missions.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    AccountTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AccountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    Mission *mission = _missions[indexPath.row];
    cell.typeLabel.text = mission.dno;
    cell.dateLable.text = [mission.ctime componentsSeparatedByString:@" "][0];

    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [self creatTableHeaderView:@[@"工单号",@"日期"]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    UIView *v = [self creatTableHeaderView:@[@"工单号",@"日期"]];
    
    return v.HEIGTH;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
