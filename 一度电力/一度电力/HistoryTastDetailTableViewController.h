//
//  HistoryTastDetailTableViewController.h
//  一度电力
//
//  Created by gn_macMini_liao on 16/4/26.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MissionDetail.h"

@interface HistoryTastDetailTableViewController : UITableViewController

@property (nonatomic, strong) MissionDetail *missionDetail;
@property (nonatomic, strong) NSString *dno;

@end
