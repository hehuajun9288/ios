//
//  HistoryTastDetailTableViewController.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/4/26.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "HistoryTastDetailTableViewController.h"
#import "AccountDetailTableViewCell.h"

@interface HistoryTastDetailTableViewController ()

@end

@implementation HistoryTastDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self getMissionDetailData];
}

-(void)getMissionDetailData{

    WEAKSELF
    [AFSoapRequestOperation getMissionDetailSoapRequestWithParams:@[@{@"dno":_dno}]
                                                    hudShowInView:weakSelf.tableView
                                                            block:^(NSString *jsonStr) {
                                                                
                                                                STRONGSELF
                                                                NSDictionary *dic = [jsonStr JSONValue];
                                                                strongSelf.missionDetail = [[MissionDetail alloc] initWithDic:dic];
                                                                NSLog(@"dic == %@",dic);
                                                                [strongSelf.tableView  reloadData];
                                                            }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_missionDetail) {
        
        return 10;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *labelText = @"";
    NSString *detailText = @"";

    
    switch (indexPath.row) {
        case 0:
            labelText = @"编号";
            detailText = _dno;
            break;
            
        case 1:
            labelText = @"用户名";
            detailText = _missionDetail.realname;
            
            break;
        case 2:
            labelText = @"地址";
            detailText = _missionDetail.address;
            
            break;
        case 3:
            labelText = @"电话";
            detailText = _missionDetail.mobile;
            
            break;
        case 4:
            labelText = @"申请时间";
            detailText = _missionDetail.ctime;
            
            break;
        case 5:
            labelText = @"任务描述";
            detailText = _missionDetail.descri;
            break;
            
        case 6:
            labelText = @"预估完成时间";
            detailText = _missionDetail.vtime;
            
            break;
        case 7:
            labelText = @"预估费用";
            detailText =  [NSString stringWithFormat:@"¥%@",_missionDetail.vfee];
            
            break;
        case 8:
            labelText = @"实际完成时间";
            detailText = _missionDetail.dtime;
            break;
        case 9:
            labelText = @"实际费用";
            detailText = [NSString stringWithFormat:@"¥%@",_missionDetail.fee];
            
            break;
   
        default:
            break;
    }
    NSString* cellID = [NSString stringWithFormat:@"cell%ld",indexPath.row];
    AccountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    
    if (cell == nil) {
        
        cell = [[AccountDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:cellID
                                                       labelText:labelText
                                                            info:[NSString stringWithFormat:@"%@",detailText] ];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AccountDetailTableViewCell *cell = (AccountDetailTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell.HEIGTH;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
