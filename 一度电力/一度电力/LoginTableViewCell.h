//
//  LoginTableViewCell.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTableViewCell.h"

typedef NS_ENUM(NSInteger,LoginTabelVIewEnumType) {

    userNameType = 1,
    passwordType,
    
};
@class LoginViewController;


@interface LoginTableViewCell : MyTableViewCell<UITextFieldDelegate>


@property (nonatomic, weak) LoginViewController *loginVC;

//登陆按钮
@property (nonatomic, strong) UIButton *loginBtn ;

@property (nonatomic, assign) BOOL isRememberPwd;

//用户名输入框
@property (nonatomic, strong) UITextField *userNameTF;

//密码输入框
@property (nonatomic, strong) UITextField *passwordTF;

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                         row:(NSInteger)row;


@end
