//
//  LoginTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "LoginTableViewCell.h"
#import "RegisterTableViewController.h"
#import "LoginViewController.h"
#import "ForgetPwdTableViewController.h"

@interface LoginTableViewCell ()

@property (nonatomic, strong) UITextField *textField;

@end
@implementation LoginTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        _isRememberPwd = [user_defaults boolForKey:rememberPwdKey];

        switch (row) {
            case 0:
                
                break;
            case 1://账号
                
                //只要内容是登陆这种格式（图片 文本 输入框）在cell里面都可以直接这样调用
                _userNameTF = [self cellView:userNameType imageName:@"account" tfPlacehold:@"账号" isHasLable:NO];
                _userNameTF.keyboardType = UIKeyboardTypeNamePhonePad;
                if (self.isRememberPwd) {
                    _userNameTF.text = [self getTFTextWithKey:loginNameKey];

                }
                break;
                
            case 2://密码
               _passwordTF = [self cellView:passwordType imageName:@"密码" tfPlacehold:@"密码" isHasLable:NO];
                _passwordTF.secureTextEntry = YES;
                if (self.isRememberPwd) {
                    _passwordTF.text = [self getTFTextWithKey:passworkKey];

                }
                break;
                
                case 3://j记住密码
                [self createRememberPwdBtn];
                break;
                
            case 4://登陆
                [self creatLoginButton];
                
                break;
            case 5://注册和忘记密码
                [self creatRegisterAndForgetPassworkButton];
                
                break;
                
                case 6://公司信息lb
                [self creatCompanyInfonLB];
                
                break;
            
            default:
                break;
        }
        
    }
    
    return self;
}

#pragma mark -创建label
-(void)creatCompanyInfonLB{

    CGRect frmae = CGRectMake(0, VIEW_START_Y, SCREEN_WIDTH, 20);
    UILabel *label1 = [UILabel labelWithFrame:frmae
                                    LableText:@"紧急救援电话:0731-84429598"
                                         size:App_Delegate.smallFont
                                    textColor:WHITE_COLOR
                                textAlignment:NSTextAlignmentCenter];
    
    [self.contentView addSubview:label1];
    
    frmae.origin.y = TABLE_VIEW_HEIGHT_FOR_ROW - 20;
    UILabel *label2  =[UILabel labelWithFrame:frmae
                                    LableText:@"湖南一度电力运行维护服务有限公司"
                                         size:App_Delegate.smallFont
                                    textColor:WHITE_COLOR
                                textAlignment:NSTextAlignmentCenter];
    [self.contentView addSubview:label2];
}
#pragma mark - 创建记住密码按钮
-(void)createRememberPwdBtn{

    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    MyButton *_rememberPwdBtn = [[MyButton alloc] initWithFrame:frame title:@"记住密码" tag:3];
    
    NSString *imageName;
    
    if (_isRememberPwd) {
        
        imageName = @"obj-checkbox-selected";
    }else{
        
        imageName = @"obj-checkbox-unselected";
    }
    [_rememberPwdBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [_rememberPwdBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_rememberPwdBtn];
    
}

#pragma mark - 获取本地的账号密码
-(NSString*)getTFTextWithKey:(NSString*)key{

    NSMutableDictionary *dic = [user_defaults valueForKey:accountKey];
    
    return dic[key];
    
}

#pragma mark - 创建登陆按钮
-(void)creatLoginButton{

    _loginBtn = [self creatButtonWhthTitle:@"登录"];
}

#pragma mark - 创建注册和忘记密码按钮
-(void)creatRegisterAndForgetPassworkButton{

    //圆角按钮
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH * 0.212, SCREEN_HEIGHT * 0.052);
    
    //注册按钮
    UIButton *rButton = [UIButton buttonWithFrame:frame
                                            title:@"注  册"
                                         fontSize:App_Delegate.smallFont
                                       titleColor:WHITE_COLOR
                                           action:@selector(buttonClick:)
                                           target:self tag:1
                                  backgroundColor:BUTTON_BACKGROUND_COLOR];
    [rButton setBorder];
    
    [self.contentView addSubview:rButton];
    
    frame.origin.x = SCREEN_WIDTH - CONTENT_VIEW_START_X - rButton.WIDTH;
    //忘记密码按钮
    UIButton *fButton = [UIButton buttonWithFrame:frame
                                            title:@"忘记密码"
                                         fontSize:App_Delegate.smallFont
                                       titleColor:WHITE_COLOR
                                           action:@selector(buttonClick:)
                                           target:self
                                              tag:2
                                  backgroundColor:BUTTON_BACKGROUND_COLOR];
    [fButton setBorder];

    [self.contentView addSubview:fButton];

}


#pragma mark - buttonClick
-(void)buttonClick:(UIButton*)button{

    [_textField resignFirstResponder];
    switch (button.tag) {
        case 0://登陆
            
          
            break;
        case 1://注册
        {
            RegisterTableViewController *rTVC = [[RegisterTableViewController alloc] init];
            [self.loginVC.navigationController pushViewController:rTVC animated:YES];
        }
            break;
        case 2://忘记密码
        {
        
            ForgetPwdTableViewController *rTVC = [[ForgetPwdTableViewController alloc] init];
            [self.loginVC.navigationController pushViewController:rTVC animated:YES];
        }
            break;
   
            case 3://记住密码
            if (self.isRememberPwd) {//记住密码
                
                [button setImage:[UIImage imageNamed:@"obj-checkbox-unselected"] forState:UIControlStateNormal];
                self.isRememberPwd = NO;

            }else{//记住密码没有选中
            
                [button setImage:[UIImage imageNamed:@"obj-checkbox-selected"] forState:UIControlStateNormal];
                self.isRememberPwd = YES;

            }
            
            [user_defaults setBool:self.isRememberPwd forKey:rememberPwdKey];
            [user_defaults synchronize];
            
            break;
        default:
            break;
    }
}
#pragma mark - textFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{

    _textField = textField;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    
    //输入字符长度
    NSInteger strLength = textField.text.length - range.length + string.length;
    
    if (textField.tag == 1) {//限制手机号码长度
        
            if (strLength > PHONE_NUMBER_LENGTH) {
                
                return NO;
            }
    }
    return YES;
}


@end
