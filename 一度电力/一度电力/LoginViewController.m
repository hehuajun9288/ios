//
//  LoginViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginTableViewCell.h"
#import "PersonCenterViewController.h"
#import "GDataXMLNode.h"
#import "UserModel.h"
#import "AverageUserPersenCenterViewController.h"
#import "PropertyPersenCenterViewController.h"
#import "ElectricianPersenCenterViewController.h"
#import "SalesmanViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) LoginTableViewCell *userNameCell;
@property (nonatomic, strong) LoginTableViewCell *passwordCell;

@end

@implementation LoginViewController

#pragma mark -view lifeCycle
-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
    [self resignFirstResponderForSelf];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"登录";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

#pragma mark -resignFirstResponder
-(void)resignFirstResponderForSelf{

    [_userNameCell.userNameTF resignFirstResponder];
    [_passwordCell.passwordTF resignFirstResponder];
}

#pragma mark - tableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    
    return 7;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSString *cellID = @"loginCell";
    LoginTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        
        cell = [[LoginTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loginVC = self;
    }
    
    [cell.loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return TABLE_VIEW_HEIGHT_FOR_ROW;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - loginAction
-(void)loginAction{

    _userNameCell = [self cellForRow:userNameType];
    _passwordCell = [self cellForRow:passwordType];
    
    [self resignFirstResponderForSelf];
    
    NSMutableArray *arr=[NSMutableArray array];
    NSDictionary *userNameDic = @{@"username":_userNameCell.userNameTF.text};
    NSDictionary *passwordDic = @{@"password":_passwordCell.passwordTF.text};

    //请求参数格式已数组形式存放 接口需要的字段字典
    [arr addObject:userNameDic];
    [arr addObject:passwordDic];
    
    if ([self checkInfo]) {
        
        //请求数据  block返回json字符串
        WEAKSELF
        [AFSoapRequestOperation loginSoapRequestWithParams:arr hudShowInView:weakSelf.view block:^(NSString *jsonStr) {
            
            //获取到user对象  因为多界面要用到user对象的信息 所以定义在delegate
            App_Delegate.userModel = [UserModel mj_objectWithKeyValues:[jsonStr JSONValue]];
            
            //密码
            App_Delegate.password = _passwordCell.passwordTF.text;
            
            //记住账号密码
            if ([user_defaults boolForKey:rememberPwdKey]) {
                
                [self rememberAccount];

            }
            
            //根据不同的id登陆不同的界面
            [self pushViewController];
            
        }];

    }
    
    
}

#pragma mark -记住账号密码
-(void)rememberAccount{

    NSMutableDictionary *mdic = [NSMutableDictionary dictionary];
    [mdic setValue:_userNameCell.userNameTF.text forKey:loginNameKey];
    [mdic setValue:_passwordCell.passwordTF.text forKey:passworkKey];

    [user_defaults setObject:mdic forKey:accountKey];
    [user_defaults synchronize];
}
#pragma mark - 根据不同的id登陆不同的界面
-(void)pushViewController{

    switch (App_Delegate.userModel.utypeid.intValue) {
        case 8://普通用户
        {
            AverageUserPersenCenterViewController *auVC = [[AverageUserPersenCenterViewController alloc] init];
            [self.navigationController pushViewController:auVC animated:YES];
            
        }
            break;
        case 7://物业
        {
//            PropertyPersenCenterViewController *puVC = [[PropertyPersenCenterViewController alloc] init];
//            [self.navigationController pushViewController:puVC animated:YES];
            
        }
            break;
        case 6://电工
        {
            ElectricianPersenCenterViewController *eleVC = [[ElectricianPersenCenterViewController alloc] init];
            [self.navigationController pushViewController:eleVC animated:YES];
            
        }
            break;
            
        case 5://业务员界面
        {
            
            SalesmanViewController *auVC = [[SalesmanViewController alloc] init];
            [self.navigationController pushViewController:auVC animated:YES];

        }
            break;
        default:
            break;
    }

}
#pragma mark - 检查输入框的信息
-(BOOL)checkInfo{

    //![Utils isValidateMobile:_userNameCell.userNameTF.text]
    if (_userNameCell.userNameTF.text.length <= 0) {
        
        [self showHUDWithHintText:@"账号不能为空"];
        return NO;
        
    }else if (![Utils isContainChinese:_passwordCell.passwordTF.text]){
        
        [self showHUDWithHintText:@"密码包含中文字符"];
        return NO;
        
    }else if ([_passwordCell.passwordTF.text isEqualToString:@""]){
        
        [self showHUDWithHintText:@"密码不能为空"];
        return NO;
    }
    
    return YES;

}
-(void)showHUDWithHintText:(NSString*)hintText{
    
    [Utils showHUD:hintText autoHide:YES inView:self.view];
    
}

-(LoginTableViewCell*)cellForRow:(NSInteger)row{

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    LoginTableViewCell *cell = (LoginTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
