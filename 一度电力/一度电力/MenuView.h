//
//  MenuView.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#define sclae 3.2

@protocol RemoveMenuViewDelegate <NSObject>

-(void) removeMenuView;
-(void) pushViewController:(id)viewController;
-(void) popToLoginViewController;

@end

@interface MenuView : UIControl
@property(nonatomic,strong)UITableView* tableView;
@property (nonatomic, weak) id <RemoveMenuViewDelegate> delegate;

-(void)showInView:(UIView*)view;
-(void)dismiss;

@end
