//
//  MenuView.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "MenuView.h"
#import "BasiInfoViewController.h"
#import "ChangePwdTableViewController.h"
#import "AddressInfoTableViewController.h"

@interface MenuView   ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray *menuTexts;

@end
@implementation MenuView

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        self.layer.masksToBounds = NO;
        
        _menuTexts = @[@"修改密码",@"基本信息",@"地址信息",@"退出"];;
        [self createView];
    }
    
    return self;
}
-(void)createView{
    
    self.backgroundColor = RGBA(0, 0, 0, 0);
    
    CGFloat tableViewWidth = SCREEN_WIDTH/sclae;

    CGRect frame = CGRectZero;
    frame.origin.y = 0;
    frame.origin.x = SCREEN_WIDTH - tableViewWidth;
    frame.size.width = tableViewWidth;
    frame.size.height = 44 * _menuTexts.count;
    
    //创建tableView
    self.tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    [self addSubview:self.tableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _menuTexts.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:App_Delegate.smallFont];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    cell.textLabel.text = _menuTexts[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    id viewController;
    switch (indexPath.row) {
        case 0://修改密码
            viewController = [[ChangePwdTableViewController alloc] init];
            break;
        case 1://基本信息
            viewController = [[BasiInfoViewController alloc] init];
            break;
        case 2://地址信息
            viewController = [[AddressInfoTableViewController alloc] init];
            break;
         case 3:
            
            break;
        default:
            break;
    }
    
    if (indexPath.row != 3) {
        
        [self.delegate pushViewController:viewController];
        
    }else{//退出
        
        [self.delegate popToLoginViewController];
    }

    [self dismiss];
}

//分割线左移
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)showInView:(UIView*)view{
    
#warning 可插入动画 后期再做
    [view addSubview:self];
    
}
-(void)dismiss{
    
    [self.delegate removeMenuView];
}

-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{

    [self dismiss];
    return YES;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    return TABLE_VIEW_HEIGHT_FOR_ROW;
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
