//
//  Mission.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/6.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  private Long id;			//序号
	private String dno;			//任务流水号
	private Long uid;			//用户id
	private int sid;			//站点id
	private Long aid;			//地址id
	private int mtype;			//任务类型
	private int mstatus;		//任务状态
	private Long dutymanid;		//责任人id
	private String description;	//任务描述
	private String ctime;		//任务创建时间
	private String dtime;		//任务完成时间
 */
@interface Mission : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *dno;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *sid;
@property (nonatomic, strong) NSString *aid;
@property (nonatomic, strong) NSString *mtype;
@property (nonatomic, strong) NSString *mstatus;
@property (nonatomic, strong) NSString *dutymanid;
@property (nonatomic, strong) NSString *descri;
@property (nonatomic, strong) NSString *ctime;
@property (nonatomic, strong) NSString *dtime;

-(instancetype)initWithDic:(NSDictionary*)dic;



@end
