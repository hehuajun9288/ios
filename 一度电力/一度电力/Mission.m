//
//  Mission.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/6.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "Mission.h"

@implementation Mission

/**
 {
 "id": 73,
 "dno": "CS30001601061291",
 "uid": 97,
 "sid": 3,
 "aid": 2,
 "mtype": 2,
 "mstatus": 5,
 "dutymanid": null,
 "description": "jj",
 "ctime": "2016-01-06 17:55:01.0",
 "dtime": null
 },
 */
 -(instancetype)initWithDic:(NSDictionary *)dic{

     self = [super init];
     
     if (self) {
         
         self.id = dic[@"id"];
         self.dno = dic[@"dno"];
         self.uid = dic[@"uid"];
         self.sid = dic[@"sid"];
         self.aid = dic[@"aid"];
         self.mtype = dic[@"mtype"];
         self.mstatus = dic[@"mstatus"];
         self.dutymanid = dic[@"dutymanid"];
         self.descri = dic[@"description"];
         self.ctime = dic[@"ctime"];
         self.dtime = dic[@"dtime"];

     }
     
     
     return self;
}
@end
