//
//  MissionDetail.h
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 "dno": "CS30001601079713",
 "address": "长沙市岳麓区河西望月湖小区9片月华街59号",
 "realname": null,
 "station": null,
 "mobile": "13302211886",
 "uid": 97,
 "mtype": 0,
 "mstatus": 5,
 "dutymanid": null,
 "description": "你好，测试测试测试",
 "ctime": "2016-01-07 00:09:19.0",
 "dtime": null,
 "reason": null,
 "result": null,
 "fee": null,
 "score": null
 */
@interface MissionDetail : NSObject

@property (nonatomic, strong) NSString *dno;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *station;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *mtype;
@property (nonatomic, strong) NSString *mstatus;
@property (nonatomic, strong) NSString *dutymanid;
@property (nonatomic, strong) NSString *descri;
@property (nonatomic, strong) NSString *ctime;
@property (nonatomic, strong) NSString *dtime;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *fee;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *vtime;  //预计天数
@property (nonatomic, strong) NSString *vfee; //费用

-(instancetype)initWithDic:(NSDictionary*)dic;
@end
