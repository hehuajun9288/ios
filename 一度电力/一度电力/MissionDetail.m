//
//  MissionDetail.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "MissionDetail.h"

@implementation MissionDetail

-(instancetype)initWithDic:(NSDictionary*)dic{


    self = [super init];
    
    if (self) {
        
        /**
         "dno": "CS30001601079713",
         "address": "长沙市岳麓区河西望月湖小区9片月华街59号",
         "realname": null,
         "station": null,
         "mobile": "13302211886",
         "uid": 97,
         "mtype": 0,
         "mstatus": 5,
         "dutymanid": null,
         "description": "你好，测试测试测试",
         "ctime": "2016-01-07 00:09:19.0",
         "dtime": null,
         "reason": null,
         "result": null,
         "fee": null,
         "score": null
         */
        self.dno = [self nullString: dic[@"dno"]];
        self.address = [self nullString: dic[@"address"]];
        self.realname = [self nullString: dic[@"realname"]];
        self.station = [self nullString: dic[@"station"]];
        self.mobile = [self nullString: dic[@"mobile"]];
        self.uid = [self nullString: dic[@"uid"]];
        self.mtype = [self nullString: dic[@"mtype"]];
        self.mstatus = [self nullString: dic[@"mstatus"]];
        self.dutymanid = [self nullString: dic[@"dutymanid"]];
        self.descri = [self nullString: dic[@"description"]];
        self.ctime = [self nullString: dic[@"ctime"]];
        self.dtime = [self nullString: dic[@"dtime"]];
        self.reason = [self nullString: dic[@"reason"]];
        self.result = [self nullString: dic[@"result"]];
        self.fee = [self nullString: dic[@"fee"]];
        self.score = [self nullString: dic[@"score"]];
        self.vfee = [self nullString:dic[@"vfee"]];
        self.vtime = [self nullString:dic[@"vtime"]];

    }
    
    return self;
}

-(NSString*)nullString:(NSString*)string{

    
    if ((string == NULL) || ([string isEqual:[NSNull null]]) || (string == nil)) {
        
        string = @"";
    }
    
    return string;
}

@end
