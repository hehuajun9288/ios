//
//  MissionDetailTableViewCell.h
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MissionDetailTableViewCell;

@protocol MissionDetailDelegate <NSObject>

-(void)missionDetailTableViewCell:(MissionDetailTableViewCell*)missionDetailCell score:(NSString*)score;

@end
@interface MissionDetailTableViewCell : MyTableViewCell

@property (nonatomic, strong) UIButton *commitBtn;
@property (nonatomic, weak) id <MissionDetailDelegate> delegate;
-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                         row:(NSInteger)row;

-(void)cellData:(NSString*)labelText2;

@end
