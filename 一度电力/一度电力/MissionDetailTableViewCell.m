//
//  MissionDetailTableViewCell.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "MissionDetailTableViewCell.h"
#import "LHRatingView.h"

@interface MissionDetailTableViewCell ()<ratingViewDelegate>{

    UIView *contentView;
}

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;

@property (nonatomic, strong) LHRatingView *starRatingView;

@property (nonatomic, strong) UILabel *scoreLabel;
@end

#define contentView_X 5

@implementation MissionDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{

    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        //白色北京那个视图
        self.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(contentView_X, VIEW_START_Y, SCREEN_WIDTH - contentView_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
        
        contentView.backgroundColor  = WHITE_COLOR;
        
        [self.contentView addSubview:contentView];
        //                                                             strongSelf.labelText1Arr = @[@"编号",@"地址",@"时间",@"结果",@"费用",@"评分"];


        switch (row) {
            case 0:
                [self creatLabelWithLB1Text:@"编号"];
                break;
                
            case 1:
                [self creatLabelWithLB1Text:@"地址"];

                break;

            case 2:
                [self creatLabelWithLB1Text:@"时间"];

                break;

            case 3:
                [self creatLabelWithLB1Text:@"结果"];

                break;

            case 4:
                [self creatLabelWithLB1Text:@"费用"];

                break;
            case 5://评分视图
                [self creatLB1WithText:@"评分"];
                [self creatScoreLB];
                
                break;
                
                case 6://提交button
                _commitBtn = [self creatButtonWhthTitle:@"提交"];
                contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
                break;
            default:
                break;
        }
      }
    
    return self;
}

#pragma mark - 创建评分视图
-(void)creatScoreLB{

    CGRect frame = _label1.frame;
    frame.origin.x = contentView.WIDTH - VIEW_START_X - _label1.WIDTH;
    _scoreLabel = [UILabel labelWithFrame:frame
                                LableText:@"1.0"
                                     size:App_Delegate.largeFont
                                textColor:[UIColor grayColor]
                            textAlignment:NSTextAlignmentRight];
    [contentView addSubview:_scoreLabel];
    
    [self creatStartView];
}
#pragma mark -星星评价视图
-(void)creatStartView{
    
    CGRect frame = CGRectMake(_label1.X + _label1.WIDTH,0, _scoreLabel.X - _label1.X * 2 - _label1.WIDTH, contentView.HEIGTH);

    _starRatingView = [[LHRatingView alloc] initWithFrame:frame];
    _starRatingView.delegate = self;
    _starRatingView.ratingType = HALF_TYPE; //半颗星

    [contentView addSubview:_starRatingView];
}

#pragma mark - starRatingView delegate
-(void)ratingView:(LHRatingView *)view score:(CGFloat)score{
    
    self.scoreLabel.text = [NSString stringWithFormat:@"%0.1f",score  ];
    [self.delegate missionDetailTableViewCell:self score:self.scoreLabel.text];
}

-(void)creatLabelWithLB1Text:(NSString*)label1Text{

    [self creatLB1WithText:label1Text];
    
    CGRect frame = _label1.frame;
    frame.origin.x = _label1.X + _label1.WIDTH ;
    
    //具体显示内容
    _label2 = [UILabel labelWithFrame:frame
                            LableText:@""
                                 size:App_Delegate.largeFont
                            textColor:[UIColor grayColor]
                        textAlignment:NSTextAlignmentLeft];
    [contentView addSubview:_label2];

    
}

-(void)creatLB1WithText:(NSString*)text{

    
    CGSize size = [Utils getTextSize:text
                          textRegion:CGSizeMake(100, 200)
                        textFontSize:App_Delegate.largeFont];
    
    CGRect frame = CGRectMake(VIEW_START_X, VIEW_START_Y, size.width + 10, contentView.HEIGTH - VIEW_START_Y * 2);

    _label1 = [UILabel labelWithFrame:frame
                            LableText:text
                                 size:App_Delegate.largeFont
                            textColor:BLACK_COLOR
                        textAlignment:NSTextAlignmentLeft];
    [contentView addSubview:_label1];

}

#pragma mark - 设置cell的内容
-(void)cellData:(NSString*)labelText2{

//    _label1.text = labelText1;
    NSLog(@"labelText2 == %@",labelText2);
    //获取文本的宽高
    CGSize size = [Utils getTextSize:labelText2
                          textRegion:CGSizeMake(contentView.WIDTH - VIEW_START_X * 2 - _label1.WIDTH, 1000)
                        textFontSize:App_Delegate.largeFont];
    
    //size.height > frame.size.height ? size.height : frame.size.height;
//    frame.size.width = size.width
    CGRect frame = _label2.frame;
    frame.size.height = size.height > frame.size.height? size.height:frame.size.height;
    frame.size.width = size.width;
    _label2.frame = frame;
    
    CGPoint center = _label1.center;
    center.y = _label2.center.y;
    _label1.center = center;;
    
    _label2.text = labelText2;
    
    frame = contentView.frame;
    frame.size.height = _label2.HEIGTH + _label2.Y + VIEW_START_Y;
    contentView.frame = frame;
    
    frame = contentView.frame;
    frame.size.height = contentView.HEIGTH + contentView.Y + VIEW_START_Y;
    self.frame = frame;
    
}

@end
