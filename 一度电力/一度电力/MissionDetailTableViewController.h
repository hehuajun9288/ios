//
//  MissionDetailTableViewController.h
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MissionDetailTableViewController : UITableViewController
@property (nonatomic, strong) NSString *missionDno;
@property (nonatomic, copy) void (^FinishReqeust)(BOOL finishRequest);

@end
