//
//  MissionDetailTableViewController.m
//  一度电力
//
//  Created by gn_macMini_liao on 16/1/7.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "MissionDetailTableViewController.h"
#import "MissionDetailTableViewCell.h"
#import "MissionDetail.h"

@interface MissionDetailTableViewController ()<MissionDetailDelegate>{

    
}
@property (nonatomic, strong)  MissionDetail *missionDetail;
//@property (nonatomic, strong) NSArray *labelText1Arr;
@property (nonatomic, strong) NSMutableArray *labelText2Arr;

@property (nonatomic, strong) NSString *scoreStr;
@end

@implementation MissionDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.title = @"工单任务评分";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    

    _labelText2Arr = [NSMutableArray array];
    self.scoreStr = @"1.0";
    
    [self getMissionDetailData];
}
#pragma mark - MissionDetailTableViewCell delegate
-(void)missionDetailTableViewCell:(MissionDetailTableViewCell *)missionDetailCell score:(NSString *)score{

    self.scoreStr = score;
}
#pragma mark - 获取任务详情
-(void)getMissionDetailData{

    NSDictionary *dic = @{@"dno":self.missionDno};
    
    WEAKSELF
    [AFSoapRequestOperation missionDetailSoapRequestWithParams:@[dic]
                                                 hudShowInView:weakSelf.view
                                                         block:^(NSString *jsonStr) {
                                                             
                                                             STRONGSELF
                                                             strongSelf.missionDetail = [[MissionDetail alloc] initWithDic:[jsonStr JSONValue]];
                                                             
                                                             //左边label显示的文本
//                                                             strongSelf.labelText1Arr = @[@"编号",@"地址",@"时间",@"结果",@"费用",@"评分"];
                                                             
                                                             //从missionDetail找出对应的字段添加到数组
                                                             //编号
                                                             [strongSelf.labelText2Arr addObject:strongSelf.missionDetail.dno];
                                                             
                                                             //地址
                                                             [strongSelf.labelText2Arr addObject:strongSelf.missionDetail.address];
                                                             
                                                             //创建时间
                                                             [strongSelf.labelText2Arr addObject:[strongSelf.missionDetail.ctime componentsSeparatedByString:@" "][0]];
                                                             
                                                             //结果
                                                             [strongSelf.labelText2Arr addObject:strongSelf.missionDetail.result];
                                                             
                                                             //费用
                                                             NSString *fee = [NSString stringWithFormat:@"%@",strongSelf.missionDetail.fee];
                                                             [strongSelf.labelText2Arr addObject:[NSString stringWithFormat:@"¥ %.2f",fee.floatValue]];
                                                             
                                                             [strongSelf.tableView reloadData];
                                                         }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MissionDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[MissionDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" row:indexPath.row];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.commitBtn addTarget:self action:@selector(Score) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (indexPath.row <= 4 && self.labelText2Arr.count > 0) {
        
        [cell cellData:_labelText2Arr[indexPath.row]];

    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    MissionDetailTableViewCell *cell  = (MissionDetailTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row <= 4 && self.labelText2Arr.count > 0) {
        
        [cell cellData:_labelText2Arr[indexPath.row]];

        return cell.HEIGTH;

    }else{
//
        return TABLE_VIEW_HEIGHT_FOR_ROW;
    }
}
#pragma mark - 评分提交
-(void)Score{

    NSLog(@"scoreStr == %@",self.scoreStr);
    if ([self.scoreStr isEqualToString:@"0.0"]) {
        
        [Utils showHUD:@"请进行评分" autoHide:YES inView:self.view];

    }else{
        
        NSDictionary *dnoDic = @{@"dno":self.missionDetail.dno};
        NSDictionary *scoreDic = @{@"score":self.scoreStr};
        
        WEAKSELF
        [AFSoapRequestOperation toScoreSoapRequestWithParams:@[dnoDic,scoreDic]
                                               hudShowInView:weakSelf.view
                                                       block:^(NSString *jsonStr) {
                                                           
                                                           STRONGSELF
                                                           NSDictionary *dic = [jsonStr JSONValue];
                                                           
                                                           NSString *result = dic[@"result"];
                                                           
                                                           if (result.intValue == 1) {
                                                               
                                                               [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户，您的评分已成功提交..."
                                                                                              message:nil
                                                                                    cancelButtonTitle:@"确定"
                                                                                    otherButtonTitles:nil
                                                                                              handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                                                  
                                                                                                  [strongSelf.navigationController popViewControllerAnimated:YES];
                                                                                                  if (strongSelf.FinishReqeust) {
                                                                                                      
                                                                                                      strongSelf.FinishReqeust(YES);
                                                                                                      
                                                                                                  }

                                                                                    }];
                                                               
                                                           }else{
                                                               
                                                               [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                           }
                                                       }];
        

    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
