//
//  MissionModel.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  private Long uid;		//用户id
	private int mtype;		//任务类型（普通用户 填2，物业填 1）
	private int mstatus;	//任务状态：0:已审核;1:取消;2:已分派;3:完成;4:已评分;5:申请;
	private int aid;		//地址id
	private String description;	//任务描述
 */
@interface MissionModel : NSObject

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *mtype;
@property (nonatomic, strong) NSString *mstatus;
@property (nonatomic, strong) NSString *aid;
@property (nonatomic, strong) NSString *descri;

@property (nonatomic, strong) NSString *id;
@property(nonatomic,strong)NSString* dno;// 任务流水号
@property(nonatomic,strong)NSString* reason;// 故障原因
@property(nonatomic,strong)NSString* result;//处理结果
@property(nonatomic,strong)NSString* fee;//维护费用
@property(nonatomic,strong)NSString* score;//评分
@end
