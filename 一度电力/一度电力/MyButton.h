//
//  MyButton.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/3.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyButton : UIButton

-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)title tag:(NSInteger)tag;


@end
