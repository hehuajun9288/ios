//
//  MyButton.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/3.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "MyButton.h"
#define Q_RADIO_ICON_WH                     (SCREEN_WIDTH * 0.044)
#define Q_ICON_TITLE_MARGIN                 (5.0)


@implementation MyButton

-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)title tag:(NSInteger)tag{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:App_Delegate.largeFont];
        [self setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        self.tag = 3;

    }
    
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    return CGRectMake(0, (CGRectGetHeight(contentRect) - Q_RADIO_ICON_WH)/2.0, Q_RADIO_ICON_WH, Q_RADIO_ICON_WH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectMake(Q_RADIO_ICON_WH + Q_ICON_TITLE_MARGIN, 0,
                      CGRectGetWidth(contentRect) - Q_RADIO_ICON_WH - Q_ICON_TITLE_MARGIN,
                      CGRectGetHeight(contentRect));
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
