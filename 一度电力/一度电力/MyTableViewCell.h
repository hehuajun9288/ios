//
//  MyTableViewCell.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/25.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

/**
 *  创建cell的内容
 *
 *  @param row         tableView row
 *  @param imageName   图片名称
 *  @param tfPlacehold TF占位符
 *  @param isLable     是否要显示lable
 *
 *  @return return
 */
-(UITextField*)cellView:(NSInteger)row
              imageName:(NSString*)imageName
            tfPlacehold:(NSString*)tfPlacehold
                isHasLable:(BOOL)isHasLable;

/**
 *  创建button
 *
 *  @param buttonTitle button title
 *
 *  @return 返回button
 */
-(UIButton*)creatButtonWhthTitle:(NSString*)buttonTitle;

@end
