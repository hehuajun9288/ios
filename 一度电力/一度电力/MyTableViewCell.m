//
//  MyTableViewCell.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/25.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

#pragma mark - 创建整个cell的视图
/**
 *  创建整个cell的视图
 *
 *  @param row         cell的row
 *  @param imageName   图片名称
 *  @param tfPlacehold textField占位符
 *
 *  @return cellView
 */
-(UITextField*)cellView:(NSInteger)row
              imageName:(NSString*)imageName
            tfPlacehold:(NSString*)tfPlacehold
                isHasLable:(BOOL)isHasLable{
    
    UIView *cellView = [[UIView alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:cellView];
    
    //白色背景图片
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
    UIImage *image = [UIImage imageNamed:@"content"];
    UIImage *resizeImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
    iv.image = resizeImage;
    iv.userInteractionEnabled = YES;
    [cellView addSubview:iv];
    
    
    return [self creatContentView:iv
                        imageName:imageName
                      tfPlacehold:tfPlacehold
                              row:row
                          isHasLable:isHasLable];
    
}

/**
 *  创建内容视图
 *
 *  @param imageView   内容控件的父视图
 *  @param imageName   图片名称
 *  @param tfPlacehold 占位符
 *  @param row         行
 */
-(UITextField*)creatContentView:(UIImageView*)imageView
                      imageName:(NSString*)imageName
                    tfPlacehold:(NSString*)tfPlacehold
                            row:(NSInteger)row
                        isHasLable:(BOOL)isHasLable{
    
    //图标
    UIImageView *iv = [UIImageView imageWithFrame:CGRectMake(VIEW_START_X, (imageView.HEIGTH - 20) / 2, 20, 20) backgroundColor:CLEAR_COLOR imageNamed:imageName];
    [imageView addSubview:iv];
    
    CGRect tfFrame = CGRectMake(iv.X + VIEW_START_X * 2, 0, imageView.WIDTH - VIEW_START_X * 2 - iv.WIDTH, imageView.HEIGTH);
    
    if (isHasLable) {//图片后添加一个label显示文本
        
        CGSize size = [Utils getTextSize:tfPlacehold textRegion:CGSizeMake(200, 100) textFontSize:App_Delegate.largeFont];
        
        CGRect lbFrame = CGRectMake(iv.X + iv.WIDTH + 5 , 0, size.width + 10, imageView.HEIGTH);

        UILabel *label = [UILabel labelWithFrame:lbFrame
                                       LableText:tfPlacehold
                                            size:App_Delegate.largeFont
                                       textColor:[UIColor lightGrayColor]
                                   textAlignment:NSTextAlignmentLeft];
        [imageView addSubview:label];
        
        tfFrame.origin.x = label.X + label.WIDTH ;
        tfFrame.size.width = imageView.WIDTH - label.X - label.WIDTH - VIEW_START_X ;
    }
    UITextField *tf = [UITextField textFieldWithFrame:tfFrame
                                            placehold:tfPlacehold
                                               target:self
                                                  tag:row];
    [imageView addSubview:tf];
    
    return tf;
    
}

-(UIButton*)creatButtonWhthTitle:(NSString*)buttonTitle{

    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    UIButton *button = [UIButton buttonWithFrame:frame
                                        title:buttonTitle
                                     fontSize:App_Delegate.largeFont
                                   titleColor:WHITE_COLOR
                                       action:nil
                                       target:nil tag:0
                              backgroundColor:BUTTON_BACKGROUND_COLOR];
    
    [self.contentView addSubview:button];
    return button;
}

@end
