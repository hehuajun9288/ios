//
//  MyTextView.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/28.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTextView : UITextView


@property(nonatomic, strong) NSString *placeholder;     //占位符

-(void)addObserver;//添加通知
-(void)removeobserver;//移除通知

@end
