//
//  MyTextView.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/28.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "MyTextView.h"


@interface MyTextView ()

@property (nonatomic, strong) UIColor *aTextColor;
- (void) beginEditing:(NSNotification*) notification;
- (void) endEditing:(NSNotification*) notification;

@end

@implementation MyTextView

- (id) initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        self.aTextColor = [UIColor blackColor];
        [self addObserver];
    }
    return self;
}

-(void)addObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginEditing:) name:UITextViewTextDidBeginEditingNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endEditing:) name:UITextViewTextDidEndEditingNotification object:self];
}
-(void)removeobserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Setter/Getters
- (void) setPlaceholder:(NSString *)aPlaceholder {
    _placeholder = aPlaceholder;
    [self endEditing:nil];
}

- (NSString *) text {
    NSString* text = [super text];
    if ([text isEqualToString:_placeholder]) return @"";
    return text;
}

- (void) beginEditing:(NSNotification*) notification {
    if ([super.text isEqualToString:_placeholder]) {
        super.text = nil;
        //字体颜色
        [super setTextColor:self.aTextColor];
    }
    
}

- (void) endEditing:(NSNotification*) notification {
    if ([super.text isEqualToString:@""] || self.text == nil) {
        super.text = _placeholder;
        //注释颜色
        [super setTextColor:[UIColor lightGrayColor]];
    }
}
@end
