//
//  OrderDetailTableViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 16/4/27.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MissionDetail.h"

@interface OrderDetailTableViewController : UITableViewController

@property (nonatomic, strong) NSString *dno;
@property (nonatomic, strong) MissionDetail *missionDetail;
@property (nonatomic, copy) void (^FinishReqeust)(BOOL finishRequest);
@end
