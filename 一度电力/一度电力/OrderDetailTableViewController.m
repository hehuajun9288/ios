//
//  OrderDetailTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/27.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "OrderDetailTableViewController.h"
#import "AccountDetailTableViewCell.h"

@interface OrderDetailTableViewController ()


@end

@implementation OrderDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.title = @"用电代办单";
    [self RequestData];
}

#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    //     *  uid	用户id
    //     mtype	任务类型 普通用户 填2，物业填 1
    //     offset	分页开始序号
    //     size	每页内容数
    
    
    NSDictionary *dnoDic = @{@"dno":self.dno};
    return @[dnoDic];
}

-(void)RequestData{
    WEAKSELF
    [AFSoapRequestOperation ElectricianGetMissionDeatailSoapRequestWithParams:[weakSelf getParams]
                                                                hudShowInView:weakSelf.view
                                                                        block:^(NSString *jsonStr) {
                                                                            STRONGSELF
                                                                            NSLog(@"jsonStr == %@",[jsonStr JSONValue]);
                                                                      
                                                                            NSDictionary *dic = [jsonStr JSONValue];
                                                                            strongSelf.missionDetail = [[MissionDetail alloc] initWithDic:dic];
                                                                            
                                                                            [self.tableView reloadData];
                                                                            
                                                                        }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.missionDetail) {
        
        return 8;

    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *labelText = @"";
    NSString *detailText = @"";
    
    
    switch (indexPath.row) {
        case 0:
            labelText = @"编号";
            detailText = _dno;
            return  [self creatDetailCell:labelText
                       detailText:detailText];
            break;
            
        case 1:
            labelText = @"用户名";
            detailText = _missionDetail.realname;
            return [self creatDetailCell:labelText
                       detailText:detailText];

            break;
        case 2:
            labelText = @"地址";
            detailText = _missionDetail.address;
            return [self creatDetailCell:labelText
                       detailText:detailText];

            break;
        case 3:
            labelText = @"电话";
            detailText = _missionDetail.mobile;
            return [self creatDetailCell:labelText
                       detailText:detailText];

            break;
        case 4:
            labelText = @"申请时间";
            detailText = _missionDetail.ctime;
            return [self creatDetailCell:labelText
                       detailText:detailText];

            break;
        case 5:
            labelText = @"代办描述";
            detailText = _missionDetail.descri;
            return [self creatDetailCell:labelText
                       detailText:detailText];

            break;
            
        case 6:{
            
            labelText = @"预估费用(元)";
            MissionDetailInputCell *inputCell = [[MissionDetailInputCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                         reuseIdentifier:@"inputCell" labelText:labelText];
            
            return inputCell;
        }
            break;
            
        case 7:{//提交按钮
            ButtonCell *buttonCell = [[ButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell1"];
            WEAKSELF
            buttonCell.buttonTap = ^(){
            
                STRONGSELF
                [strongSelf configure];
                
            };
            return buttonCell;

        }
            break;

            
        default:
            return nil;
            break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 7) {
        
        return TABLE_VIEW_HEIGHT_FOR_ROW;
    }
    AccountDetailTableViewCell *cell = (AccountDetailTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell.HEIGTH;
}

#pragma mark - 提交的方法
-(void)configure{

    //因为cell中的button不能获取到其他cell的内容，所以要这样获取cell的内容
    MissionDetailInputCell *inputCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    if (inputCell.textField.text.length == 0) {
        
        [Utils showHUD:@"请输入预估费用" autoHide:YES inView:self.view];
        return;
    }else{
        
        NSDictionary *uidDic = @{@"uid":App_Delegate.userModel.id};
        NSDictionary *dnoDic = @{@"dno":self.dno};
        NSDictionary* fee = @{@"vfee":inputCell.textField.text};
        NSDictionary* Vtime = @{@"vtime":@"1"};
        NSArray* para = @[uidDic,dnoDic,fee,Vtime];
        WEAKSELF
        [AFSoapRequestOperation ElectricianReceiveMissionSoapRequestWithParams:para
                                                                 hudShowInView:weakSelf.view
                                                                         block:^(NSString *jsonStr) {
                                                                             
                                                                             STRONGSELF
                                                                             
                                                                             NSDictionary *dic = [jsonStr JSONValue];
                                                                             
                                                                             NSString *result = dic[@"result"];
                                                                             
                                                                             if (result.intValue == 1) {
                                                                                 
                                                                                 [UIAlertView bk_showAlertViewWithTitle:@"您已经接受该任务" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                                     
                                                                                     [strongSelf.navigationController popViewControllerAnimated:YES];
                                                                                     if (strongSelf.FinishReqeust) {
                                                                                         
                                                                                         strongSelf.FinishReqeust(YES);
                                                                                         
                                                                                     }
                                                                                 }];
                                                                             }else{
                                                                                 
                                                                                 [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                                             }
                                                                             
                                                                         }];
    }
    
    NSLog(@"_inputCell == %@",inputCell.textField.text);
}
-(AccountDetailTableViewCell*)creatDetailCell:(NSString*)labelText
                                   detailText:(NSString*)detailText{

    AccountDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[AccountDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:@"cell"
                                                       labelText:labelText
                                                            info:[NSString stringWithFormat:@"%@",detailText] ];
    }
    
    
    return cell;

}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
