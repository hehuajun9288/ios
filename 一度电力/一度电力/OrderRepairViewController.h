//
//  OrderRepairViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 16/4/25.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//
#import "OrderWorkOrderDeatailViewController.h"
#import "TastStateTableViewController.h"
#import "ElectricianWorkOrderProcessingViewController.h"
#import "MissionDetail.h"

@interface OrderRepairViewController : ElectricianWorkOrderProcessingViewController
@property (nonatomic, strong) NSString *dno;
@property (nonatomic, strong) MissionDetail *missionDetail;
@property (nonatomic, copy) void (^FinishReqeust)(BOOL finishRequest);
@end
