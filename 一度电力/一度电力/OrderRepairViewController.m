//
//  OrderRepairViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/25.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "OrderRepairViewController.h"

@interface OrderRepairViewController ()

@end

@implementation OrderRepairViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"工单处理单";
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Mission *mission = self.missionArr[indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OrderWorkOrderDeatailViewController *deatailVC = [[OrderWorkOrderDeatailViewController alloc] init];
    deatailVC.receiveMissionModel = mission;
    deatailVC.FinishReqeust = ^(BOOL finishRequest){
        
        if (finishRequest) {
            
            [self.tableView.mj_header beginRefreshing];
        }
    };
    [self.navigationController pushViewController:deatailVC animated:YES];
    
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    return [self creatTableHeaderView:@[@"交易类型",@"日期"]];
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    UIView *v = [self creatTableHeaderView:@[@"交易类型",@"日期"]];
//    
//    return v.HEIGTH;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
