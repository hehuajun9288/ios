//
//  OrderWorkOrderDeatailViewController.m
//  一度电力
//
//  Created by apple on 16/4/28.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "OrderWorkOrderDeatailViewController.h"

@interface OrderWorkOrderDeatailViewController ()

@end

@implementation OrderWorkOrderDeatailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"代办处理单";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    WorkOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[WorkOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.acceptBtn addTarget:self action:@selector(clickBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    switch (indexPath.row) {
        case 0:
            [cell cellLBData:self.JsonDno];
            break;
        case 1:
            [cell cellLBData:self.JsonAddress];
            break;
        case 2:
            cell.leftLB.text = @"任务描述";
            break;
        case 4:
            cell.leftLB.text = @"实际收费";
            break;
            
        default:
            break;
    }
    
    return cell;
    
}
/**
 把提交按钮放在footView，当键盘出现时按钮会升高，而放在cell里面点击时获取不到其他cell的内容，所以这样来初始化cell
 
 */
#pragma mark 初始化cell
-(void)initCell{
    
    self.reasonCell = [self tableViewCell:2];
    self.resultCell = [self tableViewCell:3];
    self.moneyCell = [self tableViewCell:4];
    
}

-(WorkOrderCell*)tableViewCell:(NSInteger)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    WorkOrderCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

/**
 *  获取接口要的参数
 *
 *
 *  @return 接口参数
 */
#pragma mark - 设置提交接口参数
-(NSArray*)getParamsforFinishMission{
    
    MissionModel *mission = [[MissionModel alloc] init];
    //mission.uid = @"";
    mission.id = @"";
    mission.dno = self.JsonDno;
    mission.reason = self.reasonCell.reasonTXT.text;
    mission.result = self.resultCell.resultTXT.text;
    mission.fee = self.moneyCell.moneyTXT.text;
    mission.score = @"0";
    
    NSMutableDictionary *mdic = [NSMutableDictionary dictionary];
    mdic = [mission mj_keyValues];
    NSString *jsonStr = [Utils dictionaryToJsonStriing:mdic];
    NSDictionary *missionDic = @{@"mrstr":jsonStr};
    
    return @[missionDic];
}

-(void)clickBtnAction:(UIButton* )sender{
    
    [self initCell];
    UIButton* btn = sender;
    switch (btn.tag) {
        case 2000:
        {
            NSLog(@"提交");
            //[Utils showMessage:@"您已经完成该任务！" Delegate:self];
            if (self.reasonCell.reasonTXT.text.length == 0) {
                [Utils showMessage:@"任务描述不能为空" Delegate:nil];
                return;
            }else if (self.resultCell.resultTXT.text.length == 0){
                [Utils showMessage:@"处理结果不能为空" Delegate:nil];
                return;
            }else if (self.moneyCell.moneyTXT.text.length == 0){
                [Utils showMessage:@"实际收费不能为空" Delegate:nil];
                return;
            }else{
                
                WEAKSELF
                [AFSoapRequestOperation ElectricianFinishMissionSoapRequestWithParams:[weakSelf getParamsforFinishMission]
                                                                        hudShowInView:weakSelf.view
                                                                                block:^(NSString *jsonStr) {
                                                                                    
                                                                                    STRONGSELF
                                                                                    NSDictionary *dic = [jsonStr JSONValue];
                                                                                    
                                                                                    NSString *result = dic[@"result"];
                                                                                    
                                                                                    if (result.intValue == 1) {
                                                                                        
                                                                                        [UIAlertView bk_showAlertViewWithTitle:@"您已经完成该任务！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                                            
                                                                                            [strongSelf.navigationController popViewControllerAnimated:YES];
                                                                                        }];
                                                                                        if (strongSelf.FinishReqeust) {
                                                                                            
                                                                                            strongSelf.FinishReqeust(YES);
                                                                                            
                                                                                        }
                                                                                    }else{
                                                                                        
                                                                                        [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                                                    }
                                                                                    
                                                                                }];
                
            }
            
            
        }
            break;
        case 2001:
        {
            NSLog(@"取消");
        }
            break;
            
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
