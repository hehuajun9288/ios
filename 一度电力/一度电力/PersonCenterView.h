//
//  PersonCenterView.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCenterView : UIView

//@property (nonatomic, strong) UILabel *userNumberLB; //用户编号
//@property (nonatomic, strong) UILabel *userNameLB;      //用户名

/**
 *  利用block 返回点击时imageView的tag
 */
@property (nonatomic, copy) void (^HangleImageViewTag) (NSInteger imageViewTag);

/**
 *  初始化视图
 *
 *  @param frame      frame
 *  @param imageNameA imageView的图片名称
 *  @param imageNameB imageView的图片名称
 *  @param labelText  imageView显示的文字
 *
 *  @return self
 */
//-(instancetype)initWithFrame:(CGRect)frame
//                   imageNameA:(NSString*)imageNameA
//                   imageNameB:(NSString*)imageNameB
//                   labelTextA:(NSString*)labelTextA
//                   labelTextB:(NSString*)labelTextB;


/**
 *  初始化视图
 *
 *  @param frame      imageView frame
 *  @param labelTexts iamgeView 中显示的文本
 *  @param imageNames 图片名称
 *
 *  @return self
 */
-(instancetype)initWithFrame:(CGRect)frame
                  labelTexts:(NSArray*)labelTexts
                  imageNames:(NSArray*)imageNames;
 @end
