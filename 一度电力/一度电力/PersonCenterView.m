//
//  PersonCenterView.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "PersonCenterView.h"

@implementation PersonCenterView

#pragma mark - 初始化
-(instancetype)initWithFrame:(CGRect)frame
                  labelTexts:(NSArray*)labelTexts
                  imageNames:(NSArray*)imageNames{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self creatViewWithLabelTexts:labelTexts
                           imageNames:imageNames];
    }
    
    return self;

}

//#define infoViewHeight  (SCREEN_HEIGHT * 0.264)
//#define label_space 10
//#define label_space_x 20

#define Start_X 20.0f           // 第一个label的X坐标
#define Start_Y 20.0f           // 第一个label的Y坐标
#define Width_Space 0.0f        // 2个label之间的横间距
#define Height_Space 15.0f      // 竖间距
#define label_Height 20    // 高
#define label_Width (SCREEN_HEIGHT - Start_X *2)       // 宽#define lbFontSize   (SCREEN_HEIGHT * 0.035)

-(void)creatViewWithLabelTexts:(NSArray*)labelTexts
                    imageNames:(NSArray*)imageNames{

    UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    infoView.backgroundColor = RGBA(46, 154, 109, 1);
    [self addSubview:infoView];
    
    NSArray *array = @[[NSString stringWithFormat:@"用户编号: %@",App_Delegate.userModel.uno],[NSString stringWithFormat:@"用户姓名: %@",App_Delegate.userModel.realname],[NSString stringWithFormat:@"账户余额: %@",App_Delegate.userModel.overage]];
    
    UILabel *label ;
    for (int i = 0; i < array.count; i++) {
        
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        
        CGRect frame = CGRectMake(index * (label_Width + Width_Space) + Start_X, page  * (label_Height + Height_Space)+Start_Y, label_Width, label_Height);
        
        label = [self creatLabel:frame LableText:array[i]];
        [infoView addSubview:label];
    }
    
    CGRect frame = infoView.frame;
    frame.size.height = label.HEIGTH + label.Y + Start_Y;
    infoView.frame  = frame;
    
    
    //如果是电工登陆 不信息 用户编号和用户姓名视图 高度置0
    if (App_Delegate.userModel.utypeid.intValue == 6 || App_Delegate.userModel.utypeid.intValue == 5 ) {
        
        CGRect infoViewFrame = infoView.frame;
        infoViewFrame.size.height = 0;
        infoView.frame = infoViewFrame;
    }

    CGFloat startX = (SCREEN_WIDTH - SCREEN_WIDTH * 0.313 * 2) / 3;
    CGFloat iv_start_y = infoView.Y + infoView.HEIGTH + 20;
    CGFloat iv_width = SCREEN_WIDTH * 0.313;
    CGFloat iv_height = SCREEN_HEIGHT * 0.229;
    
    for (int i = 0; i < imageNames.count; i++) {
        
        NSInteger index = i % 2;
        NSInteger page = i / 2;
        
        CGRect frame = CGRectMake(index * (iv_width + startX) + startX, page  * (iv_height + Height_Space)+iv_start_y, iv_width, iv_height);
        
        UIImageView *iv = [self creatImageView:frame
                                     imageName:imageNames[i]
                                           tag:i
                                     labelText:labelTexts[i]];
        [self addSubview:iv];
    }

    
}

-(UILabel*)creatLabel:(CGRect)frame
        LableText:(NSString *)labelText {

    UILabel *label =  [UILabel labelWithFrame:frame
                  LableText:labelText
                       size:App_Delegate.largeFont
                  textColor:WHITE_COLOR
              textAlignment:NSTextAlignmentLeft];
    
    label.font = [UIFont fontWithName:@"Arial" size:App_Delegate.largeFont];
    
    return label;

}

-(UIImageView*)creatImageView:(CGRect)frame  imageName:(NSString*)imageName tag:(NSInteger)tag labelText:(NSString*)labelText{

    
    UIImageView *iv = [UIImageView imageWithFrame:frame backgroundColor:CLEAR_COLOR imageNamed:imageName];
    iv.tag = tag;
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGR:)];
    [iv addGestureRecognizer:tapGR];
    
    UILabel *label = [UILabel labelWithFrame:CGRectMake(0, iv.HEIGTH - 40, iv.WIDTH, 20) LableText:labelText size:App_Delegate.largeFont textColor:WHITE_COLOR textAlignment:NSTextAlignmentCenter];
    [iv addSubview:label];
    
    return iv;
}

-(void)tapGR:(UITapGestureRecognizer*)tapGR{
    
    if (self.HangleImageViewTag) {
        
        self.HangleImageViewTag(tapGR.view.tag);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
