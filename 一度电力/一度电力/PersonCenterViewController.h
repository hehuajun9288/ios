//
//  PersonInfoViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonCenterView.h"

@interface PersonCenterViewController : UIViewController

@property (nonatomic, strong) PersonCenterView *personCenterView;

-(void)initPersonCenterView;

@end
