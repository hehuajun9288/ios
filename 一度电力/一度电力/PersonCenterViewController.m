//
//  PersonInfoViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "PersonCenterViewController.h"
#import "TastStateTableViewController.h"
#import "MenuView.h"
#import "YiJianBaoZhangViewController.h"
#import "LoginViewController.h"

@interface PersonCenterViewController ()<RemoveMenuViewDelegate>

@property (nonatomic, strong) MenuView *menuView;
@end

@implementation PersonCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = App_Delegate.userModel.loginname;
    self.view.backgroundColor = WHITE_COLOR;

    [self initNavigationBar];
    
    //初始化视图
    [self initPersonCenterView];
    
    //隐藏返回按钮
    [self hideLeftbarButtonItem];
    
}

#pragma mark - 创建头像和登录名的视图
-(void)initNavigationBarTitleView{

    UIView *barTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - self.navigationItem.rightBarButtonItem.width, 44)];
    barTitleView.backgroundColor = CLEAR_COLOR;
    self.navigationItem.titleView = barTitleView;
    barTitleView.backgroundColor = [UIColor redColor];
    UIImageView *imageView = [UIImageView imageWithFrame:CGRectMake(VIEW_START_X, VIEW_START_Y, 30, 30) backgroundColor:CLEAR_COLOR imageNamed:@"头像"];
    [barTitleView addSubview:imageView];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //开启滑动返回
    self.fd_interactivePopDisabled = NO;
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    //禁止滑动返回
    self.fd_interactivePopDisabled = YES;
    
}

#pragma mark - 隐藏左边的返回按钮
-(void)hideLeftbarButtonItem{

    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.leftBarButtonItem = bar;

}

#pragma mark - 初始化视图
-(void)initPersonCenterView{

//    _personCenterView = [[PersonCenterView alloc] initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT)];
//    [self.view addSubview:_personCenterView];
//    
//    WEAKSELF
//    weakSelf.personCenterView.HangleImageViewTag = ^(NSInteger imageViewTag){
//        
//        STRONGSELF
//        if (imageViewTag == 0) {//一键报障
//            
//            YiJianBaoZhangViewController *yjbzVC = [[YiJianBaoZhangViewController alloc] init];
//            [strongSelf.navigationController pushViewController:yjbzVC animated:YES];
//            
//        }else {//任务状态
//            
//            TastStateTableViewController *tastVC = [[TastStateTableViewController alloc] init];
//            [strongSelf.navigationController pushViewController:tastVC animated:YES];
//        }
//    };

}
#pragma mark -创建临时按钮 调试界面
-(void)initNavigationBar{
    
    UIButton *button = [UIButton buttonWithFrame:CGRectMake(0, 0, 8, 20) imageNamed:@"more" selecteImageNamed:@"more" action:@selector(menuAction) target:self tag:0];

    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = bar;
    
}

#pragma mark - 菜单选项
-(void)menuAction{
    
    if (!_menuView) {
        
        _menuView = [[MenuView alloc] initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT)];
        _menuView.delegate = self;
        [_menuView showInView:self.view];
    }else{
        
        [_menuView dismiss];
    }
}

#pragma mark - menuView delegate
-(void)removeMenuView{
    
    [_menuView removeFromSuperview];
    _menuView = nil;
    
}

-(void)popToLoginViewController{

    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[LoginViewController class]]) {
            
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}
-(void)pushViewController:(id)viewController{

    [self.navigationController pushViewController:viewController animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//分割线左移
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.menuView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.menuView.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.menuView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.menuView.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
