//
//  PropertyUserViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/1/2.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "PropertyPersenCenterViewController.h"
#import "YiJianBaoZhangViewController.h"
#import "PropertyTSViewController.h"


@interface PropertyPersenCenterViewController ()

@end

@implementation PropertyPersenCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

#pragma mark - 初始化视图
-(void)initPersonCenterView{
    
    CGRect frame = CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT);
    self.personCenterView = [[PersonCenterView alloc] initWithFrame:frame
                                                         labelTexts:@[@"信息发布",@"任务状态"]
                                                         imageNames:@[@"故障申报",@"工单处理"]];
    
//    [[PersonCenterView alloc] initWithFrame:frame
//                                                         imageNameA:@"故障申报"
//                                                         imageNameB:@"工单处理"
//                                                         labelTextA:@"信息发布"
//                                                         labelTextB:@"任务状态"];
    [self.view addSubview:self.personCenterView];
    
    WEAKSELF
    weakSelf.personCenterView.HangleImageViewTag = ^(NSInteger imageViewTag){
        
        STRONGSELF //有self的地方要把注释打开 用strongself 代替self 防止弱引用
        if (imageViewTag == 0) {//信息发布
            

            YiJianBaoZhangViewController *yjbzVC = [[YiJianBaoZhangViewController alloc] init];
            yjbzVC.title = @"信息发布";
            [strongSelf.navigationController pushViewController:yjbzVC animated:YES];
            
        }else {//工单处理
            
            PropertyTSViewController *tastVC = [[PropertyTSViewController alloc] init];
            tastVC.labelTexts = @[@"工单号",@"日期",@"状态"];
            tastVC.mtype = @"1";
            [strongSelf.navigationController pushViewController:tastVC animated:YES];
        }
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
