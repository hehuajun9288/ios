//
//  RegInfon.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/1.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  注册是请求的对象
 */
@interface RegRequestInfon : NSObject

/**
 *  private String username;
	private String password;
	private String realname;
	private String address;
	private String mobile;
	private String email;
 */

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *email;

@end
