//
//  RegResult.h
//  一度电力
//
//  Created by 廖幸杰 on 16/1/1.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegResult : NSObject

@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *msg;

@end
