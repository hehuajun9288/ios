//
//  RegisterTableViewCell.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RegisterTableViewCell : MyTableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *usernameTF ; //账号
@property (nonatomic, strong) UITextField *realnameTF ; //姓名
@property (nonatomic, strong) UITextField *passwordTF ; //密码
@property (nonatomic, strong) UITextField *confirmPwdTF ; //确认密码输入框
@property (nonatomic, strong) UITextField *addressTF ;      //地址输入框
@property (nonatomic, strong) UITextField *mobileTF ; //地址输入框

@property (nonatomic, strong) UIButton *registerBtn ;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row;

@end
