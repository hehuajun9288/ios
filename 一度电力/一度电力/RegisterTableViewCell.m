//
//  RegisterTableViewCell.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "RegisterTableViewCell.h"

@interface RegisterTableViewCell ()

//@property (nonatomic, strong) NSString *string;

@end
@implementation RegisterTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        switch (row) {
            case 0://账号
                _usernameTF = [self cellView:row imageName:@"account" tfPlacehold:@"账号" isHasLable:YES];
                _usernameTF.keyboardType = UIKeyboardTypeNamePhonePad;
                break;
            case 1://姓名
                
                _realnameTF = [self cellView:row imageName:@"account" tfPlacehold:@"姓名" isHasLable:YES];
            
                break;
            case 2://密码
                _passwordTF = [self cellView:row imageName:@"密码" tfPlacehold:@"密码" isHasLable:YES];
                _passwordTF.secureTextEntry = YES;

                break;
            case 3://确认密码
                _confirmPwdTF = [self cellView:row imageName:@"密码" tfPlacehold:@"确认密码" isHasLable:YES];
                _confirmPwdTF.secureTextEntry = YES;
                break;
            case 4://手机号
               
                _mobileTF = [self cellView:row imageName:@"手机号" tfPlacehold:@"手机号" isHasLable:YES];
                _mobileTF.keyboardType = UIKeyboardTypeNumberPad;
                
                break;
             case 5://地址
                _addressTF = [self cellView:row imageName:@"account" tfPlacehold:@"地址" isHasLable:YES];
                _addressTF.keyboardType = UIKeyboardTypeDefault;
                break;
                case 6:
                [self creatRegisterButton];

                break;
            default:
                break;
        }
        
    }
    
    return self;
}

#pragma mark - 创建注册按钮
-(void)creatRegisterButton{
    
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    _registerBtn = [UIButton buttonWithFrame:frame
                                             title:@"注册"
                                          fontSize:App_Delegate.largeFont
                                        titleColor:WHITE_COLOR
                                            action:nil
                                            target:nil tag:0
                                   backgroundColor:BUTTON_BACKGROUND_COLOR];
    
    [self.contentView addSubview:_registerBtn];
}


#pragma mark - textField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    //输入字符长度
    NSInteger strLength = textField.text.length - range.length + string.length;
    
    if (textField.tag == 4) {//限制手机号码长度
        
        if (strLength > PHONE_NUMBER_LENGTH) {
            
            return NO;
        }
    }
    return YES;
}

@end
