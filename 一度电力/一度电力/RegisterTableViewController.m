//
//  RegisterTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "RegisterTableViewController.h"
#import "RegisterTableViewCell.h"
#import "RegRequestInfon.h"
#import "RegResult.h"
#import "LoginViewController.h"

@interface RegisterTableViewController ()

@property (nonatomic, strong) RegisterTableViewCell *usernameCell;
@property (nonatomic, strong) RegisterTableViewCell *realnameCell;
@property (nonatomic, strong) RegisterTableViewCell *passwordCell;
@property (nonatomic, strong) RegisterTableViewCell *confirmPwdCell;
@property (nonatomic, strong) RegisterTableViewCell *mobileCell;
@property (nonatomic, strong) RegisterTableViewCell *addressCell;

@property (nonatomic, strong) NSString *veryfiCodeStr; //验证码
@end

@implementation RegisterTableViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -view lifeCycle
-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [self resignFirstResponderForSelf];
}

-(void)resignFirstResponderForSelf{

    [_usernameCell.usernameTF resignFirstResponder];
    [_passwordCell.passwordTF resignFirstResponder];
    [_realnameCell.realnameTF resignFirstResponder];
    [_confirmPwdCell.confirmPwdTF resignFirstResponder];
    [_mobileCell.mobileTF resignFirstResponder];
    [_addressCell.addressTF resignFirstResponder];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - tableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 7;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    RegisterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        
        cell = [[RegisterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //注册按钮添加方法
        [cell.registerBtn addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    //    cell.textLabel.text = @"dfdf";
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return TABLE_VIEW_HEIGHT_FOR_ROW;
}

#pragma mark - registerAction注册
-(void)registerAction{

    _usernameCell = [self cellForRow:0];
    _realnameCell = [self cellForRow:1];
    _passwordCell = [self cellForRow:2];
    _confirmPwdCell = [self cellForRow:3];
    _mobileCell = [self cellForRow:4];
    _addressCell = [self cellForRow:5];

    [self resignFirstResponderForSelf];
    if ([self checkInfo]) {
        
        //初始化注册对象
        RegRequestInfon *regInfo = [[RegRequestInfon alloc] init];
        regInfo.username = _usernameCell.usernameTF.text;
        regInfo.realname = _realnameCell.realnameTF.text;
        regInfo.password = _passwordCell.passwordTF.text;
        regInfo.address = _addressCell.addressTF.text;
        regInfo.mobile = _mobileCell.mobileTF.text;
        regInfo.email = @"";
        
        //对象转字典
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        //对象转字典
        dic = [regInfo mj_keyValues];

        //字典转字符串
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        //参数
        NSDictionary *infonDic = @{@"reginfo":jsonStr};
        
        //请求
        WEAKSELF
        [AFSoapRequestOperation registerSoapRequestWithParams:@[infonDic] hudShowInView:weakSelf.view block:^(NSString *jsonStr) {
            
            STRONGSELF
            RegResult *regResult = [RegResult mj_objectWithKeyValues:[jsonStr JSONValue]];
            
            if (regResult.result.integerValue == 1) {
                
                [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您已注册成功,请登录..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [strongSelf popToLoginViewController];
                }];
             }else{
            
                 [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,注册失败,请与客服联系..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];

            }
        }];
    }
}

#pragma mark - 返回登陆界面
-(void)popToLoginViewController{
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[LoginViewController class]]) {
            
            [self.navigationController popToViewController:vc animated:YES];
        }
    }


}
#pragma mark - 根据row获取tableView的cell
-(RegisterTableViewCell*)cellForRow:(NSInteger)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    RegisterTableViewCell *cell = (RegisterTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

#pragma mark - 检查输入框的内容
-(BOOL)checkInfo{

    if (_usernameCell.usernameTF.text.length < 5){
    
        [self showHUDWithHintText:@"账号名称不能少于5个字符"];
        return NO;
        
    }else if (_realnameCell.realnameTF.text.length <= 0){
    
        [self showHUDWithHintText:@"姓名不能为空"];
        return NO;
    }
    else if (![Utils isContainChinese:_passwordCell.passwordTF.text]){
        
        [self showHUDWithHintText:@"密码包含中文字符"];
        return NO;
        
    }else if (_passwordCell.passwordTF.text.length < 6){
        
        [UIAlertView bk_showAlertViewWithTitle:PWD_ALERT_TEXT message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
        return NO;
        
    }else if (![_passwordCell.passwordTF.text isEqualToString:_confirmPwdCell.confirmPwdTF.text]){
    
        [self showHUDWithHintText:@"输入密码不一致"];
        return NO;
    } else if (![Utils isValidateMobile:_mobileCell.mobileTF.text]) {
        
        [self showHUDWithHintText:@"请输入有效的手机号"];
        return NO;
        
    }else if (_addressCell.addressTF.text.length <= 0){
    
        [self showHUDWithHintText:@"地址不能为空"];
        return NO;
    }
    return YES;
}
#pragma mark -显示提示内容
-(void)showHUDWithHintText:(NSString*)hintText{
    
    [Utils showHUD:hintText autoHide:YES inView:self.view];
    
}

@end
