//
//  SalesmanViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 16/4/25.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "SalesmanViewController.h"
#import "AcceptOrderViewController.h"
#import "OrderRepairViewController.h"
#import "SalesmanHistoricalTaskViewController.h"


@interface SalesmanViewController ()

@end

@implementation SalesmanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - 初始化视图
-(void)initPersonCenterView{
    
    CGRect frame = CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT);
    self.personCenterView = [[PersonCenterView alloc] initWithFrame:frame
                                                         labelTexts:@[@"接工单",@"工单处理",@"历史任务"] imageNames:@[@"接工单",@"工单处理",@"工单处理"]];
    
    [self.view addSubview:self.personCenterView];
    
    WEAKSELF
    weakSelf.personCenterView.HangleImageViewTag = ^(NSInteger imageViewTag){
        
        STRONGSELF
        if (imageViewTag == 0) {//接工单

            [strongSelf pushVC:[[AcceptOrderViewController alloc] init]];
        }else if(imageViewTag == 1){//工单处理
            
            [strongSelf pushVC:[[OrderRepairViewController alloc] init]];

        }else if(imageViewTag == 2){//历史任务
            [strongSelf pushVC:[[SalesmanHistoricalTaskViewController alloc] init]];

        }
    };
    
}

-(void)pushVC:(id)viewController{

    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
