//
//  TastStateTableViewController.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElectricianTaskDeatailViewController.h"

@interface TastStateTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *labelTexts;

/**
 *  继承与此类的子类 刷新和加载更多只需重写这两个方法即可
 *
 */
//下拉刷新方法
-(void)headerWithRefresh;

//下拉加载更多方法
-(void)footerWithRefresh;

-(UIView*)creatTableHeaderView:(NSArray*)labelTexts;

@end
