//
//  TastStateTableViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "TastStateTableViewController.h"


@interface TastStateTableViewController ()

@end

@implementation TastStateTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"任务状态";
    self.view.backgroundColor = WHITE_COLOR;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // 下拉刷新
    self.tableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self headerWithRefresh];
      
    }];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    
    // 上拉刷新
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        [self footerWithRefresh];
        
    }];

    
}

//下拉刷新方法
-(void)headerWithRefresh{

    // 结束刷新
    [self.tableView.mj_header endRefreshing];
}

//下拉加载更多方法
-(void)footerWithRefresh{

    // 结束刷新
    [self.tableView.mj_footer endRefreshing];

}

-(UIView*)creatTableHeaderView:(NSArray*)labelTexts{
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT, 20)];
    CGFloat labelWidth = SCREEN_WIDTH / labelTexts.count;
    
    //@"工单号":@"日期"
    for (int i = 0; i < labelTexts.count; i++) {
        
        CGRect frame = CGRectMake(labelWidth * i, 0, labelWidth, 20);
        UILabel *lable = [UILabel labelWithFrame:frame
                                       LableText:labelTexts[i]
                                            size:App_Delegate.largeFont
                                       textColor:[UIColor lightGrayColor]
                                   textAlignment:NSTextAlignmentCenter];
        [tableHeaderView addSubview:lable];
        
    }
    
    return tableHeaderView;
}

-(UIView*)creatTableHeaderView11{

    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT, 40)];
    
//    //这样创建是为了和普通用户的cell内容对称，如果cell的内容不是和这里的一样，重写tableHeaderview的方法即可
    CGFloat labelWidth = (SCREEN_WIDTH - VIEW_START_X * 2) / 3;
    
    
    CGRect frame = CGRectMake(VIEW_START_X, VIEW_START_Y, labelWidth , 20);
    
    UILabel * _workOrderNunLB = [UILabel labelWithFrame:frame
                                    LableText:@"工单号"
                                         size:App_Delegate.largeFont
                                              textColor:BLACK_COLOR
                                textAlignment:NSTextAlignmentCenter];
    [tableHeaderView addSubview:_workOrderNunLB];
    _workOrderNunLB.lineBreakMode = NSLineBreakByWordWrapping;
    //日期
    frame.origin.x = _workOrderNunLB.X + _workOrderNunLB.WIDTH;
    UILabel *_dateLB = [UILabel labelWithFrame:frame
                            LableText:@"日期"
                                 size:App_Delegate.largeFont
                            textColor:BLACK_COLOR
                        textAlignment:NSTextAlignmentCenter];
    [tableHeaderView addSubview:_dateLB];
    
    //状态
    
    frame.origin.x = _dateLB.X + _dateLB.WIDTH;
    UILabel *_statusLB = [UILabel labelWithFrame:frame
                              LableText:@"状态"
                                   size:App_Delegate.largeFont
                              textColor:BLACK_COLOR
                          textAlignment:NSTextAlignmentCenter];
    [tableHeaderView addSubview:_statusLB];
    return tableHeaderView;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    return cell;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    return 44;//TABLE_VIEW_HEIGHT_FOR_ROW;
//}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    return [self creatTableHeaderView11];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{


    return [self creatTableHeaderView11].HEIGTH;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ElectricianTaskDeatailViewController *deatailVC = [[ElectricianTaskDeatailViewController alloc] init];
    [self.navigationController pushViewController:deatailVC animated:YES];
    
}

//分割线左移
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
