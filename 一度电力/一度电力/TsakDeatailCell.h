//
//  TsakDeatailCell.h
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "MyTableViewCell.h"
#import "FatherTableViewCell.h"
#import "Mission.h"

@interface TsakDeatailCell : FatherTableViewCell
@property (nonatomic, strong) UILabel *leftLB; //左边的label
@property (nonatomic, strong) UILabel *rightLB; //右边的
@property(nonatomic,strong)UILabel* numTXT;
@property(nonatomic,strong)UILabel* addressTXT;
@property(nonatomic,strong)UILabel* timeTXT;
@property(nonatomic,strong)UILabel* phoneTXT;
@property(nonatomic,strong)UILabel* breakdownTXT;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row;


-(void)cellData:(NSString*)leftLBText rightLBText:(NSString *)rightLBText;

@end
