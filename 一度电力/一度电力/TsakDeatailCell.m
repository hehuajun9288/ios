//
//  TsakDeatailCell.m
//  一度电力
//
//  Created by HO on 16/1/4.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "TsakDeatailCell.h"

@interface TsakDeatailCell ()



@property (nonatomic, strong) UIView *whiteContentView; //白色视图
@end
@implementation TsakDeatailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        [self creatLB];
        
//        switch (row) {
//            case 0://编号
//                self.numTXT = [self cellView:row tfLabel:@"编号" isHasLable:YES];
//                //self.numTXT.text = @"1234567";
//                
//                break;
//            case 1://地址
//                self.addressTXT = [self cellView:row tfLabel:@"地址" isHasLable:YES];
//                //self.addressTXT.text = @"广州市越秀区东华南路98号广州市越秀区东华南路98号";
//                
//                break;
//            case 2://时间
//                self.timeTXT = [self cellView:row tfLabel:@"时间" isHasLable:YES];
//                //self.timeTXT.text = @"2016-01-01";
//                
//                break;
//            case 3://手机号
//                
//                self.phoneTXT = [self cellView:row tfLabel:@"手机号" isHasLable:YES];
//                //self.phoneTXT.text = @"15088888888";
//                
//                
//                break;
//            case 4://故障描述
//                
//                self.breakdownTXT = [self cellView:row tfLabel:@"故障描述" isHasLable:YES];
//                //self.breakdownTXT.text = @"灯管不亮";
//                
//                break;
//            default:
//                break;
//        }
        
    }
    
    return self;
}

-(void)creatLB{

    //白色背景视图
    _whiteContentView = [[UIView alloc] initWithFrame:CGRectMake(5, VIEW_START_Y, SCREEN_WIDTH - 10 , TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
    _whiteContentView.backgroundColor = WHITE_COLOR;
    [self addSubview:_whiteContentView];
    
    CGRect frame = CGRectMake(VIEW_START_X, 0, 0, _whiteContentView.HEIGTH );

    _leftLB = [UILabel labelWithFrame:frame
                            LableText:@""
                                 size:App_Delegate.largeFont
                            textColor:BLACK_COLOR
                        textAlignment:NSTextAlignmentLeft];
    [_whiteContentView addSubview:_leftLB];
    
    frame = _leftLB.frame;
    frame.origin.x = frame.origin.x + frame.size.width + VIEW_START_X;
    
    _rightLB = [UILabel labelWithFrame:frame
                             LableText:@""
                                  size:App_Delegate.largeFont
                             textColor:[UIColor grayColor]
                         textAlignment:NSTextAlignmentLeft];
    [_whiteContentView addSubview:_rightLB];
}

-(void)cellData:(NSString*)leftLBText rightLBText:(NSString *)rightLBText{

    CGSize size = [Utils getTextSize:leftLBText
                          textRegion:CGSizeMake(200, 1000)
                        textFontSize:App_Delegate.largeFont];
    
    CGRect frame = _leftLB.frame;
    frame.size.width = size.width;
    _leftLB.frame = frame;
    _leftLB.text = leftLBText;

    size = [Utils getTextSize:rightLBText
                   textRegion:CGSizeMake(_whiteContentView.WIDTH - _leftLB.X * 3 - _leftLB.WIDTH, 1000)
                 textFontSize:App_Delegate.largeFont];
    
    frame = _rightLB.frame;
    frame.size.height = size.height > _rightLB.HEIGTH ? size.height:frame.size.height;
    frame.size.width = size.width;
    frame.origin.x = _leftLB.X + _leftLB.WIDTH + VIEW_START_X;
    _rightLB.frame = frame;
    _rightLB.text = rightLBText;

    CGPoint center = _leftLB.center;
    center.y = _rightLB.center.y;
    _leftLB.center = center;
    
    frame = _whiteContentView.frame;
    frame.size.height = _rightLB.Y + _rightLB.HEIGTH;
    _whiteContentView.frame = frame;
    
    frame = self.frame;
    frame.size.height = _whiteContentView.HEIGTH + _whiteContentView.Y + VIEW_START_Y;
    self.frame = frame;
}
@end
