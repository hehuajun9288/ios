//
//  URL.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#ifndef URL_h
#define URL_h

//如果要改ip调试只需改这里就好
// 119.33.36.159 119.33.35.10:8820
// 119.33.36.159
//开发测试ip  119.33.35.10

//开发者账号
//231120718@qq.com
//密码159Zyl7428

/* 线上测试账号
公网IP120.55.166.204

普通用户 liaoxingjie 111111
电工lijiang
hhhhj业务员
密码都是123456
 
 */
//线上ip   120.55.166.204
#define IP                                   @"119.33.35.10:8820"


#define IPNAME1                              @"epower/service"

#define IPNAME2          [NSString  stringWithFormat:@"http://%@/%@",IP,IPNAME1]

#define BASEURL(INTERFACE)         [NSString stringWithFormat:@"%@/%@",IPNAME2,INTERFACE]


#define user_Class                           @"user"

/**
 *  user 接口下得请求
 *
 */
//登录
#define login                                @"login"

//忘记密码
#define doForgetPwd                          @"doForgetPwd"

//注册
#define REGISTER                            @"register"

//修改密码
#define toModifyPwd                         @"toModifyPwd"

//修改基本信息
#define toModifyInfo                         @"toModifyInfo"


//========================================================
#define address_Class                           @"address"

/**
 *  address接口下的请求
 
 */

//获取用户所有地址
#define toGetMyAddress                         @"toGetMyAddress"

//添加地址
#define toAddAddress                         @"toAddAddress"

//修改地址
#define toModifyAddress                         @"toModifyAddress"

//获取有效地址
#define toGetMyValidAddress                         @"toGetMyValidAddress"

//=================================================================
#define mission_Class                           @"mission"

/**
 *  mission接口的请求
 */

//一键保障
#define toSendWarning     @"toSendWarning"

//检查之前任务是否全部完成
#define getCountUnfinishMission     @"getCountUnfinishMission"

//获取历史任务
#define getHistoryMissionRecord @"getHistoryMissionRecord"

//获取任务详情 getMissionDetail
#define getMissionDetail    @"getMissionDetail"

//任务进度查询
#define getMissions   @"getMissions"

//工单任务评分
#define getMissionDetail @"getMissionDetail"

//电工工单任务列表
#define electricianGetMissionList @"getAllMissions"

//电工工单任务详情
#define electricianGetMissionDetail @"getMissionDetail"

//电工接受工单任务
#define electricianReceiveMission @"recvMission"

//电工完成工单任务
#define electricianFinishMission @"finishMission"

//评分
#define toScore @"toScore"

#define bill_Class                           @"bill"

//获取账户信息
#define getBillList @"getBillList"

//获取app版本号
#define getAppVersion @"getAppVersion"


#endif /* URL_h */
