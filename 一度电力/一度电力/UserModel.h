//
//  UserModel.h
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

/**
 *  "id": 102,
 "loginname": "work1",
 "realname": "维护工1",
 "passwd": "e10adc3949ba59abbe56e057f20f883e",
 "utypeid": 6,
 "regdate": "2015-12-03 10:45:58.0",
 "mobile": "13973101010",
 "addresses": [
 ],
 "uno": "HC20000001"
 */

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *loginname;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *passwd;
@property (nonatomic, strong) NSString *utypeid;
@property (nonatomic, strong) NSString *regdate;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSArray *addresses;
@property (nonatomic, strong) NSString *uno;
@property (nonatomic, strong) NSString *overage; //余额

@end
