//
//  UserModel.m
//  一度电力
//
//  Created by gn_macMini_liao on 15/12/31.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "UserModel.h"
#import "Address.h"

@implementation UserModel


- (NSDictionary *)objectClassInArray

{
    return @{@"addresses" : [Address class]};
    
}
@end
