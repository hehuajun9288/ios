//
//  Utils.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Utils : NSObject

/**
 *  HUD 显示
 *
 *  @param str      加载文字
 *  @param autoHide yes 为只显示文本的大小 hud会自动消失 No为从导航栏开始的全屏大小 要手动调用隐藏方法
    @param view     hud 显示的父试图    
 */
+ (void)showHUD:(NSString *)str autoHide:(BOOL)autoHide inView:(UIView*)view;

/**
 *  HUD 隐藏
 */
+ (void)hideHUD;


/**
 *  设置视图背景颜色
 *
 *  @param view 视图
 */
+(void)setBackgroudColorWithView:(UIView*)view;

/**
 *  计算文本的宽高
 *
 *  @param text     文本内容
 *  @param size     文在所在区域的size
 *  @param fontsize 字体大小
 *
 *  @return 文本所占的size
 */
+(CGSize)getTextSize:(NSString*)text
          textRegion:(CGSize)size
        textFontSize:(CGFloat)fontsize ;

/**
 *  手机号验证
 *
 *  @param mobile 手机号
 *
 *  @return bool
 
 */
+ (BOOL)isValidateMobile:(NSString *)mobile;

/**
 *  判断字符串是否包含中文
 *
 *  @param str 字符串
 *
 *  @return yes or no
 */
+(BOOL)isContainChinese:(NSString*)str;


/**
 *  字典转json字符串
 *
 *  @param dic 字典
 *
 *  @return return jsonString
 */
+(NSString*)dictionaryToJsonStriing:(NSDictionary*)dic;

//alertView
+ (void) showMessage:(NSString*) msg Delegate:(id)delegate;

@end
