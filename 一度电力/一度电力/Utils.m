//
//  Utils.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/20.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "Utils.h"
#import "CTMBHUDManager.h"

@implementation Utils

+ (void)showHUD:(NSString *)str
       autoHide:(BOOL)autoHide
         inView:(UIView *)view{
    
    [CTMBHUDManager showHUDWithToShowStr:str
                                 HUDMode:MBProgressHUDModeIndeterminate
                                autoHide:autoHide
                              afterDelay:HUDHIDEDELAY
                  userInteractionEnabled:NO
                        inView:view];
}


+ (void)hideHUD{
    [CTMBHUDManager hideHUD];
}

+(void)setBackgroudColorWithView:(UIView*)view{

    
    UIImage *image = [UIImage imageNamed:@"rectangle-1"];
    UIImage *resizeImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20) resizingMode:UIImageResizingModeTile];
    UIImageView *imageView = [UIImageView imageWithFrame:view.bounds backgroundColor:CLEAR_COLOR imageNamed:@"0"];
    imageView.image = resizeImage;
    [view addSubview:imageView];
}

/**
 *  计算文本的宽高
 *
 *  @param text     文本内容
 *  @param size     文在所在区域的size
 *  @param fontsize 字体大小
 *
 *  @return 文本所占的size
 */
+(CGSize)getTextSize:(NSString*)text
          textRegion:(CGSize)size
        textFontSize:(CGFloat)fontsize{

    NSDictionary *attributes = @{NSFontAttributeName :[UIFont systemFontOfSize:fontsize],};
    
    return  [text boundingRectWithSize:size
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:attributes
                               context:nil].size;
}

#pragma mark 手机号码验证
+ (BOOL)isValidateMobile:(NSString *)mobile
{
    // 手机号以13，14 15，17，18开头
    NSString *phoneRegex = @"^1[3|4|5|7|8][0-9]{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    // NSLog(@"phoneTest is %@",phoneTest);
    return [phoneTest evaluateWithObject:mobile];
}

#pragma mark - 判断字符串是否包含中文
+(BOOL)isContainChinese:(NSString*)str{
    
    for (int i=0; i< str.length; ++i)
    {
        NSRange range = NSMakeRange(i, 1);
        NSString *subString = [str substringWithRange:range];
        const char *cString = [subString UTF8String];
        if (strlen(cString) == 3)
        {
            NSLog(@"汉字:%s", cString);
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - 字典转字符串
+(NSString *)dictionaryToJsonStriing:(NSDictionary *)dic{

    //字典转字符串
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonStr;
}

//alertView
+(void) showMessage:(NSString*) msg Delegate:(id)delegate
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles:nil];
    alertView.tag = 2005;
    [alertView show];
    
}


@end
