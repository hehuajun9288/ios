//
//  WordOrderDeatailViewController.h
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "BasiInfoViewController.h"
#import "WorkOrderCell.h"
#import "Mission.h"
#import "MissionModel.h"

@interface WordOrderDeatailViewController : UITableViewController<UITextFieldDelegate>
@property(nonatomic,strong)Mission* receiveMissionModel;
@property(nonatomic,strong)NSString* JsonDno;
@property(nonatomic,strong)NSString* JsonAddress;
@property (nonatomic, strong) WorkOrderCell *reasonCell;
@property (nonatomic, strong) WorkOrderCell *resultCell;
@property (nonatomic, strong) WorkOrderCell *moneyCell;

@property (nonatomic, copy) void (^FinishReqeust)(BOOL finishRequest);
@end
