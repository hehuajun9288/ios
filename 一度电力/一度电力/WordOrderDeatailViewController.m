//
//  WordOrderDeatailViewController.m
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "WordOrderDeatailViewController.h"


@interface WordOrderDeatailViewController ()


@end

@implementation WordOrderDeatailViewController

-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"工单处理单";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(textDidChangeNotification:)
//                                                 name:UITextFieldTextDidChangeNotification
//                                               object:nil];
    [self RequestData];
}

/**
 把提交按钮放在footView，当键盘出现时按钮会升高，而放在cell里面点击时获取不到其他cell的内容，所以这样来初始化cell
 
 */
#pragma mark 初始化cell
-(void)initCell{

    _reasonCell = [self tableViewCell:2];
    _resultCell = [self tableViewCell:3];
    _moneyCell = [self tableViewCell:4];

}

-(WorkOrderCell*)tableViewCell:(NSInteger)row{

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    WorkOrderCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}
/**
 *  获取接口要的参数
 *
 *  @param offset 分页开始序号
 *
 *  @return 接口参数
 //     *  uid	用户id
 //     mtype	任务类型 普通用户 填2，物业填 1
 //     offset	分页开始序号
 //     size	每页内容数

 */
#pragma mark - 设置接口参数
-(NSArray*)getParams{
    
    NSDictionary *dnoDic = @{@"dno":self.receiveMissionModel.dno};
    return @[dnoDic];
}

#pragma mark - 请求数据
-(void)RequestData{
    WEAKSELF
    [AFSoapRequestOperation ElectricianGetMissionDeatailSoapRequestWithParams:[weakSelf getParams]
                                                                hudShowInView:weakSelf.view
                                                                        block:^(NSString *jsonStr) {
                                                                            
                                                                            STRONGSELF
                                                                            
                                                                            NSDictionary *dic = [jsonStr JSONValue];
                                                                            
                                                                            self.JsonDno = [dic valueForKey:@"dno"];
                                                                            self.JsonAddress = [dic valueForKey:@"address"];
//
//                                                                    self.JsonAddress = @"奖励积分了升级了房价数据上来看结果来说就是建立国家实力等级公路上攻击速度来看过介绍来的可根据历史的价格了手机登陆国家实力的结果了手机登陆国家实力的国家临时登记管理上的经历感觉是代理公司的离开";
//

                                                                            [strongSelf.tableView reloadData];
                                                                        }];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = @"Cell";
    WorkOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        
        cell = [[WorkOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID row:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.acceptBtn addTarget:self action:@selector(clickBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    switch (indexPath.row) {
        case 0:
            [cell cellLBData:self.JsonDno];
            break;
         case 1:
            [cell cellLBData:self.JsonAddress];
            break;

            
        default:
            break;
    }

    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld",indexPath.row);


}
-(void)textViewCell:(WorkOrderCell *)cell didChangeText:(NSString *)text{

    NSLog(@"text === %@",text);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    WorkOrderCell *cell = (WorkOrderCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        [cell cellLBData:self.JsonDno];

        return cell.HEIGTH;
    }else if (indexPath.row == 1){
    
        [cell cellLBData:self.JsonAddress];
        return cell.HEIGTH;
    }
    else if (indexPath.row == 2){
    
        [cell cellTextFieldData:_reasonCell.reasonTXT];
        
        return cell.HEIGTH;
    }else if (indexPath.row == 3){
    
        [cell cellTextFieldData:_resultCell.resultTXT];

        return cell.HEIGTH;
    }else if (indexPath.row == 4){
    
        [cell cellTextFieldData:_moneyCell.moneyTXT];
        return cell.HEIGTH;

    }else{
    
        return TABLE_VIEW_HEIGHT_FOR_ROW;
    }
}
#pragma mark ==========  Action  ===========

/**
 *  获取接口要的参数
 *
 *
 *  @return 接口参数
 */
#pragma mark - 设置提交接口参数
-(NSArray*)getParamsforFinishMission{

    MissionModel *mission = [[MissionModel alloc] init];
    //mission.uid = @"";
    mission.id = @"";
    mission.dno = self.JsonDno;
    mission.reason = _reasonCell.reasonTXT.text;
    mission.result = _resultCell.resultTXT.text;
    mission.fee = _moneyCell.moneyTXT.text;
    mission.score = @"0";
    
    NSMutableDictionary *mdic = [NSMutableDictionary dictionary];
    mdic = [mission mj_keyValues];
    NSString *jsonStr = [Utils dictionaryToJsonStriing:mdic];
    NSDictionary *missionDic = @{@"mrstr":jsonStr};
    
    return @[missionDic];
}

-(void)clickBtnAction:(UIButton* )sender{
    
    [self initCell];
    UIButton* btn = sender;
    switch (btn.tag) {
        case 2000:
        {
            NSLog(@"提交");
            //[Utils showMessage:@"您已经完成该任务！" Delegate:self];
            if (self.reasonCell.reasonTXT.text.length == 0) {
                [Utils showMessage:@"故障原因不能为空" Delegate:nil];
                return;
            }else if (self.resultCell.resultTXT.text.length == 0){
                [Utils showMessage:@"处理结果不能为空" Delegate:nil];
                return;
            }else if (self.moneyCell.moneyTXT.text.length == 0){
                [Utils showMessage:@"维护费用不能为空" Delegate:nil];
                return;
            }else{
            
                WEAKSELF
                [AFSoapRequestOperation ElectricianFinishMissionSoapRequestWithParams:[weakSelf getParamsforFinishMission]
                                                                            hudShowInView:weakSelf.view
                                                                                    block:^(NSString *jsonStr) {
                                                                                        
                                                                                        STRONGSELF
                            NSDictionary *dic = [jsonStr JSONValue];
                                                                                        
                                                                                        NSString *result = dic[@"result"];
                                                                                        
                                                                                        if (result.intValue == 1) {
                                                                                            
                                                                                            [UIAlertView bk_showAlertViewWithTitle:@"您已经完成该任务！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                                                
                                                                                                [strongSelf.navigationController popViewControllerAnimated:YES];
                                                                                            }];
                                                                                            if (strongSelf.FinishReqeust) {
                                                                                                
                                                                                                strongSelf.FinishReqeust(YES);
                                                                                                
                                                                                            }
                                                                                        }else{
                                                                                            
                                                                                            [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];
                                                                                        }
                                        
                                                                                    }];
               
            }
            
            
        }
            break;
        case 2001:
        {
            NSLog(@"取消");
        }
            break;
            
        default:
            break;
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
