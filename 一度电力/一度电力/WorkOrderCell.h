//
//  WorkOrderCell.h
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "FatherTableViewCell.h"

@interface WorkOrderCell : MyTableViewCell<UITextFieldDelegate>
@property (nonatomic, strong) UIView *whiteContentView ;
@property (nonatomic, strong) UILabel *rightLB ;
@property (nonatomic, strong) UILabel *leftLB ;
@property(nonatomic,strong) UITextField *reasonTXT;
@property(nonatomic,strong) UITextField *resultTXT;
@property(nonatomic,strong) UITextField *moneyTXT;
@property (nonatomic, strong) UIButton* acceptBtn ;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row;

-(void)cellLBData:(NSString*)rightLBText ;

-(void)cellTextFieldData:(UITextField*)textField;


@end
