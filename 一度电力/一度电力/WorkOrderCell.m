//
//  WorkOrderCell.m
//  一度电力
//
//  Created by HO on 16/1/5.
//  Copyright © 2016年 廖幸杰. All rights reserved.
//

#import "WorkOrderCell.h"
#import "AccountDetailTableViewCell.h"

@interface WorkOrderCell ()


@end
@implementation WorkOrderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier row:(NSInteger)row{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        
        self.whiteContentView = [[UIView alloc] initWithFrame:CGRectMake(5, VIEW_START_Y, SCREEN_WIDTH - 10, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2)];
        self.whiteContentView.backgroundColor = WHITE_COLOR;
        [self.contentView addSubview:self.whiteContentView];
        self.whiteContentView.userInteractionEnabled = YES;
        self.contentView.userInteractionEnabled = YES;
        
        self.contentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
        
        switch (row) {
            case 0://编号

                [self creatLBWithLabelText:@"编号"];
                break;
            case 1://地址
                [self creatLBWithLabelText:@"地址"];
                break;
            case 2://故障原因
                [self creatLeftLBWithLabelText:@"故障原因"];
                self.reasonTXT = [self creatTextField:row];

                break;
            case 3://处理结构
                [self creatLeftLBWithLabelText:@"处理结果"];
                self.resultTXT = [self creatTextField:row];

                break;
            case 4://维护费用
                [self creatLeftLBWithLabelText:@"维护费用"];
                self.moneyTXT = [self creatTextField:row];
                self.moneyTXT.keyboardType = UIKeyboardTypeDecimalPad;
                break;
                case 5:
            {
                _acceptBtn = [self creatButtonWhthTitle:@"提交"];
                _acceptBtn.tag = 2000;
                CGRect frame = _acceptBtn.frame;
                frame.origin.y = VIEW_START_Y * 2;
                _acceptBtn.frame = frame;
                _whiteContentView.backgroundColor = VIEW_BACKGROUND_COLOR_BLUE;
                
                
            }
                break;
            default:
                break;
        }
        
    }
    
    return self;
}



-(void)creatLBWithLabelText:(NSString *)labelText {

    [self creatLeftLBWithLabelText:labelText];
    [self creatRgihtLB];
}

-(void)creatRgihtLB{

    CGRect frame = _leftLB.frame;
    frame.origin.x = _leftLB.X + _leftLB.WIDTH + VIEW_START_X;
    
    _rightLB = [UILabel labelWithFrame:frame
                             LableText:@"sdf"
                                  size:App_Delegate.largeFont
                             textColor:[UIColor grayColor]
                         textAlignment:NSTextAlignmentLeft];
    [_whiteContentView addSubview:_rightLB];
}
-(void)creatLeftLBWithLabelText:(NSString *)labelText{

    CGSize size = [Utils getTextSize:labelText
                          textRegion:CGSizeMake(200, 100)
                        textFontSize:App_Delegate.largeFont];

    CGRect frame = CGRectMake(VIEW_START_X, 0, size.width, _whiteContentView.HEIGTH );
    _leftLB = [UILabel labelWithFrame:frame
                            LableText:labelText
                                 size:App_Delegate.largeFont
                            textColor:BLACK_COLOR
                        textAlignment:NSTextAlignmentLeft];
    
    [_whiteContentView addSubview:_leftLB];

    
}

-(UITextField*)creatTextField:(NSInteger)row{

    CGRect frame = CGRectMake(_leftLB.X + _leftLB.WIDTH + VIEW_START_X, 0, _whiteContentView.WIDTH - _leftLB.X * 3 - _leftLB.WIDTH, _whiteContentView.HEIGTH);
    
    UITextField *tf = [UITextField textFieldWithFrame:frame
                                            placehold:@"请输入"
                                               target:self
                                                  tag:row];
    [_whiteContentView addSubview:tf];
    
    return tf;
    
 }

#pragma mark - 设置cell内容
-(void)cellLBData:(NSString*)rightLBText{

    CGSize size = [Utils getTextSize:rightLBText
                          textRegion:CGSizeMake(_whiteContentView.WIDTH - _leftLB.X * 3 - _leftLB.WIDTH, 1000)
                        textFontSize:App_Delegate.largeFont];
    
    CGRect frame  = _rightLB.frame;
    frame.size.height = size.height > frame.size.height ? size.height: frame.size.height;
    frame.size.width = size.width;
    _rightLB.frame = frame;
    _rightLB.text = rightLBText;
    
    CGPoint center = _leftLB.center;
    center.y = _rightLB.center.y;
    _leftLB.center = center;
    
    frame = _whiteContentView.frame;
    frame.size.height = _rightLB.HEIGTH + _rightLB.Y ;
    _whiteContentView.frame = frame;
    
    frame = self.frame;
    frame.size.height = _whiteContentView.HEIGTH + _whiteContentView.Y  ;
    self.frame = frame;
    
}
-(void)cellTextFieldData:(UITextField*)textField{
    
    CGRect frame = self.frame;
    frame.size.height = _whiteContentView.HEIGTH + _whiteContentView.Y ;
    self.frame = frame;

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    
    return YES;
}
@end
