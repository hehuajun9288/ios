//
//  YiJianBaoZhangView.h
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YiJianBaoZhangViewController.h"

@interface YiJianBaoZhangView : UIView

-(instancetype)initWithFrame:(CGRect)frame controller:(YiJianBaoZhangViewController*)controller;

@end
