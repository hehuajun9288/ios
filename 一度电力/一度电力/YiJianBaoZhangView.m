//
//  YiJianBaoZhangView.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "YiJianBaoZhangView.h"
#import "MyTextView.h"
#import "Address.h"
#import "QRadioButton.h"
#import "MissionModel.h"

@interface YiJianBaoZhangView ()<UITextViewDelegate,QRadioButtonDelegate>


@property (nonatomic, strong) MyTextView *textView;
@property (nonatomic, strong) NSMutableArray *addressArr;
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic, strong) NSString *mtype;
@property (nonatomic, strong) YiJianBaoZhangViewController *bzViewController;
//@property (nonatomic, strong) Address *selectedAddress;

@end
@implementation YiJianBaoZhangView

-(instancetype)initWithFrame:(CGRect)frame controller:(YiJianBaoZhangViewController*)controller{

    self = [super initWithFrame:frame];
    
    if (self) {
        
        _addressArr = [NSMutableArray array];
        _bzViewController = controller;
        [self getMyValidAddress];
//        [self creatView];
        
    }
    
    return self;

}
#define Start_X VIEW_START_X           // 第一个textField的X坐标
#define Start_Y 20.0f           // 第一个textField的Y坐标
#define Width_Space 5.0f        // 2个按钮之间的横间距
#define Height_Space 10.0f      // 竖间距
#define Button_Height 20   // 高
#define Button_Width (SCREEN_WIDTH - VIEW_START_X * 2)      // 宽
-(void)creatView{

    
    NSString *labelText = @"请选择你所需要的业务,并在输入框内详细描述您的问题或者要求!";
    CGSize labelTextSize = [Utils getTextSize:labelText
                                   textRegion:CGSizeMake(SCREEN_WIDTH - VIEW_START_X * 2, 200)
                                 textFontSize:App_Delegate.largeFont];
    
    CGRect frame = CGRectMake(VIEW_START_X, VIEW_START_Y, SCREEN_WIDTH - VIEW_START_X * 2, labelTextSize.height);

    UILabel *alertLB = [UILabel labelWithFrame:frame
                                     LableText:labelText
                                          size:App_Delegate.largeFont
                                     textColor:BLACK_COLOR
                                 textAlignment:NSTextAlignmentLeft];
    [self addSubview:alertLB];
    
     frame = CGRectMake(VIEW_START_X, alertLB.Y + alertLB.HEIGTH + VIEW_START_Y, Button_Width, Button_Height);

    //创建地址button
    QRadioButton *button = [self creatAddressButton:frame];
    
    //输入框的背景视图
    frame = CGRectMake(VIEW_START_X, button.Y + button.HEIGTH + VIEW_START_Y, SCREEN_WIDTH - VIEW_START_X * 2, SCREEN_HEIGHT * 0.332);

    [self creatTextViewWithFrame:frame];
    
    frame = CGRectMake(VIEW_START_X, _textView.HEIGTH + _textView.Y + VIEW_START_Y, SCREEN_WIDTH - VIEW_START_X * 2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    
    
    //提交按钮
    UIButton *commiteBtn = [UIButton buttonWithFrame:frame
                                               title:@"提交"
                                            fontSize:App_Delegate.largeFont
                                          titleColor:WHITE_COLOR
                                              action:@selector(commiteAction)
                                              target:self
                                                 tag:0
                                     backgroundColor:BUTTON_BACKGROUND_COLOR];
    [self addSubview:commiteBtn];
}
#pragma mark - 创建textView
-(void)creatTextViewWithFrame:(CGRect)frame{

    _textView = [[MyTextView alloc] initWithFrame:frame];
    [_textView setBackgroundColor: CLEAR_COLOR];
    _textView.placeholder = @"请输入你的信息...";
    _textView.delegate = self;
    _textView.layer.borderWidth = 2;
    _textView.layer.borderColor = RGBA(81, 81, 81, 1).CGColor;
    _textView.layer.cornerRadius = 5;
    _textView.layer.masksToBounds = YES;
    _textView.font = [UIFont systemFontOfSize:App_Delegate.largeFont];
    _textView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    [self addSubview:_textView];

}


#define mark -创建地址button
-(QRadioButton*)creatAddressButton:(CGRect)LBframe{

    QRadioButton *button = [[QRadioButton alloc] initWithFrame:CGRectMake(0, LBframe.origin.y, 0, 0)];;
    
//    _addressArr = [NSMutableArray arrayWithObjects:@"配电设备抢修",@"用电手续代办",nil];
    NSInteger utypeid = App_Delegate.userModel.utypeid.integerValue;
    if (utypeid == 8) {//普通用户
        
        _datas = @[@"配电设备抢修",@"用电手续代办"];
    }else if (utypeid == 5){//业务员
        
        _datas = _addressArr;
        
    }
    for (int i = 0 ; i < _datas.count; i++) {
        
        CGRect frame = CGRectMake(i % 1 * (Button_Width + Width_Space) + LBframe.origin.x, i / 1  * (Button_Height + Height_Space) + LBframe.origin.y, Button_Width, Button_Height);
        
         button = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId2"];
        button.frame = frame;
//        Address *address = _addressArr[i]; address.address
        [button setTitle:_datas[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:App_Delegate.smallFont]];
        [button setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateSelected];
        button.tag = i + 2;
        [self addSubview:button];
        
        if (i == 0) {//设置第一个为默认选中地址
            
            [button setChecked:YES];

        }
        
    }

    return button;
}

#pragma mark -获取有效地址
-(void)getMyValidAddress{
    
    NSDictionary *dic = @{@"id":App_Delegate.userModel.id};
    
    WEAKSELF
    [AFSoapRequestOperation getMyValidAddressSoapRequestWithParams:@[dic] hudShowInView:weakSelf block:^(NSString *jsonStr) {
        
        STRONGSELF
        NSArray *addressDics = [jsonStr JSONValue];
        
        for (NSDictionary *dic in addressDics) {
            
            Address *address = [Address mj_objectWithKeyValues:dic];
            [strongSelf.addressArr addObject:address];
        }
        
        if (strongSelf.addressArr.count <= 0) {
            
            [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户,您还没有有效的报障地址..." message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
                [strongSelf.bzViewController.navigationController popViewControllerAnimated:YES];
            }];
        }
        else{
        
            [strongSelf creatView];

        }
    }];
    
}

#pragma mark - commiteAction
-(void)commiteAction{

    [_textView resignFirstResponder];
    
    if (_textView.text.length > 0) {
        
        
        /**
         private Long uid;		//用户id
         private int mtype;		//任务类型（普通用户 填2，物业填 1）
         private int mstatus;	//任务状态：0:已审核;1:取消;2:已分派;3:完成;4:已评分;5:申请;
         private int aid;		//地址id
         private String description;	//任务描述
         
         */
        MissionModel *mission = [[MissionModel alloc] init];
        mission.descri = _textView.text;
        mission.uid = App_Delegate.userModel.id;
        Address *ad = self.addressArr[0];
        mission.aid = ad.id;
        mission.mtype = _mtype;
//        if (App_Delegate.userModel.utypeid.intValue == 8) {//普通用户
//            
//            mission.mstatus = @"5";
//            mission.mtype = @"2";
//
//        }else if (App_Delegate.userModel.utypeid.intValue == 7){//物业
//            
//            mission.mstatus = @"0";
//            mission.mtype = @"1";
//
//        }else{
//        
//            mission.mstatus = @"";
//            mission.mtype = @"";
//
//        }
        NSMutableDictionary *mdic = [NSMutableDictionary dictionary];
        mdic = [mission mj_keyValues];
    
        NSArray *keys = [mdic allKeys];
        for (NSString *key in keys) {
            
            if ([key isEqualToString:@"descri"]) {
                
                NSString *value = mdic[@"descri"];
                [mdic removeObjectForKey:@"descri"];
                [mdic setValue:value forKey:@"description"];

            }
        }
        NSString *jsonStr = [Utils dictionaryToJsonStriing:mdic];
        NSDictionary *missionDic = @{@"jsonstr":jsonStr};
        
        WEAKSELF
        [AFSoapRequestOperation sendWarningSoapRequestWithParams:@[missionDic]
                                                   hudShowInView:weakSelf
                                                           block:^(NSString *jsonStr) {
                                                               
                                                               STRONGSELF
                                                               NSDictionary *resultDic = [jsonStr JSONValue];
                                                               
                                                               
                                                               NSString *result = resultDic[@"result"];
                                                               if (result.intValue == 1) {
                                                                   
                                                                   [UIAlertView bk_showAlertViewWithTitle:@"尊敬的客户，您的信息已成功提交,请随时关注进度!" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                       
                                                                       [strongSelf.bzViewController.navigationController popViewControllerAnimated:YES];
                                                                   }];
                                                               }else{
                                                               
                                                                   [UIAlertView bk_showAlertViewWithTitle:@"系统异常，请联系客服人员！" message:nil cancelButtonTitle:@"确定" otherButtonTitles:nil handler:nil];

                                                               }
                                                           }];

    }else if([_textView.text isEqualToString:@""]){
    
        [Utils showHUD:@"内容不能为空,请重新输入!" autoHide:YES inView:self];
    }
}


#pragma mark - textViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    //该判断用于联想输入
    if (textView.text.length > 200)
    {
       WEAKSELF
        [Utils showHUD:@"亲，最多只能输入200字噢" autoHide:YES inView:weakSelf];
        textView.text = [textView.text substringToIndex:200];
        
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}

#pragma mark - 地址button的协议方法
-(void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId{

//    Address *address = _addressArr[radio.tag];
//    _selectedAddress = address;
//    NSLog(@"address == %@",_addressArr[radio.tag]);
//    NSLog(@"address == %d",radio.tag);

    _mtype = [NSString stringWithFormat:@"%ld",radio.tag];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
