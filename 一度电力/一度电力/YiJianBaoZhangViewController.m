//
//  YiJianBaoZhangViewController.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/26.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import "YiJianBaoZhangViewController.h"
#import "YiJianBaoZhangView.h"
#import "Address.h"

@interface YiJianBaoZhangViewController ()


@end

@implementation YiJianBaoZhangViewController

- (void)viewDidLoad {
    [super viewDidLoad];


//    self.title = @"一键报障";
    self.view.backgroundColor = WHITE_COLOR;
    
    
    //一键报障的视图
    CGRect frame = CGRectMake(0, NAVIGATIONBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NAVIGATIONBAR_HEIGHT);
    YiJianBaoZhangView *yjbzView = [[YiJianBaoZhangView alloc] initWithFrame:frame controller:self];
    [self.view addSubview:yjbzView];
    
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
