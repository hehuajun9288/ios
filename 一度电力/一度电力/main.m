//
//  main.m
//  一度电力
//
//  Created by 廖幸杰 on 15/12/19.
//  Copyright © 2015年 廖幸杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
