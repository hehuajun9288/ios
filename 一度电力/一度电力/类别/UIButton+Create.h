//
//  UIButton+Create.h
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Create)

+(UIButton*)buttonWithFrame :(CGRect)frame
                       title:(NSString *)title
                    fontSize:(CGFloat)fontSize
                  titleColor:(UIColor*)titleColor
                      action:(SEL)action
                      target:(id)target
                         tag:(NSInteger)tag
             backgroundColor:(UIColor*)backgroundColor;

-(void)setBorder;


+(UIButton*)buttonWithFrame :(CGRect)frame
                  imageNamed:(NSString*)imageNamed
           selecteImageNamed:(NSString*)selecteImageNamed
                      action:(SEL)action
                      target:(id)target
                         tag:(NSInteger)tag;;

/*
+ (UIButton*)buttonWithFrame:(CGRect)frame
                  buttonType:(UIButtonType)type
                  titleColor:(UIColor *)titleColor
             backgroundColor:(UIColor *)backgroundColor
                         tag:(int)tag
                       title:(NSString*)title
                      action:(SEL)action
                      target:(id)target;
*/
@end
