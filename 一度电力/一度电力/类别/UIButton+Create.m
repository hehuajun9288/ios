//
//  UIButton+Create.m
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UIButton+Create.h"

@implementation UIButton (Create)


+(UIButton*)buttonWithFrame :(CGRect)frame
                       title:(NSString *)title
                    fontSize:(CGFloat)fontSize
                  titleColor:(UIColor*)titleColor
                      action:(SEL)action
                      target:(id)target
                         tag:(NSInteger)tag
             backgroundColor:(UIColor*)backgroundColor{

    UIButton *btn = [[UIButton alloc] initWithFrame:frame];//[UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = backgroundColor;
    
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    btn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    btn.tag = tag;
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

+(UIButton*)buttonWithFrame :(CGRect)frame imageNamed:(NSString*)imageNamed selecteImageNamed:(NSString*)selecteImageNamed action:(SEL)action target:(id)target tag:(NSInteger)tag{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setImage:[UIImage imageNamed:imageNamed] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:selecteImageNamed] forState:UIControlStateSelected];
    btn.tag = tag;
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
    
}

+ (UIButton*)buttonWithFrame:(CGRect)frame
                  buttonType:(UIButtonType)type
                  titleColor:(UIColor *)titleColor
             backgroundColor:(UIColor *)backgroundColor
                         tag:(int)tag
                       title:(NSString*)title
                      action:(SEL)action
                      target:(id)target{
    UIButton*button=[UIButton buttonWithType:type];
    [button setFrame:frame];
    [button setTag:tag];
    [button setBackgroundColor:backgroundColor];
    button.titleLabel.font = [UIFont systemFontOfSize:App_Delegate.largeFont];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];

    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void)setBorder{

    self.layer.cornerRadius = self.WIDTH * 0.228;
    self.layer.masksToBounds = YES;
    [self.layer setBorderWidth:0.8]; //边框宽度
    self.layer.borderColor=[UIColor whiteColor].CGColor;

}
@end
