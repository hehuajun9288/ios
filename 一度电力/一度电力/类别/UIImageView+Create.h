//
//  UIImageView+Create.h
//  畅停
//
//  Created by wff on 15/8/24.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Create)

+ (UIImageView *)imageWithFrame:(CGRect)frame
                backgroundColor:(UIColor *)backgroundColor
                     imageNamed:(NSString *)imageNamed;

@end
