//
//  UIImageView+Create.m
//  畅停
//
//  Created by wff on 15/8/24.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UIImageView+Create.h"

@implementation UIImageView (Create)

+ (UIImageView *)imageWithFrame:(CGRect)frame
                backgroundColor:(UIColor *)backgroundColor
                     imageNamed:(NSString *)imageNamed{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    imageView.image = [UIImage imageNamed:imageNamed];
    imageView.backgroundColor = backgroundColor;
    imageView.userInteractionEnabled = YES;
    return imageView;
}

@end
