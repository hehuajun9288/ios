//
//  UILabel+create.h
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UILabel (create)

+ (UILabel *) labelWithFrame:(CGRect)frame  LableText:(NSString *)labelText size:(CGFloat)size textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment;

+ (UILabel *)initLabel:(NSTextAlignment)textAlignment;

-(void)setCornerRadius;
@end
