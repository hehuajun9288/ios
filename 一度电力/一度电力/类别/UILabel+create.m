//
//  UILabel+create.m
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UILabel+create.h"

@implementation UILabel (create)


-(void)setCornerRadius{

    self.layer.cornerRadius = 3;
    self.layer.masksToBounds = YES;

}
+ (UILabel *) labelWithFrame:(CGRect)frame  LableText:(NSString *)labelText size:(CGFloat)size textColor:(UIColor *)textColor  textAlignment:(NSTextAlignment)textAlignment{
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = textAlignment;
    label.font = [UIFont systemFontOfSize:size];
    label.text = labelText;
    label.textColor = textColor;
    label.lineBreakMode = NSLineBreakByTruncatingMiddle;
    label.numberOfLines = 0;
    return label;
}

+ (UILabel *)initLabel:(NSTextAlignment)textAlignment{
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = textAlignment;
    label.font = [UIFont systemFontOfSize:App_Delegate.largeFont];
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    return label;
}

@end
