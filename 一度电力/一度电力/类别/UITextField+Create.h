//
//  UITextField+Create.h
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Create)

+(UITextField*)textFieldWithFrame :(CGRect)frame placehold:(NSString *)placehold target:(id)target tag:(NSInteger)tag;
@end
