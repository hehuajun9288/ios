//
//  UITextField+Create.m
//  畅停
//
//  Created by LiaoXingJie on 15/8/13.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UITextField+Create.h"

@implementation UITextField (Create)

+(UITextField*)textFieldWithFrame :(CGRect)frame
                         placehold:(NSString *)placehold
                            target:(id)target
                               tag:(NSInteger)tag{

    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.placeholder = placehold;
    tf.returnKeyType = UIReturnKeyDefault;
    tf.delegate = target;
    tf.borderStyle = UITextBorderStyleNone;
    tf.tag = tag;
    tf.font = [UIFont systemFontOfSize:App_Delegate.largeFont];
//    tf.keyboardType = UIKeyboardTypeNamePhonePad;

    return tf;
}

@end
