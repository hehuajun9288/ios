//
//  UIView+Create.h
//  畅停
//
//  Created by wff on 15/8/17.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (Create)

+ (UIView *)viewWithFrame:(CGRect)frame
                   hidden:(BOOL)hidden
          backgroundColor:(UIColor *)backgroundColor;

@property (nonatomic, assign) CGFloat X;
@property (nonatomic, assign) CGFloat Y;
@property (nonatomic, assign) CGFloat WIDTH;
@property (nonatomic, assign) CGFloat HEIGTH;
//@property (nonatomic, copy) void (^buttonTap)(void);

-(UIButton*)creatButtonWhthFrame:(CGFloat)frameY Title:(NSString*)buttonTitle;
@end
