//
//  UIView+Create.m
//  畅停
//
//  Created by wff on 15/8/17.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UIView+Create.h"

@implementation UIView (Create)

+ (UIView *)viewWithFrame:(CGRect)frame
                   hidden:(BOOL)hidden
          backgroundColor:(UIColor *)backgroundColor{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = backgroundColor;
    view.hidden = hidden;
    return view;
}

-(CGFloat)X{

    return self.frame.origin.x;
}

-(CGFloat)Y{
    
    return self.frame.origin.y;

}

-(CGFloat)WIDTH{
    return self.frame.size.width;

}

-(CGFloat)HEIGTH{

    return  self.frame.size.height;
}

-(UIButton*)creatButtonWhthFrame:(CGFloat)frameY Title:(NSString*)buttonTitle{
    
    CGRect frame = CGRectMake(CONTENT_VIEW_START_X, frameY, SCREEN_WIDTH - CONTENT_VIEW_START_X *  2, TABLE_VIEW_HEIGHT_FOR_ROW - VIEW_START_Y * 2);
    UIButton *button = [UIButton buttonWithFrame:frame
                                           title:buttonTitle
                                        fontSize:App_Delegate.largeFont
                                      titleColor:WHITE_COLOR
                                          action:nil
                                          target:nil
                                             tag:0
                                 backgroundColor:BUTTON_BACKGROUND_COLOR];
    
//    [self.contentView addSubview:button];
    return button;
}


//-(void)buttonTap:(UIButton*)button{
//
//    if (self.buttonTap) {
//        
//        self.buttonTap();
//    }
//}

@end
