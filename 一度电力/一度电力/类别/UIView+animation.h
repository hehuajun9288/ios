//
//  UIView+animation.h
//  畅停
//
//  Created by LiaoXingJie on 15/10/6.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (animation)

-(void)showInView:(UIView*)view;
-(void)showInWindow;
-(void)dismiss;

@end
