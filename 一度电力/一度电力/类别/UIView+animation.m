//
//  UIView+animation.m
//  畅停
//
//  Created by LiaoXingJie on 15/10/6.
//  Copyright (c) 2015年 HCT. All rights reserved.
//

#import "UIView+animation.h"

@implementation UIView (animation)

-(void)addAnimation{
    
    
//    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
//    popAnimation.duration = 0.4;
//    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
//                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
//                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
//                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
//    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
//    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.layer addAnimation:popAnimation forKey:nil];

}
-(void)showInView:(UIView*)view{
    
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0.0;
    
    [UIView animateWithDuration:.5 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.alpha = 1;
        
    } completion:^(BOOL finished) {
        
        [view addSubview:self];
        
    }];
}
-(void)showInWindow{
    
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0.0;
    
    [UIView animateWithDuration:.5 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.alpha = 1;
        
    } completion:^(BOOL finished) {
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:self];
        
    }];
 }

-(void)dismiss{
    
    [UIView animateWithDuration:.5 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        
    }];

//    CAKeyframeAnimation *hideAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
//    hideAnimation.duration = 0.4;
//    hideAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
//                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0f, 1.0f, 1.0f)],
//                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.00f, 0.00f, 0.00f)]];
//    hideAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f];
//    hideAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    hideAnimation.delegate = self;
//    [self.layer addAnimation:hideAnimation forKey:nil];
//    [self removeFromSuperview];

}

@end
